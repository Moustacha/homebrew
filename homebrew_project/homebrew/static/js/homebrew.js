/* Project specific Javascript goes here. */

var style_response_cache = {};

function get_recipes_for_style(json_path, style_id) {
    console.log('Start get_recipes_for_style');
    if (style_response_cache[style_id]) {
        $("#id_recipe").html(style_response_cache[style_id]);
    } else {
        $.getJSON(json_path, {style_id: style_id},
            function (ret, textStatus) {
                var options = '<option value="" selected="selected">---------</option>';
                for (var i in ret) {
                    options += '<option value="' + ret[i].id + '">'
                        + ret[i].name + '</option>';
                }
                style_response_cache[style_id] = options;
                $("#id_recipe").html(options);
            });
    }
}

jQuery(document).ready(function() {
    var offset = 250;
    var duration = 300;
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() > offset) {
            jQuery('.back-to-top').fadeIn(duration);
        } else {
            jQuery('.back-to-top').fadeOut(duration);
        }
    });

    jQuery('.back-to-top').click(function(event) {
        event.preventDefault();
        jQuery('html, body').animate({scrollTop: 0}, duration);
        return false;
    })
});