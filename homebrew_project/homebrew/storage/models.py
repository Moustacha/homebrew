from django.core.urlresolvers import reverse
from django.db import models
from django.contrib.auth.models import User


from recipes.models import Fermentable, Hop, Other, Yeast


class StockManager(models.Manager):

    def owned_by(self, user):
        return self.get_queryset().filter(owner=user).select_related('ingredient__name')

class Stock(models.Model):
    class Meta:
        abstract = True

    UNIT_CHOICES = (
        ('g', 'Grams'),
        ('kg', 'Kilograms'),
        ('tsp', 'Teaspoon'),
        ('cup', 'Cup'),
        ('sticks', 'Sticks'),
        ('pods', 'Pods'),
        ('ml', 'Millilitres'),
        ('sachet', 'Sachets'),
        ('vial', 'Vials'),
    )

    quantity = models.DecimalField(max_digits=10, decimal_places=2)
    unit = models.CharField(max_length=10, choices=UNIT_CHOICES)

    owner = models.ForeignKey(User)

    objects=StockManager()

    def get_update_url(self):
        return reverse(str(self._meta.model_name + '-update').lower(), kwargs={'pk': self.pk})

    def get_delete_url(self):
        return reverse(str(self._meta.model_name + '-delete').lower(), kwargs={'pk': self.pk})

    def get_list_url(self):
        return reverse(str(self._meta.model_name + '-list').lower())


class FermentableStock(Stock):
    ingredient = models.ForeignKey(Fermentable, related_name='stock', related_query_name='stock')


class HopStock(Stock):
    ingredient = models.ForeignKey(Hop, related_name='stock', related_query_name='stock')


class OtherStock(Stock):
    ingredient = models.ForeignKey(Other, related_name='stock', related_query_name='stock')


class YeastStock(Stock):
    ingredient = models.ForeignKey(Yeast, related_name='stock', related_query_name='stock')

