from django import forms
from django.forms.utils import ErrorList
from recipes.models import Fermentable, Hop, Other, Yeast

from storage.models import FermentableStock, HopStock, OtherStock, YeastStock

__author__ = 'sam'

EMPTY_CHOICE = [(None, '---------')]


class FermentableStockForm(forms.ModelForm):
    list_viewname = 'fermentablestock-list'

    class Meta:
        model = FermentableStock
        fields = ['ingredient', 'quantity', 'unit', 'owner']
        widgets = {
            'owner': forms.HiddenInput()
        }

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
                 label_suffix=None, empty_permitted=False, instance=None, user=None):
        super().__init__(data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, instance)
        # override 'fermentable' choices to limit to what the user should be able to see
        self.fields['ingredient'].choices = EMPTY_CHOICE + [(f['id'], f['name']) for f in
                                                            Fermentable.objects.public_or_owner(user).values(
                                                                'id', 'name')]


class HopStockForm(forms.ModelForm):
    list_viewname = 'hopstock-list'

    class Meta:
        model = HopStock
        fields = ['ingredient', 'quantity', 'unit', 'owner']
        widgets = {
                    'owner': forms.HiddenInput()
                }

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
                 label_suffix=None, empty_permitted=False, instance=None, user=None):
        super().__init__(data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, instance)
        # override 'hop' choices to limit to what the user should be able to see
        self.fields['ingredient'].choices = EMPTY_CHOICE + [(f['id'], f['name']) for f in
                                                            Hop.objects.public_or_owner(user).values(
                                                                'id', 'name')]


class OtherStockForm(forms.ModelForm):
    list_viewname = 'otherstock-list'

    class Meta:
        model = OtherStock
        fields = ['ingredient', 'quantity', 'unit', 'owner']
        widgets = {
            'owner': forms.HiddenInput()
        }

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
                 label_suffix=None, empty_permitted=False, instance=None, user=None):
        super().__init__(data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, instance)
        # override 'other' choices to limit to what the user should be able to see
        self.fields['ingredient'].choices = EMPTY_CHOICE + [(f['id'], f['name']) for f in
                                                            Other.objects.public_or_owner(user).values(
                                                                'id', 'name')]


class YeastStockForm(forms.ModelForm):
    list_viewname = 'yeaststock-list'

    class Meta:
        model = YeastStock
        fields = ['ingredient', 'quantity', 'unit', 'owner']
        widgets = {
            'owner': forms.HiddenInput()
        }

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
                 label_suffix=None, empty_permitted=False, instance=None, user=None):
        super().__init__(data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, instance)
        # override 'yeast' choices to limit to what the user should be able to see
        self.fields['ingredient'].choices = EMPTY_CHOICE + [(f['id'], '{} {}'.format(f['lab__name'], f['name'])) for f
                                                            in Yeast.objects.public_or_owner(user)
                                                            .values('id', 'name', 'lab__name')]
