# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0012_auto_20150412_1344'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='FermentableStock',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('quantity', models.DecimalField(max_digits=10, decimal_places=2)),
                ('unit', models.CharField(max_length=10, choices=[('g', 'Grams'), ('kg', 'Kilograms'), ('tsp', 'Teaspoon'), ('cup', 'Cup'), ('sticks', 'Sticks'), ('pods', 'Pods'), ('ml', 'Millilitres')])),
                ('fermentable', models.ForeignKey(related_query_name='stock', related_name='stock', to='recipes.Fermentable')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='HopStock',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('quantity', models.DecimalField(max_digits=10, decimal_places=2)),
                ('unit', models.CharField(max_length=10, choices=[('g', 'Grams'), ('kg', 'Kilograms'), ('tsp', 'Teaspoon'), ('cup', 'Cup'), ('sticks', 'Sticks'), ('pods', 'Pods'), ('ml', 'Millilitres')])),
                ('hop', models.ForeignKey(related_query_name='stock', related_name='stock', to='recipes.Hop')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OtherStock',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('quantity', models.DecimalField(max_digits=10, decimal_places=2)),
                ('unit', models.CharField(max_length=10, choices=[('g', 'Grams'), ('kg', 'Kilograms'), ('tsp', 'Teaspoon'), ('cup', 'Cup'), ('sticks', 'Sticks'), ('pods', 'Pods'), ('ml', 'Millilitres')])),
                ('other', models.ForeignKey(related_query_name='stock', related_name='stock', to='recipes.Other')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
