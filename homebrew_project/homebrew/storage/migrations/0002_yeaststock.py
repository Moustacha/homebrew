# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('recipes', '0012_auto_20150412_1344'),
        ('storage', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='YeastStock',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('quantity', models.DecimalField(decimal_places=2, max_digits=10)),
                ('unit', models.CharField(choices=[('g', 'Grams'), ('kg', 'Kilograms'), ('tsp', 'Teaspoon'), ('cup', 'Cup'), ('sticks', 'Sticks'), ('pods', 'Pods'), ('ml', 'Millilitres')], max_length=10)),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('yeast', models.ForeignKey(related_query_name='stock', to='recipes.Yeast', related_name='stock')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
