# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('storage', '0002_yeaststock'),
    ]

    operations = [
        migrations.RenameField(
            model_name='fermentablestock',
            old_name='fermentable',
            new_name='ingredient',
        ),
        migrations.RenameField(
            model_name='hopstock',
            old_name='hop',
            new_name='ingredient',
        ),
        migrations.RenameField(
            model_name='otherstock',
            old_name='other',
            new_name='ingredient',
        ),
        migrations.RenameField(
            model_name='yeaststock',
            old_name='yeast',
            new_name='ingredient',
        ),
    ]
