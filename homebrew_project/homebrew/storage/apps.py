__author__ = 'sam'

from django.apps import AppConfig


class StorageConfig(AppConfig):
    name = 'storage'
    verbose_name = "Homebrew Inventory"
