from django.core.urlresolvers import reverse
from django.views.generic import CreateView, UpdateView, DeleteView
from django_tables2 import SingleTableView
from permissionsx.contrib.django.views import DjangoViewMixin
from homebrew.permissions import UserIsAuthenticatedPermissions
from recipes.permissions import ObjectOwnerPermissions
from recipes.views.generic import FormViewMessageMixin, get_dependant_objects

__author__ = 'sam'

class StockCreateView(FormViewMessageMixin, DjangoViewMixin, CreateView):
    template_name = 'storage/storage_create.html'

    # Authorisation
    permissions = UserIsAuthenticatedPermissions()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.model._meta.verbose_name.title()
        return context

    def get_initial(self):
        initial = super().get_initial()
        initial['owner'] = self.request.user
        return initial

    def get_success_url(self):
        return reverse(str(self.model.__name__ + '-list').lower())

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        form_kwargs['user'] = self.request.user
        return form_kwargs


class StockUpdateView(FormViewMessageMixin, DjangoViewMixin, UpdateView):
    template_name = 'storage/storage_update.html'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        # Authorisation
        self.permissions = ObjectOwnerPermissions(model_class=self.model, lookup_field='pk')

    def get_success_url(self):
        return reverse(str(self.model.__name__ + '-list').lower())

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        form_kwargs['user'] = self.request.user
        return form_kwargs


class StockDeleteView(FormViewMessageMixin, DjangoViewMixin, DeleteView):
    template_name = 'storage/storage_delete.html'
    success_message = '{} successfully deleted'
    failure_message = 'Error while deleting {}'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        # Authorisation
        self.permissions = ObjectOwnerPermissions(model_class=self.model, lookup_field='pk')

    def get_success_url(self):
        return reverse(str(self.model.__name__ + '-list').lower())

    def get_queryset(self):
        return super().get_queryset().select_related('ingredient__name')


class StockListView(SingleTableView):
    template_name = 'storage/storage_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.model._meta.verbose_name.title()
        context['create_url'] = reverse(str(self.model.__name__ + '-create').lower())
        return context

    def get(self, request, *args, **kwargs):
        self.queryset = self.model.objects.owned_by(request.user)
        return super().get(request, *args, **kwargs)