from django_tables2 import tables, Column, LinkColumn, A, TemplateColumn
from storage.models import FermentableStock

__author__ = 'sam'

ATTRS = {'class': 'table table-hover'}
UPDATE_TEMPLATE = '<a href="{{ record.get_update_url }}" class="btn btn-primary" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>'
DELETE_TEMPLATE = '<a href="{{ record.get_delete_url }}" class="btn btn-danger" title="Delete"><i class="glyphicon glyphicon-remove"></i></a>'


class FermentableStockTable(tables.Table):
    fermentable_name = Column(accessor='ingredient.name', verbose_name='Fermentable')
    quantity = Column(accessor='quantity', verbose_name='Quantity')
    unit = Column(accessor='unit', verbose_name='Unit')
    update = TemplateColumn(UPDATE_TEMPLATE)
    delete = TemplateColumn(DELETE_TEMPLATE)

    class Meta:
        model = FermentableStock
        fields = ('fermentable_name', 'quantity', 'unit', )
        attrs = ATTRS
        order_by = ('fermentable_name', 'quantity',)
        empty_text = 'You\'ve got no fermentables in stock!'


class HopStockTable(tables.Table):
    hop_name = Column(accessor='ingredient.name', verbose_name='Hop')
    quantity = Column(accessor='quantity', verbose_name='Quantity')
    unit = Column(accessor='unit', verbose_name='Unit')
    update = TemplateColumn(UPDATE_TEMPLATE)
    delete = TemplateColumn(DELETE_TEMPLATE)

    class Meta:
        model = FermentableStock
        fields = ('hop_name', 'quantity', 'unit', )
        attrs = ATTRS
        order_by = ('hop_name', 'quantity',)
        empty_text = 'You\'ve got no hops in stock!'


class OtherStockTable(tables.Table):
    other_name = Column(accessor='ingredient.name', verbose_name='Other Adjunct')
    quantity = Column(accessor='quantity', verbose_name='Quantity')
    unit = Column(accessor='unit', verbose_name='Unit')
    update = TemplateColumn(UPDATE_TEMPLATE)
    delete = TemplateColumn(DELETE_TEMPLATE)

    class Meta:
        model = FermentableStock
        fields = ('other_name', 'quantity', 'unit', )
        attrs = ATTRS
        order_by = ('other_name', 'quantity',)
        empty_text = 'You\'ve got no other adjuncts in stock!'


class YeastStockTable(tables.Table):
    yeast_name = Column(accessor='ingredient.name', verbose_name='Yeast')
    quantity = Column(accessor='quantity', verbose_name='Quantity')
    unit = Column(accessor='unit', verbose_name='Unit')
    update = TemplateColumn(UPDATE_TEMPLATE)
    delete = TemplateColumn(DELETE_TEMPLATE)

    class Meta:
        model = FermentableStock
        fields = ('yeast_name', 'quantity', 'unit', )
        attrs = ATTRS
        order_by = ('yeast_name', 'quantity',)
        empty_text = 'You\'ve got no yeasts in stock!'