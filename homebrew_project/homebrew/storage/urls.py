from django.conf.urls import url, patterns

from storage.forms import FermentableStockForm, HopStockForm, YeastStockForm, OtherStockForm
from storage.models import FermentableStock, HopStock, YeastStock, OtherStock
from storage.views.tables import FermentableStockTable, HopStockTable, YeastStockTable, OtherStockTable
from storage.views.view import StockListView, StockCreateView, StockUpdateView, StockDeleteView

__author__ = 'sam'

urlpatterns = patterns(
    '',
    url(r'^list/fermentable/$', StockListView.as_view(model=FermentableStock, table_class=FermentableStockTable),
        name='fermentablestock-list'),
    url(r'^create/fermentable/$', StockCreateView.as_view(model=FermentableStock, form_class=FermentableStockForm),
        name='fermentablestock-create'),
    url(r'^update/fermentable/(?P<pk>\d+)/$',
        StockUpdateView.as_view(model=FermentableStock, form_class=FermentableStockForm),
        name='fermentablestock-update'),
    url(r'^delete/fermentable/(?P<pk>\d+)/$',
        StockDeleteView.as_view(model=FermentableStock),
        name='fermentablestock-delete'),

    url(r'^list/hop/$', StockListView.as_view(model=HopStock, table_class=HopStockTable),
        name='hopstock-list'),
    url(r'^create/hop/$', StockCreateView.as_view(model=HopStock, form_class=HopStockForm),
        name='hopstock-create'),
    url(r'^update/hop/(?P<pk>\d+)/$',
        StockUpdateView.as_view(model=HopStock, form_class=HopStockForm),
        name='hopstock-update'),
    url(r'^delete/hop/(?P<pk>\d+)/$',
        StockDeleteView.as_view(model=HopStock),
        name='hopstock-delete'),

    url(r'^list/other/$', StockListView.as_view(model=OtherStock, table_class=OtherStockTable),
        name='otherstock-list'),
    url(r'^create/other/$', StockCreateView.as_view(model=OtherStock, form_class=OtherStockForm),
        name='otherstock-create'),
    url(r'^update/other/(?P<pk>\d+)/$',
        StockUpdateView.as_view(model=OtherStock, form_class=OtherStockForm),
        name='otherstock-update'),
    url(r'^delete/other/(?P<pk>\d+)/$',
        StockDeleteView.as_view(model=OtherStock),
        name='otherstock-delete'),

    url(r'^list/yeast/$', StockListView.as_view(model=YeastStock, table_class=YeastStockTable),
        name='yeaststock-list'),
    url(r'^create/yeast/$', StockCreateView.as_view(model=YeastStock, form_class=YeastStockForm),
        name='yeaststock-create'),
    url(r'^update/yeast/(?P<pk>\d+)/$',
        StockUpdateView.as_view(model=YeastStock, form_class=YeastStockForm),
        name='yeaststock-update'),
    url(r'^delete/yeast/(?P<pk>\d+)/$',
        StockDeleteView.as_view(model=YeastStock),
        name='yeaststock-delete'),
)