from django.views.generic import TemplateView
from brews.models import Brew
from recipes.models import BaseRecipe

__author__ = 'sam'


# These views don't quite fit into either app, so they have been placed here instead
class HomeTemplateView(TemplateView):
    template_name = 'home/home_base.html'


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.user.is_authenticated():
            context['latest_brews'] = Brew.objects.latest_brews(self.request.user)
            context['latest_recipes'] = BaseRecipe.objects.latest_recipes(self.request.user)

        return context