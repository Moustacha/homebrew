from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from permissionsx.contrib.django.views import MessageRedirectView
from permissionsx.models import Permissions, P

__author__ = 'sam'


class LoginRequiredRedirectView(MessageRedirectView):
    message = (messages.warning, 'You need to be logged in to view this content')
    redirect_url = reverse_lazy('account_login')


class NotAllowedRedirectView(MessageRedirectView):
    message = (messages.warning, 'You do not have permission to view this content')
    redirect_url = reverse_lazy('home')


authenticated_permission = P(user__is_authenticated=True, if_false=LoginRequiredRedirectView.as_view())


class UserIsAuthenticatedPermissions(Permissions):
    rules = authenticated_permission
