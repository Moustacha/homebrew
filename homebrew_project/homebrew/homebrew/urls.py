from django.conf.urls import patterns, include, url
from django.conf import settings
from django.views.generic import TemplateView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from homebrew.views import HomeTemplateView

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^$', HomeTemplateView.as_view(), name='home'),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^accounts/settings/$',
        TemplateView.as_view(template_name='account/account_settings_overview.html'),
        name='account_settings'),
    url(r'^about/$', TemplateView.as_view(template_name='about.html'), name='about'),
    url(r'^markdownx/', include('markdownx.urls')),

    # Examples:
    # url(r'^$', 'homebrew.views.home', name='home'),
    # url(r'^homebrew/', include('homebrew.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^recipes/', include('recipes.urls')),
    url(r'^brews/', include('brews.urls')),
    url(r'^stock/', include('storage.urls')),
)

# Uncomment the next line to serve media files in dev.
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += patterns(
        '',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
