import datetime

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Max
from uuslug import uuslug
from recipes import calculations

from recipes.models import BaseRecipe, AllGrainRecipe, ExtractRecipe


class BrewManager(models.Manager):
    def for_user(self, user):
        return self.get_queryset().filter(owner=user)

    def latest_brews(self, user):
        return self.for_user(user).select_related('recipe').order_by('-brew_date')[:5]


class Brew(models.Model):
    objects = BrewManager()

    recipe = models.ForeignKey(BaseRecipe)
    brew_date = models.DateField()
    bottle_date = models.DateField(blank=True, null=True)
    # gravityreading_set
    # temperaturereading_set
    recipe_editable = models.BooleanField(default=False, editable=False)  # Has the recipe been cloned yet?
    owner = models.ForeignKey(User)

    slug = models.SlugField(editable=False)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.id:
            self.slug = uuslug(self.recipe.name, instance=self, max_length=50)
        super().save(force_insert, force_update, using, update_fields)

    def clone_recipe_for_update(self):
        # Get a new reference to the recipe
        recipe = BaseRecipe.objects.get(pk=self.recipe.pk)

        if hasattr(recipe, 'extractrecipe'):
            recipe = recipe.extractrecipe
            if hasattr(recipe, 'allgrainrecipe'):
                recipe = recipe.allgrainrecipe

        recipe.clone_for_batch(self.owner)

        self.recipe = recipe
        self.recipe_editable = True

        self.save()

    def get_absolute_url(self):
        return reverse('brew-detail', kwargs={'slug': self.slug})

    def get_estimated_ready_date(self):
        estimated_date = None
        if self.bottle_date:
            kwargs = {}
            if self.recipe.conditioning_time_quantifier == 'd':
                kwargs = {'days': self.recipe.conditioning_time}
            elif self.recipe.conditioning_time_quantifier == 'w':
                kwargs = {'weeks': self.recipe.conditioning_time}
            elif self.recipe.conditioning_time_quantifier == 'y':
                kwargs = {'days': self.recipe.conditioning_time * 365}

            # Don't calculate a date if the conditioning time quantifier is invalid
            if len(kwargs.keys()) == 1:
                estimated_date = self.bottle_date + datetime.timedelta(**kwargs)

        return estimated_date

    def alcohol_by_volume(self):
        if self.gravityreading_set.count() > 0:
            qs = self.gravityreading_set.order_by('time')

            original_gravity = qs.first()
            final_gravity = qs.last()

            return calculations.alcohol_by_volume(original_gravity.value, final_gravity.value)
        else:
            return 0

    def priming_sucrose(self):
        # Get the highest temperature
        max_temp = self.temperaturereading_set.aggregate(Max('value'))['value__max']
        return calculations.priming_sucrose(self.recipe.target_volume, max_temp, self.recipe.style.co_volume)

    def priming_dextrose(self):
        # Get the highest temperature
        max_temp = self.temperaturereading_set.aggregate(Max('value'))['value__max']
        return calculations.priming_dextrose(self.recipe.target_volume, max_temp, self.recipe.style.co_volume)

    def priming_dme(self):
        # Get the highest temperature
        max_temp = self.temperaturereading_set.aggregate(Max('value'))['value__max']
        return calculations.priming_dme(self.recipe.target_volume, max_temp, self.recipe.style.co_volume)


class Reading(models.Model):
    """
    Abstract base class for any kind of reading, e.g. Gravity Reading, Temperature Reading
    """
    batch = models.ForeignKey(Brew)
    time = models.DateTimeField()
    value = models.DecimalField(max_digits=19, decimal_places=10)

    class Meta:
        abstract = True

    def get_absolute_url(self):
        return reverse('brew-detail', kwargs={'slug': self.batch.slug})


class GravityReading(Reading):

    class Meta:
        ordering = ('time', )

    def __str__(self):
        return 'SG {value} @ {time}'.format(value=self.value, time=self.time)

    def get_absolute_url(self):
        return super().get_absolute_url()+'#tab_gravity_readings'

    def get_update_url(self):
        return reverse('brew-detail-gravity-update', kwargs={'slug': self.batch.slug, 'pk': self.pk})

    def get_delete_url(self):
        return reverse('brew-detail-gravity-delete', kwargs={'slug': self.batch.slug, 'pk': self.pk})

    def get_formatted_value(self):
        return self.value


class TemperatureReading(Reading):

    class Meta:
        ordering = ('time', )

    def __str__(self):
        return '{value:0.2f}°C @ {time}'.format(value=self.value, time=self.time)

    def get_absolute_url(self):
        return super().get_absolute_url()+'#tab_temp_readings'

    def get_update_url(self):
        return reverse('brew-detail-temp-update', kwargs={'slug': self.batch.slug, 'pk': self.pk})

    def get_delete_url(self):
        return reverse('brew-detail-temp-delete', kwargs={'slug': self.batch.slug, 'pk': self.pk})

    def get_formatted_value(self):
        return '{:0.2f}°C'.format(self.value)


class BrewNoteManager(models.Manager):
    def appearance_set(self, brew):
        return self.for_type(brew=brew, note_type=BrewNote.APPEARANCE)

    def aroma_set(self, brew):
        return self.for_type(brew=brew, note_type=BrewNote.AROMA)

    def boil_set(self, brew):
        return self.for_type(brew=brew, note_type=BrewNote.BOIL)

    def feel_set(self, brew):
        return self.for_type(brew=brew, note_type=BrewNote.FEEL)

    def fermentation_set(self, brew):
        return self.for_type(brew=brew, note_type=BrewNote.FERMENTATION)

    def flavour_set(self, brew):
        return self.for_type(brew=brew, note_type=BrewNote.FLAVOUR)

    def general_set(self, brew):
        return self.for_type(brew=brew, note_type=BrewNote.GENERAL)

    def mash_set(self, brew):
        return self.for_type(brew=brew, note_type=BrewNote.MASH)

    def overall_set(self, brew):
        return self.for_type(brew=brew, note_type=BrewNote.OVERALL)

    def didnt_work_set(self, brew):
        return self.for_type(brew=brew, note_type=BrewNote.DIDNT_WORK)

    def what_worked_set(self, brew):
        return self.for_type(brew=brew, note_type=BrewNote.WHAT_WORKED)

    def for_type(self, brew, note_type):
        return self.get_queryset().filter(brew=brew, type=note_type).order_by('pk')


class BrewNote(models.Model):
    GENERAL = 1
    MASH = 2
    BOIL = 3
    FERMENTATION = 4
    APPEARANCE = 5
    AROMA = 6
    FLAVOUR = 7
    FEEL = 8
    OVERALL = 9
    DIDNT_WORK = 10
    WHAT_WORKED = 11

    NOTE_TYPES = (
        (APPEARANCE, 'Appearance'),
        (AROMA, 'Aroma'),
        (BOIL, 'Boil'),
        (FEEL, 'Feel'),
        (FERMENTATION, 'Fermentation'),
        (FLAVOUR, 'Flavour'),
        (GENERAL, 'General'),
        (MASH, 'Mash'),
        (OVERALL, 'Overall'),
        (DIDNT_WORK, 'What Didn\'t Work'),
        (WHAT_WORKED, 'What Worked'),
    )

    objects = BrewNoteManager()

    brew = models.ForeignKey(Brew)
    type = models.IntegerField(choices=NOTE_TYPES)
    note = models.TextField()
    created_date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.note

    def get_absolute_url(self):
        return reverse('brew-detail', kwargs={'slug': self.brew.slug})+'#tab_notes'

    def get_update_url(self):
        return reverse('brew-note-edit', kwargs={'slug': self.brew.slug, 'pk': self.pk})

    def get_delete_url(self):
        return reverse('brew-note-delete', kwargs={'slug': self.brew.slug, 'pk': self.pk})