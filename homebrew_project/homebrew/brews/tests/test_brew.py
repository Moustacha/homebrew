from datetime import datetime

from django.contrib.auth.models import User
from django.test import TestCase

from brews.models import Brew, BrewNote, GravityReading, TemperatureReading
from recipes.models import AllGrainRecipe, BaseRecipe, ExtractRecipe


__author__ = 'sam'


class BrewTestCase(TestCase):
    fixtures = ['Users', 'SampleDB']

    def setUp(self):
        self.user_name = 'test_user'
        self.user_password = 'test_password'

        self.user_two_name = 'test_two'
        self.user_two_password = 'test_password'

        self.superuser_name = 'superuser'
        self.superuser_password = 'superpassword'
        super().setUp()

    def test_clone_recipe_for_update(self):
        recipe = AllGrainRecipe.objects.get(pk=19)
        user = User.objects.get_by_natural_key(self.user_two_name)
        brew = Brew(recipe=recipe, brew_date=datetime.now(), owner=user)

        self.assertFalse(brew.recipe_editable)

        brew.clone_recipe_for_update()

        # The recipe for the brew should be different
        self.assertNotEqual(recipe.pk, brew.recipe.pk)
        self.assertTrue(brew.recipe_editable)
        self.assertNotEqual(recipe.slug, brew.recipe.slug)

        brew_recipe = BaseRecipe.objects.get(pk=brew.recipe.pk)
        self.assertIsNotNone(brew_recipe)
        self.assertTrue(hasattr(brew_recipe, 'extractrecipe'))
        self.assertTrue(hasattr(brew_recipe.extractrecipe, 'allgrainrecipe'))


        # ExtractRecipe
        recipe = ExtractRecipe.objects.get(pk=21)
        brew = Brew(recipe=recipe, brew_date=datetime.now(), owner=user)
        self.assertFalse(brew.recipe_editable)

        brew.clone_recipe_for_update()

        # The recipe for the brew should be different
        self.assertNotEqual(recipe.pk, brew.recipe.pk)
        self.assertTrue(brew.recipe_editable)
        self.assertNotEqual(recipe.slug, brew.recipe.slug)

        brew_recipe = BaseRecipe.objects.get(pk=brew.recipe.pk)
        self.assertIsNotNone(brew_recipe)
        self.assertTrue(hasattr(brew_recipe, 'extractrecipe'))
        self.assertFalse(hasattr(brew_recipe.extractrecipe, 'allgrainrecipe'))


        # BaseRecipe
        recipe = BaseRecipe.objects.get(pk=22)
        brew = Brew(recipe=recipe, brew_date=datetime.now(), owner=user)
        self.assertFalse(brew.recipe_editable)

        brew.clone_recipe_for_update()

        # The recipe for the brew should be different
        self.assertNotEqual(recipe.pk, brew.recipe.pk)
        self.assertTrue(brew.recipe_editable)
        self.assertNotEqual(recipe.slug, brew.recipe.slug)

        brew_recipe = BaseRecipe.objects.get(pk=brew.recipe.pk)
        self.assertIsNotNone(brew_recipe)
        self.assertFalse(hasattr(brew_recipe, 'extractrecipe'))

    def test_brew_save(self):
        recipe = AllGrainRecipe.objects.get(pk=19)
        user = User.objects.get_by_natural_key(self.user_two_name)
        brew = Brew(recipe=recipe, brew_date=datetime.now(), owner=user)

        self.assertEqual('', brew.slug)
        brew.save()
        self.assertNotEqual('', brew.slug)
        slug = '' + brew.slug

        # Make sure the slug doesn't change if the brew is saved again
        brew.bottle_date = datetime.today()
        brew.save()
        self.assertEqual(slug, brew.slug)

    def test_brew_get_estimated_ready_date(self):
        recipe = AllGrainRecipe.objects.get(pk=19)
        user = User.objects.get_by_natural_key(self.user_two_name)
        brew = Brew(recipe=recipe, brew_date=datetime.now(), owner=user)

        # No bottle date
        self.assertIsNone(brew.get_estimated_ready_date())

        brew.bottle_date = datetime(year=2014, month=1, day=1)
        # Conditioning time is days
        recipe.conditioning_time = 1
        recipe.conditioning_time_quantifier = 'd'
        estimated_ready_date = brew.get_estimated_ready_date()

        self.assertEqual(2014, estimated_ready_date.year)
        self.assertEqual(1, estimated_ready_date.month)
        self.assertEqual(2, estimated_ready_date.day)

        # weeks
        recipe.conditioning_time = 1
        recipe.conditioning_time_quantifier = 'w'
        estimated_ready_date = brew.get_estimated_ready_date()

        self.assertEqual(2014, estimated_ready_date.year)
        self.assertEqual(1, estimated_ready_date.month)
        self.assertEqual(8, estimated_ready_date.day)

        # year
        recipe.conditioning_time = 1
        recipe.conditioning_time_quantifier = 'y'
        estimated_ready_date = brew.get_estimated_ready_date()

        self.assertEqual(2015, estimated_ready_date.year)
        self.assertEqual(1, estimated_ready_date.month)
        self.assertEqual(1, estimated_ready_date.day)

        # Invalid conditioning time quantifier
        recipe.conditioning_time = 1
        recipe.conditioning_time_quantifier = 'xyz'
        estimated_ready_date = brew.get_estimated_ready_date()
        self.assertIsNone(estimated_ready_date)

    def test_brewnote_manager(self):
        recipe = AllGrainRecipe.objects.get(pk=19)
        user = User.objects.get_by_natural_key(self.user_two_name)
        brew = Brew(recipe=recipe, brew_date=datetime.now(), owner=user)
        brew.save()

        general_note = BrewNote(brew=brew, type=BrewNote.GENERAL, note='general note')
        mash_note = BrewNote(brew=brew, type=BrewNote.MASH, note='mash note')
        boil_note = BrewNote(brew=brew, type=BrewNote.BOIL, note='boil note')
        fermentation_note = BrewNote(brew=brew, type=BrewNote.FERMENTATION, note='fermentation note')
        appearance_note = BrewNote(brew=brew, type=BrewNote.APPEARANCE, note='appearance note')
        aroma_note = BrewNote(brew=brew, type=BrewNote.AROMA, note='aroma note')
        flavour_note = BrewNote(brew=brew, type=BrewNote.FLAVOUR, note='flavour note')
        feel_note = BrewNote(brew=brew, type=BrewNote.FEEL, note='feel note')
        overall_note = BrewNote(brew=brew, type=BrewNote.OVERALL, note='overall note')
        didnt_work_note = BrewNote(brew=brew, type=BrewNote.DIDNT_WORK, note='didnt work note')
        what_worked_note = BrewNote(brew=brew, type=BrewNote.WHAT_WORKED, note='what worked note')

        general_note.save()
        mash_note.save()
        boil_note.save()
        fermentation_note.save()
        appearance_note.save()
        aroma_note.save()
        flavour_note.save()
        feel_note.save()
        overall_note.save()
        didnt_work_note.save()
        what_worked_note.save()

        self.assertEqual(1, BrewNote.objects.general_set(brew).count())
        self.assertEqual(1, BrewNote.objects.mash_set(brew).count())
        self.assertEqual(1, BrewNote.objects.boil_set(brew).count())
        self.assertEqual(1, BrewNote.objects.fermentation_set(brew).count())
        self.assertEqual(1, BrewNote.objects.appearance_set(brew).count())
        self.assertEqual(1, BrewNote.objects.aroma_set(brew).count())
        self.assertEqual(1, BrewNote.objects.flavour_set(brew).count())
        self.assertEqual(1, BrewNote.objects.feel_set(brew).count())
        self.assertEqual(1, BrewNote.objects.overall_set(brew).count())
        self.assertEqual(1, BrewNote.objects.didnt_work_set(brew).count())
        self.assertEqual(1, BrewNote.objects.what_worked_set(brew).count())

    def test_brew_alcohol_by_volume(self):
        recipe = AllGrainRecipe.objects.get(pk=19)
        user = User.objects.get_by_natural_key(self.user_two_name)
        brew = Brew(recipe=recipe, brew_date=datetime.now(), owner=user)
        brew.save()

        gr = GravityReading(batch=brew, time=datetime(day=1, month=1, year=2015), value=1.085)
        gr.save()
        gr = GravityReading(batch=brew, time=datetime(day=7, month=1, year=2015), value=1.021)
        gr.save()

        abv = brew.alcohol_by_volume()

        self.assertEqual('8.4', format(abv, '.1f'))

    def test_brew_priming(self):
        recipe = AllGrainRecipe.objects.get(pk=19)
        user = User.objects.get_by_natural_key(self.user_two_name)
        brew = Brew(recipe=recipe, brew_date=datetime.now(), owner=user)
        brew.save()

        tr = TemperatureReading(batch=brew, time=datetime(day=1, month=1, year=2015), value=18)
        tr.save()
        tr = TemperatureReading(batch=brew, time=datetime(day=7, month=1, year=2015), value=16)
        tr.save()
        tr = TemperatureReading(batch=brew, time=datetime(day=1, month=1, year=2015), value=20)
        tr.save()
        tr = TemperatureReading(batch=brew, time=datetime(day=7, month=1, year=2015), value=6)
        tr.save()

        self.assertEqual('150.7', format(brew.priming_sucrose(), '.1f'))
        self.assertEqual('165.7', format(brew.priming_dextrose(), '.1f'))
        self.assertEqual('221.7', format(brew.priming_dme(), '.1f'))