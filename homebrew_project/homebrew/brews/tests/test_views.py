from datetime import datetime
import json

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test import TestCase

from brews.models import Brew, BrewNote, GravityReading, TemperatureReading
from recipes.models import AllGrainRecipe, BaseRecipe, ExtractRecipe


__author__ = 'sam'


class ViewTestCase(TestCase):
    fixtures = ['Users', 'SampleDB']

    def setUp(self):
        self.user_name = 'test_user'
        self.user_password = 'test_password'

        self.user_two_name = 'test_two'
        self.user_two_password = 'test_password'

        self.superuser_name = 'superuser'
        self.superuser_password = 'superpassword'
        super().setUp()

    def assertResponseOk(self, response):
        self.assertEqual(200, response.status_code)

    def test_brew_create_brew(self):
        create_brew_url = reverse('brew-create')

        # Not logged in should redirect to the login page.
        response = self.client.get(create_brew_url)
        # Redirect to login page
        self.assertRedirects(response, reverse('account_login') + '?next=' + create_brew_url)

        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(create_brew_url)
        self.assertResponseOk(response)
        self.assertTemplateUsed(response, 'brew/brew_create.html')

        # Interrogate the form to make sure the owner has been set in the initial data
        context = response.context_data
        self.assertIsNotNone(context['form'])
        form = context['form']
        self.assertIn('owner', form.initial)

        # is it the right owner?
        user = User.objects.get_by_natural_key(self.user_name)
        self.assertEqual(user.pk, form.initial['owner'].pk)

        # Test the initial data when using a recipe in the page parameter
        recipe = AllGrainRecipe.objects.get(pk=19)

        response = self.client.get(create_brew_url + '?recipe=19')
        self.assertResponseOk(response)

        context = response.context_data
        self.assertIsNotNone(context['form'])
        form = context['form']

        # Make sure that the style and recipe have been set in the form
        self.assertIn('style', form.initial)
        self.assertIn('recipe', form.initial)

        self.assertEqual(recipe.style_id, form.initial['style'])
        self.assertEqual(recipe.pk, form.initial['recipe'].pk)

        # Test a BaseRecipe
        recipe = BaseRecipe.objects.get(pk=22)

        response = self.client.get(create_brew_url + '?recipe=22')
        self.assertResponseOk(response)

        context = response.context_data
        self.assertIsNotNone(context['form'])
        form = context['form']

        # Make sure that the style and recipe have been set in the form
        self.assertIn('style', form.initial)
        self.assertIn('recipe', form.initial)

        self.assertEqual(recipe.style_id, form.initial['style'])
        self.assertEqual(recipe.pk, form.initial['recipe'].pk)

        # Test an ExtractRecipe
        recipe = ExtractRecipe.objects.get(pk=21)

        response = self.client.get(create_brew_url + '?recipe=21')
        self.assertResponseOk(response)

        context = response.context_data
        self.assertIsNotNone(context['form'])
        form = context['form']

        # Make sure that the style and recipe have been set in the form
        self.assertIn('style', form.initial)
        self.assertIn('recipe', form.initial)

        self.assertEqual(recipe.style_id, form.initial['style'])
        self.assertEqual(recipe.pk, form.initial['recipe'].pk)

    def test_brew_recipes_for_style(self):
        recipes_for_style_url = reverse('recipes-for-style')

        # Non-AJAX request should fail
        response = self.client.get(recipes_for_style_url)
        self.assertContains(response, 'Not Ajax or no GET')

        # AJAX but without the parameter
        response = self.client.get(recipes_for_style_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, 'Not Ajax or no GET')

        # AJAX but with the parameter, not logged in
        response = self.client.get(recipes_for_style_url, data={'style_id': 1}, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, 'Not Ajax or no GET')

        # AJAX but with the parameter, logged in
        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(recipes_for_style_url, data={'style_id': 1}, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, 'Mexican Cerveza')

        # Change request type to POST
        response = self.client.post(recipes_for_style_url, data={'style_id': 1}, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, 'Not Ajax or no GET')

    def test_brew_list_view(self):
        brew_list_url = reverse('brew-list')

        # Not logged in should redirect to the login page.
        response = self.client.get(brew_list_url)
        # Redirect to login page
        self.assertRedirects(response, reverse('account_login') + '?next=' + brew_list_url)

        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(brew_list_url)
        self.assertResponseOk(response)
        self.assertTemplateUsed(response, 'brew/brew_list.html')

        context = response.context_data

        # There should be an object called filter that is the form filter
        self.assertIsNotNone(context['filter'])
        # The table should exist too
        self.assertIsNotNone(context['table'])

        # Table should have nothing in there
        table = context['table']
        self.assertEqual(0, len(table.data))

        # Create a brew for the logged in user, then view the page again
        recipe = AllGrainRecipe.objects.get(pk=19)
        user = User.objects.get_by_natural_key(self.user_name)
        brew = Brew(recipe=recipe, brew_date=datetime.now(), owner=user)
        brew.save()

        response = self.client.get(brew_list_url)
        context = response.context_data
        self.assertIsNotNone(context['table'])
        table = context['table']
        self.assertEqual(1, len(table.data))

        # Create a brew for another user, then view the page again
        # The user should only be able to see their own brews
        user_two = User.objects.get_by_natural_key(self.user_two_name)
        brew_two = Brew(recipe=recipe, brew_date=datetime.now(), owner=user_two)
        brew_two.save()

        response = self.client.get(brew_list_url)
        context = response.context_data
        self.assertIsNotNone(context['table'])
        table = context['table']
        self.assertEqual(1, len(table.data))

    def test_brew_update_view(self):
        # Create the test brew
        recipe = AllGrainRecipe.objects.get(pk=19)
        user = User.objects.get_by_natural_key(self.user_name)
        brew = Brew(recipe=recipe, brew_date=datetime.now(), owner=user)
        brew.save()

        brew_update_url = reverse('brew-update', kwargs={'slug': brew.slug})

        # Not logged in should redirect to the login page.
        response = self.client.get(brew_update_url)
        # Redirect to login page
        self.assertRedirects(response, reverse('account_login') + '?next=' + brew_update_url)

        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(brew_update_url)
        self.assertResponseOk(response)
        self.assertTemplateUsed(response, 'brew/brew_update.html')

    def test_brew_detail_view(self):
        # Create the test brew
        recipe = AllGrainRecipe.objects.get(pk=19)
        user = User.objects.get_by_natural_key(self.user_name)
        brew = Brew(recipe=recipe, brew_date=datetime.now(), owner=user)
        brew.save()

        brew_detail_url = brew.get_absolute_url()

        # Not logged in
        response = self.client.get(brew_detail_url)
        self.assertRedirects(response, reverse('account_login') + '?next=' + brew_detail_url)

        # Logged in
        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(brew_detail_url)
        self.assertResponseOk(response)

        context = response.context_data
        self.assertIn('notes', context)
        self.assertEqual(len(BrewNote.NOTE_TYPES), len(context['notes']))

        self.client.logout()

        # As another user
        self.client.login(username=self.user_two_name, password=self.user_two_password)
        response = self.client.get(brew_detail_url)
        self.assertResponseOk(response)


    def test_brew_customise_recipe_view(self):
        # Create the test brew
        recipe = AllGrainRecipe.objects.get(pk=19)
        user = User.objects.get_by_natural_key(self.user_name)
        brew = Brew(recipe=recipe, brew_date=datetime.now(), owner=user)
        brew.save()

        recipe_customise_url = reverse('brew-customise', kwargs={'slug': brew.slug})
        # Not logged in should redirect to the login page.
        response = self.client.get(recipe_customise_url)
        # Redirect to login page
        self.assertRedirects(response, reverse('account_login') + '?next=' + recipe_customise_url)

        # Make sure the recipe hasn't been cloned
        self.assertFalse(brew.recipe_editable)

        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(recipe_customise_url)

        # Update the reference to the brew
        brew = Brew.objects.get(pk=brew.pk)

        # Make sure we've been redirected to the recipe update page
        self.assertRedirects(response, reverse('recipe-update', kwargs={'slug': brew.recipe.slug}))

        self.assertTrue(brew.recipe_editable)

    def test_brewnote_create_view(self):
        # Create the test brew
        recipe = AllGrainRecipe.objects.get(pk=19)
        user = User.objects.get_by_natural_key(self.user_name)
        brew = Brew(recipe=recipe, brew_date=datetime.now(), owner=user)
        brew.save()

        brew_note_url = reverse('brew-note-create', kwargs={'slug': brew.slug, 'note_type': BrewNote.BOIL})

        # Not logged in
        response = self.client.get(brew_note_url)
        self.assertRedirects(response, reverse('account_login') + '?next=' + brew_note_url)

        # Logged in as owner
        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(brew_note_url)
        self.assertResponseOk(response)
        self.client.logout()

        # Logged in; not owner
        self.client.login(username=self.user_two_name, password=self.user_two_password)
        response = self.client.get(brew_note_url)
        self.assertRedirects(response, reverse('home'))

    def test_brewnote_update_view(self):
        # Create the test brew
        recipe = AllGrainRecipe.objects.get(pk=19)
        user = User.objects.get_by_natural_key(self.user_name)
        brew = Brew(recipe=recipe, brew_date=datetime.now(), owner=user)
        brew.save()
        note = BrewNote(brew=brew, type=BrewNote.BOIL, note='boil note')
        note.save()

        brew_note_url = note.get_update_url()

        # Not logged in
        response = self.client.get(brew_note_url)
        self.assertRedirects(response, reverse('account_login') + '?next=' + brew_note_url)

        # Logged in as owner
        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(brew_note_url)
        self.assertResponseOk(response)
        self.client.logout()

        # Logged in; not owner
        self.client.login(username=self.user_two_name, password=self.user_two_password)
        response = self.client.get(brew_note_url)
        self.assertRedirects(response, reverse('home'))

    def test_brewnote_delete_view(self):
        # Create the test brew
        recipe = AllGrainRecipe.objects.get(pk=19)
        user = User.objects.get_by_natural_key(self.user_name)
        brew = Brew(recipe=recipe, brew_date=datetime.now(), owner=user)
        brew.save()
        note = BrewNote(brew=brew, type=BrewNote.BOIL, note='boil note')
        note.save()

        brew_note_url = note.get_delete_url()

        # Not logged in
        response = self.client.get(brew_note_url)
        self.assertRedirects(response, reverse('account_login') + '?next=' + brew_note_url)

        # Logged in as owner
        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(brew_note_url)
        self.assertResponseOk(response)
        self.client.logout()

        # Logged in; not owner
        self.client.login(username=self.user_two_name, password=self.user_two_password)
        response = self.client.get(brew_note_url)
        self.assertRedirects(response, reverse('home'))

    def test_reading_create_view(self):
        # Create the test brew
        recipe = AllGrainRecipe.objects.get(pk=19)
        user = User.objects.get_by_natural_key(self.user_name)
        brew = Brew(recipe=recipe, brew_date=datetime.now(), owner=user)
        brew.save()

        reading_url = reverse('brew-detail-gravity-add', kwargs={'slug': brew.slug})

        # Not logged in
        response = self.client.get(reading_url)
        self.assertRedirects(response, reverse('account_login') + '?next=' + reading_url)

        # Logged in as owner
        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(reading_url)
        self.assertResponseOk(response)

        form = response.context_data['form']
        self.assertIn('batch', form.initial)
        self.assertEqual(brew, form.initial['batch'])

        self.client.logout()

        # Logged in; not owner
        self.client.login(username=self.user_two_name, password=self.user_two_password)
        response = self.client.get(reading_url)
        self.assertRedirects(response, reverse('home'))

    def test_reading_update_view(self):
        def test_url(url):
            # Not logged in
            response = self.client.get(url)
            self.assertRedirects(response, reverse('account_login') + '?next=' + url)

            # Logged in as owner
            self.client.login(username=self.user_name, password=self.user_password)
            response = self.client.get(url)
            self.assertResponseOk(response)
            self.client.logout()

            # Logged in; not owner
            self.client.login(username=self.user_two_name, password=self.user_two_password)
            response = self.client.get(url)
            self.assertRedirects(response, reverse('home'))
            self.client.logout()

        # Create the test brew
        recipe = AllGrainRecipe.objects.get(pk=19)
        user = User.objects.get_by_natural_key(self.user_name)
        brew = Brew(recipe=recipe, brew_date=datetime.now(), owner=user)
        brew.save()
        gravity_reading = GravityReading(batch=brew, time=datetime.now(), value=1.021)
        gravity_reading.save()

        test_url(gravity_reading.get_update_url())

        temp_reading = TemperatureReading(batch=brew, time=datetime.now(), value=22.02)
        temp_reading.save()

        test_url(temp_reading.get_update_url())

    def test_reading_delete_view(self):
        def test_url(url):
            # Not logged in
            response = self.client.get(url)
            self.assertRedirects(response, reverse('account_login') + '?next=' + url)

            # Logged in as owner
            self.client.login(username=self.user_name, password=self.user_password)
            response = self.client.get(url)
            self.assertResponseOk(response)
            self.client.logout()

            # Logged in; not owner
            self.client.login(username=self.user_two_name, password=self.user_two_password)
            response = self.client.get(url)
            self.assertRedirects(response, reverse('home'))
            self.client.logout()

        # Create the test brew
        recipe = AllGrainRecipe.objects.get(pk=19)
        user = User.objects.get_by_natural_key(self.user_name)
        brew = Brew(recipe=recipe, brew_date=datetime.now(), owner=user)
        brew.save()
        gravity_reading = GravityReading(batch=brew, time=datetime.now(), value=1.021)
        gravity_reading.save()

        test_url(gravity_reading.get_delete_url())

        temp_reading = TemperatureReading(batch=brew, time=datetime.now(), value=22.02)
        temp_reading.save()

        test_url(temp_reading.get_delete_url())

    def test_recipes_for_style(self):
        test_url = reverse('recipes-for-style')

        # Not logged in, AJAX
        response = self.client.get(test_url, data={'style_id': '1'}, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(200, response.status_code)
        self.assertIsNotNone(response.content)
        self.assertEqual('application/json', response['Content-Type'])

        json_response = json.loads(response.content.decode('utf-8'))
        self.assertIn('error', json_response.keys())

        # Logged in, not AJAX
        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(test_url, data={'style_id': '1'})
        self.assertEqual(200, response.status_code)
        self.assertIsNotNone(response.content)
        self.assertEqual('application/json', response['Content-Type'])

        json_response = json.loads(response.content.decode('utf-8'))
        self.assertIn('error', json_response.keys())

        # Logged in, AJAX, no style_id
        response = self.client.get(test_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(200, response.status_code)
        self.assertIsNotNone(response.content)
        self.assertEqual('application/json', response['Content-Type'])

        json_response = json.loads(response.content.decode('utf-8'))
        self.assertIn('error', json_response.keys())

        # Logged in, AJAX, style_id
        response = self.client.get(test_url, data={'style_id': '1'}, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(200, response.status_code)
        self.assertIsNotNone(response.content)
        self.assertEqual('application/json', response['Content-Type'])

        json_response = json.loads(response.content.decode('utf-8'))
        self.assertEqual(4, len(json_response))
