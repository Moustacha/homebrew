# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('brews', '0003_auto_20150124_1644'),
        ('recipes', '0007_auto_20150214_1349'),
    ]

    operations = [
        migrations.AlterField(
            model_name='brew',
            name='recipe',
            field=models.ForeignKey(to='recipes.BaseRecipe'),
            preserve_default=True,
        ),
    ]
