# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime

from django.db import models, migrations


def notes_forward(apps, schema_editor):
    Brew = apps.get_model('brews', 'Brew')
    BrewNote = apps.get_model('brews', 'BrewNote')

    brew_notes = list()
    for b in Brew.objects.all():
        brew_notes.append(BrewNote(brew=b, type=1, note=b.notes, created_date=datetime.now()))

    BrewNote.objects.bulk_create(brew_notes)


def notes_reverse(apps, schema_editor):
    BrewNote = apps.get_model('brews', 'BrewNote')

    for bn in BrewNote.objects.all():
        brew = bn.brew
        brew.notes = '{}\n{}'.format(brew.notes, bn.note)

        brew.save()


class Migration(migrations.Migration):
    dependencies = [
        ('brews', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BrewNote',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('type', models.IntegerField(
                    choices=[(5, 'Appearance'), (6, 'Aroma'), (3, 'Boil'), (8, 'Feel'), (4, 'Fermentation'),
                             (7, 'Flavour'), (1, 'General'), (2, 'Mash'), (9, 'Overall'), (10, "What Didn't Work"),
                             (11, 'What Worked')])),
                ('note', models.TextField()),
                ('created_date', models.DateField(auto_now_add=True)),
                ('brew', models.ForeignKey(to='brews.Brew')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RunPython(notes_forward, notes_reverse),
        migrations.RemoveField(
            model_name='brew',
            name='notes',
        ),
    ]
