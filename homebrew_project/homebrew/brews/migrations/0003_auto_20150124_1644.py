# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('brews', '0002_auto_20150109_1914'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='gravityreading',
            options={'ordering': ('time',)},
        ),
        migrations.AlterModelOptions(
            name='temperaturereading',
            options={'ordering': ('time',)},
        ),
    ]
