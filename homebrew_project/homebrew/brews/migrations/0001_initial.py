# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('recipes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Brew',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('brew_date', models.DateField()),
                ('notes', models.TextField(blank=True, null=True)),
                ('bottle_date', models.DateField(blank=True, null=True)),
                ('recipe_editable', models.BooleanField(default=False, editable=False)),
                ('slug', models.SlugField(editable=False)),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('recipe', models.ForeignKey(to='recipes.Recipe')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GravityReading',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('time', models.DateTimeField()),
                ('value', models.DecimalField(decimal_places=10, max_digits=19)),
                ('batch', models.ForeignKey(to='brews.Brew')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TemperatureReading',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('time', models.DateTimeField()),
                ('value', models.DecimalField(decimal_places=10, max_digits=19)),
                ('batch', models.ForeignKey(to='brews.Brew')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
