from django_tables2 import tables, LinkColumn, A, Column

from brews.models import Brew


__author__ = 'sam'


class BrewTable(tables.Table):
    recipe = LinkColumn('brew-detail', kwargs={'slug': A('slug')}, verbose_name='Recipe')
    brew_date = Column('Brew Date')

    class Meta:
        model = Brew
        fields = ('recipe', 'brew_date', )
        order_by = ('-brew_date', 'recipe', )
        attrs = {'class': 'table table-hover'}
        empty_text = 'No brews could be found. Please try changing your search parameters.'
