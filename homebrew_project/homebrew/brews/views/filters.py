from bootstrap3_datetime.widgets import DateTimePicker
import django_filters as filters

from brews.models import Brew


__author__ = 'sam'


class BrewFilter(filters.FilterSet):
    recipe__name = filters.CharFilter(lookup_type='icontains')
    brew_date = filters.DateFilter(widget=DateTimePicker(options={'pickTime': False}), localize=True)

    class Meta:
        model = Brew
        fields = ('recipe__name', 'brew_date', )


# class BrewFilterFormHelper(FormHelper):
#     form_method = 'GET'
#     layout = Layout(
#         'recipe__name',
#         'brew_date',
#         Submit('submit', 'Search'),
#         HTML('<a href="{% url "brew-list" %}?clear=true" class="btn btn-default">Reset</a>')
#     )
