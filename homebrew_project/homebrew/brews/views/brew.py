from datetime import datetime
import json
from calendar import Calendar

from django.core.urlresolvers import reverse
from django import forms
from django.http import HttpResponse
from django.views.generic import CreateView, RedirectView, UpdateView, DeleteView, DetailView, MonthArchiveView, \
    YearArchiveView

from permissionsx.contrib.django.views import DjangoViewMixin

from brews.forms import BrewForm, BrewNoteForm
from brews.models import Brew, BrewNote
from brews.permissions import BrewOwnerPermissions
from brews.views.filters import BrewFilter
from brews.views.tables import BrewTable
from homebrew.permissions import UserIsAuthenticatedPermissions
from recipes.models import Style, BaseRecipe
from recipes.views.generic import PagedFilteredTableView, RememberPageParametersMixin, FormViewMessageMixin

__author__ = 'sam'


class BrewCreateView(FormViewMessageMixin, DjangoViewMixin, CreateView):
    model = Brew
    form_class = BrewForm
    template_name = 'brew/brew_create.html'
    # owner = None

    # Authorisation
    permissions = UserIsAuthenticatedPermissions()

    def get_initial(self):
        initial = super().get_initial()
        initial['owner'] = self.request.user

        if 'recipe' in self.request.GET:
            recipe = BaseRecipe.objects.get(pk=self.request.GET['recipe'])

            initial['style'] = recipe.style_id
            initial['recipe'] = recipe
        return initial

    def get_form(self, form_class):
        form = super().get_form(form_class)
        form.fields['style'].choices = Style.objects.all().values_list('id', 'name')
        form.fields['recipe'].queryset = BaseRecipe.objects.public_or_owner(self.request.user).order_by('name')

        # form.helper['bottle_date'].wrap(Field, type='hidden')
        return form


class JSONResponse(HttpResponse):
    def __init__(self, data):
        super(JSONResponse, self).__init__(
            json.dumps(data), content_type='application/json')


def recipes_for_style(request):
    if request.is_ajax() and request.GET and 'style_id' in request.GET and request.user.is_authenticated():
        objs = BaseRecipe.objects.public_or_owner(request.user).filter(style=request.GET['style_id']).order_by('name') \
            .values_list('id', 'name')

        return JSONResponse([{'id': o[0], 'name': o[1]}
                             for o in objs])
    else:
        return JSONResponse({'error': 'Not Ajax or no GET'})


class CustomiseRecipeRedirectView(DjangoViewMixin, RedirectView):
    permanent = False

    # Authorisation
    permissions = BrewOwnerPermissions()

    def get_redirect_url(self, *args, **kwargs):
        # Get the brew, clone the recipe, then redirect to the new recipe
        brew = Brew.objects.get(slug=kwargs['slug'])
        if not brew.recipe_editable:
            brew.clone_recipe_for_update()

        return reverse('recipe-update', kwargs={'slug': brew.recipe.slug})


class BrewListView(DjangoViewMixin, RememberPageParametersMixin, PagedFilteredTableView):
    template_name = 'brew/brew_list.html'
    model = Brew
    table_class = BrewTable
    filter_class = BrewFilter
    # formhelper_class = BrewFilterFormHelper

    url_name = 'brew-list'

    # Authorisation
    permissions = UserIsAuthenticatedPermissions()

    def get(self, request, *args, **kwargs):
        self.queryset = Brew.objects.for_user(request.user)
        return super().get(request, *args, **kwargs)


class BrewUpdateView(FormViewMessageMixin, DjangoViewMixin, UpdateView):
    model = Brew
    form_class = BrewForm
    template_name = 'brew/brew_update.html'
    # owner = None

    # Authorisation
    permissions = BrewOwnerPermissions()


class BrewDeleteView(FormViewMessageMixin, DjangoViewMixin, DeleteView):
    template_name = 'brew/brew_delete.html'
    model = Brew
    success_message = '{} successfully deleted'
    failure_message = 'Error while deleting {}'

    # Authorisation
    permissions = BrewOwnerPermissions()

    def get_success_url(self):
        return reverse('brew-list')


class BrewDetailView(DjangoViewMixin, DetailView):
    model = Brew
    queryset = Brew.objects.all().select_related(
        'recipe__style', 'recipe__yeast',
        'recipe__yeast__lab')

    # Authorisation
    permissions = UserIsAuthenticatedPermissions()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        brew = context['brew']

        notes = list()

        all_notes = list(brew.brewnote_set.all())

        for note_code, note_name in sorted(BrewNote.NOTE_TYPES, key=lambda note: note[0]):
            notes.append(
                {'code': note_code, 'name': note_name, 'note_set': [x for x in all_notes if x.type == note_code]})

        context['notes'] = notes

        return context


class ReadingCreateView(FormViewMessageMixin, DjangoViewMixin, CreateView):
    template_name = 'brew/reading/create.html'

    # Authorisation
    permissions = BrewOwnerPermissions()

    def get_initial(self):
        initial = super().get_initial()
        initial['batch'] = Brew.objects.get(slug=self.kwargs['slug'])
        return initial


class ReadingUpdateView(FormViewMessageMixin, DjangoViewMixin, UpdateView):
    template_name = 'brew/reading/update.html'

    # Authorisation
    permissions = BrewOwnerPermissions()


class ReadingDeleteView(FormViewMessageMixin, DjangoViewMixin, DeleteView):
    template_name = 'brew/reading/delete.html'
    success_message = '{} successfully deleted'
    failure_message = 'Error while deleting {}'
    brew_slug = None

    # Authorisation
    permissions = BrewOwnerPermissions()

    def post(self, request, *args, **kwargs):
        self.brew_slug = kwargs.get(self.slug_url_kwarg)
        return super().post(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('brew-detail', kwargs={self.slug_url_kwarg: self.brew_slug})


class BrewNoteCreateView(FormViewMessageMixin, DjangoViewMixin, CreateView):
    template_name = 'brew/brew_note_create.html'
    model = BrewNote
    form_class = BrewNoteForm

    # Authorisation
    permissions = BrewOwnerPermissions()

    def get_initial(self):
        initial = super().get_initial()

        initial['type'] = self.kwargs['note_type']
        initial['brew'] = Brew.objects.get(slug=self.kwargs['slug'])

        return initial


class BrewNoteUpdateView(FormViewMessageMixin, DjangoViewMixin, UpdateView):
    template_name = 'brew/brew_note_update.html'
    model = BrewNote
    form_class = BrewNoteForm

    # Authorisation
    permissions = BrewOwnerPermissions()


class BrewNoteDeleteView(FormViewMessageMixin, DjangoViewMixin, DeleteView):
    template_name = 'brew/brew_note_delete.html'
    model = BrewNote
    success_message = '{} successfully deleted'
    failure_message = 'Error while deleting {}'

    # Authorisation
    permissions = BrewOwnerPermissions()

    def get_success_url(self):
        return reverse('brew-detail', kwargs={'slug': self.get_object().brew.slug})


class BrewMonthArchiveView(MonthArchiveView):
    template_name = 'brew/brew_archive_month.html'
    date_field = 'brew_date'
    allow_future = True
    allow_empty = True

    def get_queryset(self):
        return Brew.objects.for_user(self.request.user).select_related('recipe__name')

    def get_calendar(self):
        cal = Calendar()
        brews = self.get_dated_items()[1]
        month_calendar = cal.monthdatescalendar(int(self.get_year()), int(self.get_month()))

        month_calendar2 = list()
        for week in month_calendar:
            week2 = list()
            for day in week:
                week2.append((day, [x for x in brews if x.brew_date.day == day.day]))
            month_calendar2.append(week2)

        return month_calendar2

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['calendar'] = self.get_calendar()
        return context


class BrewYearArchiveView(YearArchiveView):
    template_name = 'brew/brew_archive_year.html'
    date_field = 'brew_date'
    allow_future = True
    allow_empty = True

    def get_queryset(self):
        return Brew.objects.for_user(self.request.user)


class BrewArchiveRedirectView(RedirectView):
    permanent = True

    def get_redirect_url(self, *args, **kwargs):
        now = datetime.now()
        return reverse('brew-archive-year', kwargs={'year': now.year})
