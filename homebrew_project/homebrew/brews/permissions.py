from brews.models import Brew
from recipes.permissions import ObjectOwnerPermissions

__author__ = 'sam'


class BrewOwnerPermissions(ObjectOwnerPermissions):
    model_class = Brew
