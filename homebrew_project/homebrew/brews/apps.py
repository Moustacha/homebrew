__author__ = 'sam'

from django.apps import AppConfig


class BrewsConfig(AppConfig):
    name = 'brews'
    verbose_name = "Homebrew Journal"
