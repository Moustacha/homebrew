from django.conf.urls import patterns, url

from brews.forms import GravityReadingForm, TemperatureReadingForm
from brews.models import GravityReading, TemperatureReading
from brews.views.brew import BrewCreateView, BrewDetailView, ReadingCreateView, ReadingUpdateView, ReadingDeleteView, \
    CustomiseRecipeRedirectView, BrewListView, BrewUpdateView, BrewDeleteView, BrewNoteCreateView, BrewNoteUpdateView, \
    BrewNoteDeleteView, recipes_for_style, BrewMonthArchiveView, BrewYearArchiveView, BrewArchiveRedirectView

__author__ = 'sam'

urlpatterns = patterns(
    '',
    url(r'^create$', BrewCreateView.as_view(), name='brew-create'),
    url(r'^create/recipes_for_style/$', recipes_for_style, name='recipes-for-style'),
    url(r'^view/(?P<slug>[\w-]+)/$', BrewDetailView.as_view(template_name='brew/brew_detail.html'),
        name='brew-detail'),


    url(r'^view/(?P<slug>[\w-]+)/gravity_readings/add/$',
        ReadingCreateView.as_view(model=GravityReading, form_class=GravityReadingForm),
        name='brew-detail-gravity-add'),
    url(r'^view/(?P<slug>[\w-]+)/temperature_readings/add/$',
        ReadingCreateView.as_view(model=TemperatureReading, form_class=TemperatureReadingForm),
        name='brew-detail-temp-add'),
    url(r'^view/(?P<slug>[\w-]+)/gravity_readings/update/(?P<pk>\d+)/$',
        ReadingUpdateView.as_view(model=GravityReading, form_class=GravityReadingForm),
        name='brew-detail-gravity-update'),
    url(r'^view/(?P<slug>[\w-]+)/temperature_readings/update/(?P<pk>\d+)/$',
        ReadingUpdateView.as_view(model=TemperatureReading, form_class=TemperatureReadingForm),
        name='brew-detail-temp-update'),
    url(r'^view/(?P<slug>[\w-]+)/gravity_readings/delete/(?P<pk>\d+)/$',
        ReadingDeleteView.as_view(model=GravityReading),
        name='brew-detail-gravity-delete'),
    url(r'^view/(?P<slug>[\w-]+)/temperature_readings/delete/(?P<pk>\d+)/$',
        ReadingDeleteView.as_view(model=TemperatureReading),
        name='brew-detail-temp-delete'),


    url(r'^view/(?P<slug>[\w-]+)/customise_recipe/', CustomiseRecipeRedirectView.as_view(), name='brew-customise'),
    url(r'^list/$', BrewListView.as_view(), name='brew-list'),
    url(r'^update/(?P<slug>[\w-]+)/$', BrewUpdateView.as_view(), name='brew-update'),
    url(r'^delete/(?P<slug>[\w-]+)/$', BrewDeleteView.as_view(), name='brew-delete'),
    url(r'^view/(?P<slug>[\w-]+)/notes/create/(?P<note_type>\d+)/$', BrewNoteCreateView.as_view(), name='brew-note-create'),
    url(r'^view/(?P<slug>[\w-]+)/notes/edit/(?P<pk>\d+)/$', BrewNoteUpdateView.as_view(), name='brew-note-edit'),
    url(r'^view/(?P<slug>[\w-]+)/notes/delete/(?P<pk>\d+)/$', BrewNoteDeleteView.as_view(), name='brew-note-delete'),

    url(r'^archive/$', BrewArchiveRedirectView.as_view(),
        name='brew-archive'),
    url(r'^archive/(?P<year>\d{4})/$', BrewYearArchiveView.as_view(),
        name='brew-archive-year'),
    url(r'^archive/(?P<year>\d{4})/(?P<month>\d+)/$', BrewMonthArchiveView.as_view(month_format='%m'),
        name='brew-archive-month'),
)
