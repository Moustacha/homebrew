from bootstrap3_datetime.widgets import DateTimePicker
from django import forms
from django.utils import formats
from django.forms.utils import ErrorList

from brews.models import Brew, GravityReading, TemperatureReading, BrewNote

from markdownx.fields import MarkdownxFormField


__author__ = 'sam'


class BrewForm(forms.ModelForm):
    # helper = FormHelper()

    style = forms.ChoiceField(choices=(), required=False)
    brew_date = forms.DateField(widget=DateTimePicker(options={'pickTime': False}), localize=True)
    bottle_date = forms.DateField(widget=DateTimePicker(options={'pickTime': False}), localize=True, required=False)

    class Meta:
        model = Brew
        fields = ['recipe', 'brew_date', 'bottle_date', 'owner']
        widgets = {
            'owner': forms.HiddenInput()
        }

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
                 label_suffix=None, empty_permitted=False, instance=None):
        super().__init__(data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, instance)

        # Only displaying dates, no times, so set the format key to only date input formats, for localisation.
        self.fields['brew_date'].widget.format_key = 'DATE_INPUT_FORMATS'
        self.fields['bottle_date'].widget.format_key = 'DATE_INPUT_FORMATS'


class BaseReadingForm(forms.ModelForm):

    time = forms.DateTimeField(widget=DateTimePicker(), localize=True)

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
                 label_suffix=None, empty_permitted=False, instance=None):
        super().__init__(data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, instance)

        # Get this locale's short datetime format, convert it to the JS format with DateTimePicker's class method
        # and assign it to the format options.
        widget_format = DateTimePicker.conv_datetime_format_py2js(formats.get_format('DATETIME_INPUT_FORMATS')[0])
        self.fields['time'].widget.options['format'] = widget_format


class GravityReadingForm(BaseReadingForm):
    value = forms.CharField(widget=forms.NumberInput(attrs={'step': '0.001'}), label='Specific Gravity')

    class Meta:
        model = GravityReading
        fields = ['batch', 'time', 'value']
        widgets = {
            'batch': forms.HiddenInput()
        }


class TemperatureReadingForm(BaseReadingForm):
    value = forms.CharField(widget=forms.NumberInput(attrs={'step': '0.1', 'addon_after': '°C'}), label='Temperature', help_text='In °C')

    class Meta:
        model = TemperatureReading
        fields = ['batch', 'time', 'value']
        widgets = {
            'batch': forms.HiddenInput()
        }

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
                 label_suffix=None, empty_permitted=False, instance=None):
        super().__init__(data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, instance)

        # self.helper[2].wrap(AppendedText, text='°C') # value field


class BrewNoteForm(forms.ModelForm):
    
    note = MarkdownxFormField()

    class Meta:
        model = BrewNote
        fields = ['brew', 'type', 'note']
        widgets = {
            'brew': forms.HiddenInput(),
            'type': forms.HiddenInput(),
        }
