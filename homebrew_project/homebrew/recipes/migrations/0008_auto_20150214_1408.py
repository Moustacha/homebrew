# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0007_auto_20150214_1349'),
        ('brews', '0004_auto_20150214_1349'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Recipe',
        ),
    ]
