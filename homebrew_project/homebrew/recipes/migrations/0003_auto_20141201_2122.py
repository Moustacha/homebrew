# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0002_auto_20141127_1026'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Brand',
            new_name='Lab',
        ),
        migrations.AlterModelOptions(
            name='yeast',
            options={'ordering': ('lab', 'name')},
        ),
        migrations.RenameField(
            model_name='yeast',
            old_name='brand',
            new_name='lab'
        ),
        migrations.AlterField(
            model_name='yeast',
            name='lab',
            field=models.ForeignKey(to='recipes.Lab', on_delete=django.db.models.deletion.PROTECT, help_text='Brand/Manufacturer/Lab'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='fermentable',
            name='lovibond_rating',
            field=models.DecimalField(max_digits=10, decimal_places=2, validators=[django.core.validators.MinValueValidator(0)]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recipe',
            name='boil_liquor',
            field=models.DecimalField(max_digits=10, decimal_places=2, default=0, help_text='Leave as 0 to calculate'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recipe',
            name='expected_final_gravity',
            field=models.DecimalField(decimal_places=3, max_digits=4, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(2)], default=0, help_text='Leave as 0 to calculate'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recipe',
            name='mash_liquor',
            field=models.DecimalField(max_digits=10, decimal_places=2, default=0, help_text='Leave as 0 to calculate'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recipe',
            name='original_gravity',
            field=models.DecimalField(decimal_places=3, max_digits=4, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(2)], default=0, help_text='Leave as 0 to calculate'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recipehop',
            name='ibu',
            field=models.DecimalField(decimal_places=1, max_digits=3, blank=True, default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recipehop',
            name='quantity',
            field=models.IntegerField(blank=True, default=0, help_text='In grams'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='yeast',
            unique_together=set([('name', 'lab')]),
        ),
    ]
