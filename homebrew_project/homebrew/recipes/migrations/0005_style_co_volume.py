# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core import serializers

from django.db import models, migrations


def co_volume(apps, schema_editor):
    with open('recipes/fixtures/Style.json') as style_file:
        for obj in serializers.deserialize('json', style_file):
            obj.save()


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0004_auto_20141203_2107'),
    ]

    operations = [
        migrations.AddField(
            model_name='style',
            name='co_volume',
            field=models.DecimalField(help_text='Volumes of CO₂ per volume of beer', default=2, max_digits=3, decimal_places=2),
            preserve_default=False,
        ),
        migrations.RunPython(co_volume)
    ]
