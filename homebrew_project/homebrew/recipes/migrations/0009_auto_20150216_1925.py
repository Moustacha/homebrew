# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0008_auto_20150214_1408'),
    ]

    operations = [
        migrations.AddField(
            model_name='extractrecipe',
            name='steep_liquor',
            field=models.DecimalField(help_text='Leave as 0 to calculate', max_digits=10, default=0, blank=True, null=True, decimal_places=2),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='extractrecipe',
            name='steep_temperature',
            field=models.IntegerField(blank=True, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='extractrecipe',
            name='steep_time',
            field=models.IntegerField(help_text='Time in minutes', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='recipegrain',
            name='steeped',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
