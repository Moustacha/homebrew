# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0009_auto_20150216_1925'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fermentable',
            name='type',
            field=models.CharField(choices=[('MALTGR', 'Malted Grain'), ('MALTEX', 'Malted Extract'), ('HOPMALTEX', 'Hopped Malted Extract'), ('ADJUNCT', 'Adjunct'), ('SUGAR', 'Sugar')], max_length=20),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='style',
            name='co_volume',
            field=models.DecimalField(help_text='Suggested volumes of CO₂ per volume of beer', default=2, max_digits=3, decimal_places=2),
            preserve_default=True,
        ),
        migrations.AlterIndexTogether(
            name='baserecipe',
            index_together=set([('public', 'owner')]),
        ),
        migrations.AlterIndexTogether(
            name='fermentable',
            index_together=set([('public', 'owner')]),
        ),
        migrations.AlterIndexTogether(
            name='hop',
            index_together=set([('public', 'owner')]),
        ),
        migrations.AlterIndexTogether(
            name='other',
            index_together=set([('public', 'owner')]),
        ),
    ]
