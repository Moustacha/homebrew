# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
import django.core.validators
from django.conf import settings


def recipe_forwards(apps, schema_editor):
    Recipe = apps.get_model('recipes', 'Recipe')
    AllGrainRecipe = apps.get_model('recipes', 'AllGrainRecipe')

    for r in Recipe.objects.all():
        fields = r.__dict__.copy()
        # Remove _state key from fields
        fields.pop('_state', None)

        agr = AllGrainRecipe(**fields)
        agr.save()


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('recipes', '0006_auto_20150213_1652'),
    ]

    operations = [
        migrations.CreateModel(
            name='BaseRecipe',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField(blank=True, null=True)),
                ('target_volume', models.IntegerField(help_text='Target volume of beer in L')),
                ('original_gravity', models.DecimalField(validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(2)], help_text='Leave as 0 to calculate', default=0, decimal_places=3, max_digits=4)),
                ('expected_final_gravity', models.DecimalField(validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(2)], help_text='Leave as 0 to calculate', default=0, decimal_places=3, max_digits=4)),
                ('fermentation_temp', models.IntegerField()),
                ('conditioning_time', models.IntegerField()),
                ('conditioning_time_quantifier', models.CharField(max_length=1, choices=[('d', 'Days'), ('w', 'Weeks'), ('y', 'Years')])),
                ('conditioning_temp', models.IntegerField()),
                ('public', models.BooleanField(help_text='Do you want other people to be able to view this recipe?', default=False)),
                ('slug', models.SlugField(editable=False)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('last_updated_date', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ExtractRecipe',
            fields=[
                ('baserecipe_ptr', models.OneToOneField(serialize=False, parent_link=True, to='recipes.BaseRecipe', primary_key=True, auto_created=True)),
                ('boil_liquor', models.DecimalField(max_digits=10, help_text='Leave as 0 to calculate', default=0, decimal_places=2)),
                ('boil_time', models.IntegerField(help_text='Time in minutes')),
            ],
            options={
            },
            bases=('recipes.baserecipe',),
        ),
        migrations.CreateModel(
            name='AllGrainRecipe',
            fields=[
                ('extractrecipe_ptr', models.OneToOneField(serialize=False, parent_link=True, to='recipes.ExtractRecipe', primary_key=True, auto_created=True)),
                ('mash_liquor', models.DecimalField(max_digits=10, help_text='Leave as 0 to calculate', default=0, decimal_places=2)),
                ('mash_time', models.IntegerField(help_text='Time in minutes')),
                ('mash_temperature', models.IntegerField()),
            ],
            options={
            },
            bases=('recipes.extractrecipe',),
        ),
        migrations.AddField(
            model_name='extractrecipe',
            name='hops',
            field=models.ManyToManyField(through='recipes.RecipeHop', to='recipes.Hop'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='baserecipe',
            name='cloned_from',
            field=models.ForeignKey(blank=True, editable=False, to='recipes.BaseRecipe', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='baserecipe',
            name='fermentables',
            field=models.ManyToManyField(through='recipes.RecipeGrain', to='recipes.Fermentable'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='baserecipe',
            name='others',
            field=models.ManyToManyField(through='recipes.RecipeOther', to='recipes.Other'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='baserecipe',
            name='owner',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='baserecipe',
            name='style',
            field=models.ForeignKey(to='recipes.Style', on_delete=django.db.models.deletion.PROTECT),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='baserecipe',
            name='yeast',
            field=models.ForeignKey(to='recipes.Yeast', on_delete=django.db.models.deletion.PROTECT),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recipegrain',
            name='recipe',
            field=models.ForeignKey(to='recipes.BaseRecipe'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recipehop',
            name='recipe',
            field=models.ForeignKey(to='recipes.ExtractRecipe'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recipeother',
            name='recipe',
            field=models.ForeignKey(to='recipes.BaseRecipe'),
            preserve_default=True,
        ),
        migrations.RunPython(recipe_forwards),
    ]
