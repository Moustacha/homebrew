# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from recipes.models import AllGrainRecipe


def recipe_forwards(apps, schema_editor):
    for r in AllGrainRecipe.objects.all():
        r.calculate_fields(only_empty=True)
        r.save()


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0010_auto_20150302_1729'),
    ]

    operations = [
        migrations.AddField(
            model_name='allgrainrecipe',
            name='mash_thickness',
            field=models.DecimalField(default=2.1, max_digits=10, help_text='The thickness of the mash, in L/kg', decimal_places=2),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='allgrainrecipe',
            name='sparge_volume',
            field=models.DecimalField(default=0, max_digits=10, help_text='Leave as 0 to calculate', decimal_places=2),
            preserve_default=True,
        ),
        migrations.RunPython(recipe_forwards),
    ]
