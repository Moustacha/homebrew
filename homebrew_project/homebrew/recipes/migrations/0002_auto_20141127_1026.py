# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fermentable',
            name='lovibond_rating',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='recipe',
            name='boil_liquor',
            field=models.DecimalField(decimal_places=2, max_digits=10, default=0),
        ),
        migrations.AlterField(
            model_name='recipe',
            name='expected_final_gravity',
            field=models.DecimalField(decimal_places=3, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(2)], default=0, max_digits=4),
        ),
        migrations.AlterField(
            model_name='recipe',
            name='mash_liquor',
            field=models.DecimalField(decimal_places=2, max_digits=10, default=0),
        ),
        migrations.AlterField(
            model_name='recipe',
            name='original_gravity',
            field=models.DecimalField(decimal_places=3, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(2)], default=0, max_digits=4),
        ),
    ]
