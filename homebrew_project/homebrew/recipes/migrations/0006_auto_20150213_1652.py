# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import recipes.models


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0005_style_co_volume'),
    ]

    operations = [
        migrations.CreateModel(
            name='LowercaseTag',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(unique=True, verbose_name='Name', max_length=100)),
                ('slug', models.SlugField(unique=True, verbose_name='Slug', max_length=100)),
            ],
            options={
                'verbose_name': 'Tag',
                'verbose_name_plural': 'Tags',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TaggedFermentable',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('content_object', models.ForeignKey(to='recipes.Fermentable')),
                ('tag', models.ForeignKey(to='recipes.LowercaseTag', related_name='recipes_taggedfermentable_items')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TaggedHop',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('content_object', models.ForeignKey(to='recipes.Hop')),
                ('tag', models.ForeignKey(to='recipes.LowercaseTag', related_name='recipes_taggedhop_items')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TaggedOther',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('content_object', models.ForeignKey(to='recipes.Other')),
                ('tag', models.ForeignKey(to='recipes.LowercaseTag', related_name='recipes_taggedother_items')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TaggedYeast',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('content_object', models.ForeignKey(to='recipes.Yeast')),
                ('tag', models.ForeignKey(to='recipes.LowercaseTag', related_name='recipes_taggedyeast_items')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='fermentable',
            name='tags',
            field=recipes.models.FixedTaggableManager(blank=True, to='recipes.LowercaseTag', help_text='A comma-separated list of tags.', verbose_name='Tags', through='recipes.TaggedFermentable'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='hop',
            name='tags',
            field=recipes.models.FixedTaggableManager(blank=True, to='recipes.LowercaseTag', help_text='A comma-separated list of tags.', verbose_name='Tags', through='recipes.TaggedHop'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='other',
            name='tags',
            field=recipes.models.FixedTaggableManager(blank=True, to='recipes.LowercaseTag', help_text='A comma-separated list of tags.', verbose_name='Tags', through='recipes.TaggedOther'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='yeast',
            name='tags',
            field=recipes.models.FixedTaggableManager(blank=True, to='recipes.LowercaseTag', help_text='A comma-separated list of tags.', verbose_name='Tags', through='recipes.TaggedYeast'),
            preserve_default=True,
        ),
    ]
