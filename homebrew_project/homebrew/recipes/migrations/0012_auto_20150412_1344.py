# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0011_auto_20150325_2000'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='other',
            options={'ordering': ('name',)},
        ),
        migrations.AlterField(
            model_name='recipeother',
            name='unit',
            field=models.CharField(choices=[('g', 'Grams'), ('kg', 'Kilograms'), ('tsp', 'Teaspoon'), ('cup', 'Cup'), ('sticks', 'Sticks'), ('pods', 'Pods'), ('ml', 'Millilitres')], max_length=10),
            preserve_default=True,
        ),
    ]
