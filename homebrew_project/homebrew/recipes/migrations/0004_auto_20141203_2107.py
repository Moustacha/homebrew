# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0003_auto_20141201_2122'),
    ]

    operations = [
        migrations.AlterField(
            model_name='yeast',
            name='style',
            field=models.ManyToManyField(blank=True, to='recipes.Style', null=True),
            preserve_default=True,
        ),
    ]
