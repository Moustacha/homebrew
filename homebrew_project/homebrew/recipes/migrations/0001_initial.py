# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators
from django.conf import settings
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Brand',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'ordering': ('name',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Fermentable',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField(blank=True)),
                ('slug', models.SlugField(editable=False)),
                ('public', models.BooleanField(default=False)),
                ('type', models.CharField(max_length=20, choices=[('MALTGR', 'Malted Grain'), ('MALTEX', 'Malted Extract'), ('ADJUNCT', 'Adjunct'), ('SUGAR', 'Sugar')])),
                ('lovibond_rating', models.DecimalField(blank=True, decimal_places=2, max_digits=10)),
                ('requires_mashing', models.BooleanField(default=False)),
                ('extract_potential', models.DecimalField(default=70, decimal_places=2, help_text='The theoretical maximum percentage that will be converted to sugar', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], max_digits=5)),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('name', 'type'),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Hop',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField(blank=True)),
                ('slug', models.SlugField(editable=False)),
                ('public', models.BooleanField(default=False)),
                ('country_of_origin', models.CharField(max_length=100)),
                ('alpha_acid_low', models.IntegerField(validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)])),
                ('alpha_acid_high', models.IntegerField(validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)])),
                ('intensity', models.IntegerField(help_text='1 = low; 10 = high', validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(10)])),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('name',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Other',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField(blank=True)),
                ('slug', models.SlugField(editable=False)),
                ('public', models.BooleanField(default=False)),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Recipe',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField(blank=True, null=True)),
                ('target_volume', models.IntegerField(help_text='Target volume of beer in L')),
                ('original_gravity', models.DecimalField(decimal_places=3, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(2)], max_digits=4)),
                ('expected_final_gravity', models.DecimalField(decimal_places=3, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(2)], max_digits=4)),
                ('mash_liquor', models.DecimalField(decimal_places=2, max_digits=10)),
                ('mash_time', models.IntegerField(help_text='Time in minutes')),
                ('mash_temperature', models.IntegerField()),
                ('boil_liquor', models.DecimalField(decimal_places=2, max_digits=10)),
                ('boil_time', models.IntegerField(help_text='Time in minutes')),
                ('fermentation_temp', models.IntegerField()),
                ('conditioning_time', models.IntegerField()),
                ('conditioning_time_quantifier', models.CharField(max_length=1, choices=[('d', 'Days'), ('w', 'Weeks'), ('y', 'Years')])),
                ('conditioning_temp', models.IntegerField()),
                ('public', models.BooleanField(default=False, help_text='Do you want other people to be able to view this recipe?')),
                ('slug', models.SlugField(editable=False)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('last_updated_date', models.DateTimeField(auto_now=True)),
                ('cloned_from', models.ForeignKey(null=True, editable=False, blank=True, to='recipes.Recipe')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RecipeGrain',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('quantity', models.DecimalField(decimal_places=2, max_digits=10)),
                ('unit', models.CharField(max_length=10, choices=[('g', 'Grams'), ('kg', 'Kilograms')])),
                ('fermentable', models.ForeignKey(to='recipes.Fermentable', on_delete=django.db.models.deletion.PROTECT)),
                ('recipe', models.ForeignKey(to='recipes.Recipe')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RecipeHop',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('alpha_acid_rating', models.DecimalField(decimal_places=2, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], max_digits=5)),
                ('quantity', models.IntegerField(help_text='In grams')),
                ('ibu', models.DecimalField(decimal_places=1, max_digits=3)),
                ('when_to_add', models.IntegerField(help_text='Time in minutes from the end (If it is added at the start, enter the whole boil time)')),
                ('hop', models.ForeignKey(to='recipes.Hop', on_delete=django.db.models.deletion.PROTECT)),
                ('recipe', models.ForeignKey(to='recipes.Recipe')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RecipeOther',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('quantity', models.IntegerField()),
                ('unit', models.CharField(max_length=10, choices=[('g', 'Grams'), ('kg', 'Kilograms'), ('tsp', 'Teaspoon'), ('cup', 'Cup'), ('sticks', 'Sticks')])),
                ('when_to_add', models.CharField(max_length=100)),
                ('other', models.ForeignKey(to='recipes.Other', on_delete=django.db.models.deletion.PROTECT)),
                ('recipe', models.ForeignKey(to='recipes.Recipe')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RecipeProfile',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('mash_efficiency', models.DecimalField(default=70, decimal_places=2, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], max_digits=5)),
                ('grain_absorption', models.DecimalField(default=1.1, decimal_places=2, help_text='How much water is absorbed by the grain, per kg', max_digits=5)),
                ('evaporation_rate', models.DecimalField(default=2.1, decimal_places=2, help_text='How much water evaporates from the boil, per hour', max_digits=5)),
                ('trub_loss', models.DecimalField(default=1, decimal_places=2, help_text='How much you expect to lose to the hot break and hops after the boil', max_digits=5)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Style',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'ordering': ('name',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Yeast',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField(blank=True)),
                ('slug', models.SlugField(editable=False)),
                ('public', models.BooleanField(default=False)),
                ('attenuation', models.DecimalField(default=75, decimal_places=2, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], max_digits=5)),
                ('alternatives', models.ManyToManyField(blank=True, null=True, to='recipes.Yeast', related_name='alternatives_rel_+')),
                ('brand', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, help_text='Brand/Manufacturer', to='recipes.Brand')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('style', models.ManyToManyField(to='recipes.Style')),
            ],
            options={
                'ordering': ('brand', 'name'),
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='yeast',
            unique_together=set([('name', 'brand')]),
        ),
        migrations.AddField(
            model_name='recipe',
            name='fermentables',
            field=models.ManyToManyField(through='recipes.RecipeGrain', to='recipes.Fermentable'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='recipe',
            name='hops',
            field=models.ManyToManyField(through='recipes.RecipeHop', to='recipes.Hop'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='recipe',
            name='others',
            field=models.ManyToManyField(through='recipes.RecipeOther', to='recipes.Other'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='recipe',
            name='owner',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='recipe',
            name='style',
            field=models.ForeignKey(to='recipes.Style', on_delete=django.db.models.deletion.PROTECT),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='recipe',
            name='yeast',
            field=models.ForeignKey(to='recipes.Yeast', on_delete=django.db.models.deletion.PROTECT),
            preserve_default=True,
        ),
    ]
