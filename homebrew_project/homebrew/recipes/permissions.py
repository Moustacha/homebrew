from permissionsx.models import Permissions, P, Cmp

from homebrew.permissions import NotAllowedRedirectView, authenticated_permission
from recipes.models import AllGrainRecipe, Fermentable, Hop, Other, Yeast, BaseRecipe


__author__ = 'sam'


class ObjectOwnerPermissions(Permissions):
    model_class = None
    lookup_field = None

    def __init__(self, model_class=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.model_class:
            self.model_class = model_class
        self.lookup_field = kwargs.get('lookup_field', None)
        if not self.lookup_field:
            self.lookup_field = 'slug'

    def get_rules(self, request=None, **kwargs):
        request.object = None
        try:
            if self.model_class:
                get_kwargs = {self.lookup_field: kwargs.get(self.lookup_field)}
                request.object = self.model_class.objects.get(**get_kwargs)
        except self.model_class.DoesNotExist:
            pass

        # If not authenticated, then get taken to the login screen with a message
        # If not a superuser or the owner, then get taken to the detail view with a message
        not_allowed_view = NotAllowedRedirectView.as_view()
        return authenticated_permission & P(P(user__is_superuser=True) | P(object__owner=Cmp('user')),
                                            if_false=not_allowed_view)


class RecipeOwnerPermissions(ObjectOwnerPermissions):
    model_class = BaseRecipe


class FermentableOwnerPermissions(ObjectOwnerPermissions):
    model_class = Fermentable


class HopOwnerPermissions(ObjectOwnerPermissions):
    model_class = Hop


class OtherOwnerPermissions(ObjectOwnerPermissions):
    model_class = Other


class YeastOwnerPermissions(ObjectOwnerPermissions):
    model_class = Yeast
