from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from recipes.models import RecipeProfile


__author__ = 'sam'


@receiver(post_save, sender=User, dispatch_uid='user-profile-creation-signal')
def create_profile(sender, instance, created, raw, **kwargs):
    if raw:
        return
    if created:
        profile = RecipeProfile(user=instance)
        profile.save()
