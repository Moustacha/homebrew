import math
from decimal import Decimal

__author__ = 'sam'

# http://homebrewmanual.com/home-brewing-calculations/


def recipe_gravity_points(original_gravity, volume):
    """

    :param original_gravity: Target original gravity, e.g. 1.085
    :param volume: Target volume of how much beer you want, in litres. e.g. 16
    :return:
    """
    return (Decimal(original_gravity) - 1) * Decimal(1000) * Decimal(volume)


def original_gravity(gravity_points, volume):
    return (Decimal(gravity_points) / (Decimal(1000) * Decimal(volume))) + 1


def fermentable_gravity_points(extract_potential, weight, efficiency=70):
    """
        Calculates how many gravity points a fermentable will produce for
        the given weight
    :param fermentable: recipes.models.Fermentable
    :param weight: Weight in kg
    :param efficiency: Percentage efficiency of the system
    :return:
    """
    return Decimal(weight) * (Decimal(extract_potential) / Decimal(100)) * (
        Decimal(efficiency) / Decimal(100)) * Decimal(384)


def fermentable_weight(fermentable, gravity_points, efficiency=70):
    """
        Calculates how much of a fermentable is needed to reach the
        given gravity points
    :param fermentable: recipes.models.Fermentable
    :param gravity_points:
    :param efficiency: Percentage efficiency of the system
    :return:
    """
    return gravity_points / ((fermentable.extract_potential / Decimal(100)) * (efficiency / Decimal(100)) * 384)


def malt_colour_units(lovibond_rating, weight, target_volume):
    """

    :param fermentable: recipes.models.Fermentable
    :param weight: Weight of the fermentable in kg
    :param target_volume: Target final volume of the beer
    :return:
    """
    return (Decimal(weight) * Decimal(lovibond_rating) * Decimal(2.205)) / (
        Decimal(target_volume) * Decimal(0.264))


def ebc(malt_colour_units):
    """

    :param malt_colour_units: The sum of malt_colour_units for each fermentable
    :return:
    """
    return (Decimal(1.49) * malt_colour_units ** Decimal(0.69)) * Decimal(1.97)


def ebc_to_srm(ebc):
    return Decimal(ebc) * Decimal(0.508)


def lovibond_to_srm(lovibond):
    return Decimal(1.3546) * Decimal(lovibond) - Decimal(0.76)


def srm_to_ebc(srm):
    return Decimal(srm) * Decimal(1.97)


def final_gravity(original_gravity, yeast_attenuation):
    return ((Decimal(original_gravity) - 1) * (1 - (Decimal(yeast_attenuation) / Decimal(100)))) + 1


def alcohol_by_volume(original_gravity, final_gravity):
    return (original_gravity - final_gravity) * 131


def water_loss_to_grain(grain_weight, grain_absorption_rate):
    """

    :param grain_weight: The weight of the grain
    :param grain_absorption_rate: How much water is lost to the grain, per grain unit of measurement
    :return:
    """
    return Decimal(grain_weight) * Decimal(grain_absorption_rate)


def water_loss_to_evaporation(boil_time, evaporation_rate):
    """
    :param boil_time: How long you need to boil in minutes
    :param evaporation_rate: how much water is lost to evaporation per hour
    :return:
    """
    return (Decimal(boil_time) / Decimal(60)) * Decimal(evaporation_rate)


def mash_volume(target_volume, grain_weight, grain_absorption_rate):
    losses_to_grain = water_loss_to_grain(grain_weight, grain_absorption_rate)
    return target_volume + losses_to_grain


def strike_volume(grain_weight, mash_thickness):
    return Decimal(grain_weight) * Decimal(mash_thickness)


def mash_thickness(strike_volume, grain_weight):
    return Decimal(strike_volume) / Decimal(grain_weight)


def first_runnings_volume(strike_volume, grain_weight, grain_absorption_rate):
    losses_to_grain = water_loss_to_grain(grain_weight, grain_absorption_rate)
    return Decimal(strike_volume) - losses_to_grain


def sparge_volume(boil_volume, first_runnings):
    return Decimal(boil_volume) - Decimal(first_runnings)


def boil_volume(target_volume, boil_time, evaporation_rate, break_loss):
    losses_to_evaporation = water_loss_to_evaporation(boil_time, evaporation_rate)
    return target_volume + losses_to_evaporation + break_loss


def starting_volume(target_volume, grain_weight, grain_absorption_rate,
                    boil_time, evaporation_rate, break_loss):
    """

    :param target_volume: Target batch size
    :param grain_weight: The weight of the grain
    :param grain_absorption_rate: How much water is lost to the grain, per grain unit of measurement
    :param boil_time: How long you need to boil in minutes
    :param evaporation_rate: how much water is lost to evaporation per hour
    :param break_loss: How much is lost at the end due to the break and hops
    :return:
    """
    losses_to_grain = water_loss_to_grain(grain_weight, grain_absorption_rate)
    losses_to_evaporation = water_loss_to_evaporation(boil_time, evaporation_rate)
    return Decimal(target_volume) + Decimal(losses_to_grain) + Decimal(losses_to_evaporation) + Decimal(break_loss)


def alpha_acid_units(alpha_acid_percentage, weight_of_hops):
    return alpha_acid_percentage * weight_of_hops


def hop_weight_aau(aau, alpha_acid_percentage):
    return aau / alpha_acid_percentage


def hop_weight(target_volume, ibu, alpha_acid_percentage, utilisation):
    numerator = Decimal(target_volume) * Decimal(ibu)
    denominator = Decimal(alpha_acid_percentage) * Decimal(utilisation) * Decimal(10)
    return numerator / denominator


def ibu(average_boil_gravity, hop_boil_time, hop_weight, alpha_acid_percentage, target_volume):
    """

    :param average_boil_gravity: Calculated from average_boil_gravity()
    :param hop_boil_time: How long the hops are boiled for
    :param alpha_acid_percentage:
    :param target_volume:
    :return:
    """
    mgl_added_alpha_acids = (alpha_acid_percentage * hop_weight * 10) / target_volume
    return alpha_acid_utilisation(average_boil_gravity, hop_boil_time) * Decimal(mgl_added_alpha_acids)


def average_boil_gravity(gravity_points, starting_volume, losses_to_grain, losses_to_evaporation):
    preboil_volume = Decimal(starting_volume) - Decimal(losses_to_grain)
    postboil_volume = preboil_volume - Decimal(losses_to_evaporation)
    avg_boil_volume = (preboil_volume + postboil_volume) / Decimal(2)

    return (Decimal(gravity_points) / (avg_boil_volume * Decimal(1000))) + 1


def alpha_acid_utilisation(average_boil_gravity, hop_boil_time):
    """

    :param average_boil_gravity: Calculated from average_boil_gravity()
    :param hop_boil_time: How long the hops are boiled for
    :return:
    """
    bigness_factor = Decimal(1.65) * Decimal(0.000125) ** (Decimal(average_boil_gravity) - 1)
    boil_time_factor = Decimal(1 - math.exp(Decimal(-0.04) * Decimal(hop_boil_time))) / Decimal(4.15)

    return bigness_factor * boil_time_factor


def priming_sucrose(target_volume, max_temperature, co_volume):
    target_volume = Decimal(target_volume)
    # Need to convert for fahrenheit because the formula takes that as a temperature
    # for some reason
    max_temperature = celsius_to_fahrenheit(max_temperature)
    co_volume = Decimal(co_volume)

    beer_co2 = Decimal(3.0378) - (Decimal(0.050062) * max_temperature) + (Decimal(0.00026555) * max_temperature ** 2)

    return ((co_volume * Decimal(2)) - (beer_co2 * Decimal(2))) * Decimal(2) * target_volume


def priming_dextrose(target_volume, max_temperature, co_volume):
    sucrose = priming_sucrose(target_volume, max_temperature, co_volume)

    return sucrose / Decimal(0.91)


def priming_dme(target_volume, max_temperature, co_volume):
    sucrose = priming_sucrose(target_volume, max_temperature, co_volume)

    return sucrose / Decimal(0.68)


def celsius_to_fahrenheit(celsius):
    return (Decimal(9) / Decimal(5)) * Decimal(celsius) + Decimal(32)