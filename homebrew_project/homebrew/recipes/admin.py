from django.db import models
from django.contrib import admin

# Register your models here.
from django.forms import CheckboxSelectMultiple
from recipes.models import Fermentable, Hop, Other, Lab, Yeast, AllGrainRecipe, Style, RecipeProfile, BaseRecipe, \
    ExtractRecipe


class YeastModelAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple}
    }

admin.site.register(Fermentable)
admin.site.register(Hop)
admin.site.register(Other)
admin.site.register(Lab)
admin.site.register(Yeast, YeastModelAdmin)
admin.site.register(AllGrainRecipe)
admin.site.register(BaseRecipe)
admin.site.register(ExtractRecipe)
admin.site.register(Style)
admin.site.register(RecipeProfile)
