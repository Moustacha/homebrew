from recipes.models import Fermentable, Hop, Other, Yeast, BaseRecipe

__author__ = 'sam'

from django_tables2 import tables, LinkColumn, A, Column

empty_text_str = 'No {} could be found. Please try changing your search parameters.'


class RecipeTableData(tables.TableData):
    """
    This class only overrides the verbose names to a static value
    """
    def verbose_name(self):
        return 'recipe'

    def verbose_name_plural(self):
        return 'recipes'


class RecipeTable(tables.Table):
    name = LinkColumn('recipe-detail', kwargs={'slug': A('slug')}, verbose_name='Name')
    style = Column(verbose_name='Style')

    TableDataClass = RecipeTableData

    class Meta:
        model = BaseRecipe
        fields = ('name', 'style',)
        attrs = {'class': 'table table-hover'}
        order_by = ('name', )
        empty_text = empty_text_str.format('recipes')


class FermentableTable(tables.Table):
    name = LinkColumn('fermentable-detail', kwargs={'slug': A('slug')}, verbose_name='Name')
    type = Column(verbose_name='Type')

    class Meta:
        model = Fermentable
        fields = ('name', 'type', )
        attrs = {'class': 'table table-hover'}
        order_by = ('name', )
        empty_text = empty_text_str.format('fermentables')


class HopTable(tables.Table):
    name = LinkColumn('hop-detail', kwargs={'slug': A('slug')}, verbose_name='Name')

    class Meta:
        model = Hop
        fields = ('name', )
        attrs = {'class': 'table table-hover'}
        order_by = ('name', )
        empty_text = empty_text_str.format('hops')


class OtherTable(tables.Table):
    name = LinkColumn('other-detail', kwargs={'slug': A('slug')}, verbose_name='Name')

    class Meta:
        model = Other
        fields = ('name', )
        attrs = {'class': 'table table-hover'}
        order_by = ('name', )
        empty_text = empty_text_str.format('items')


class YeastTable(tables.Table):
    name = LinkColumn('yeast-detail', kwargs={'slug': A('slug')}, verbose_name='Name')
    lab = Column(verbose_name='Lab')

    class Meta:
        model = Yeast
        fields = ('lab', 'name', )
        attrs = {'class': 'table table-hover'}
        order_by = ('lab', 'name', )
        empty_text = empty_text_str.format('yeasts')
