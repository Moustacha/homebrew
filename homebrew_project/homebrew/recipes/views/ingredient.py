from django.core.urlresolvers import reverse
from django.views.generic import UpdateView, DeleteView, CreateView, DetailView
from permissionsx.contrib.django.views import DjangoViewMixin

from homebrew.permissions import UserIsAuthenticatedPermissions
from recipes.permissions import ObjectOwnerPermissions
from recipes.views.generic import PagedFilteredTableView, get_dependant_objects, RememberPageParametersMixin, \
    FormViewMessageMixin


__author__ = 'sam'


class IngredientCreateView(FormViewMessageMixin, DjangoViewMixin, CreateView):
    template_name = 'ingredient/ingredient_create.html'

    # Authorisation
    permissions = UserIsAuthenticatedPermissions()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.model.__name__
        return context

    def get_initial(self):
        initial = super().get_initial()
        initial['owner'] = self.request.user
        return initial


class IngredientListView(RememberPageParametersMixin, PagedFilteredTableView):
    template_name = 'ingredient/ingredient_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.model.__name__
        context['create_url'] = reverse(str(self.model.__name__ + '-create').lower())
        return context

    def get(self, request, *args, **kwargs):
        self.queryset = self.model.objects.public_or_owner(request.user)
        return super().get(request, *args, **kwargs)


class IngredientUpdateView(FormViewMessageMixin, DjangoViewMixin, UpdateView):
    template_name = 'ingredient/ingredient_update.html'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        # Authorisation
        self.permissions = ObjectOwnerPermissions(model_class=self.model)


class IngredientDeleteView(FormViewMessageMixin, DjangoViewMixin, DeleteView):
    template_name = 'ingredient/ingredient_delete.html'
    success_view_name = ''
    success_message = '{} successfully deleted'
    failure_message = 'Error while deleting {}'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        # Authorisation
        self.permissions = ObjectOwnerPermissions(model_class=self.model)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        dependants = get_dependant_objects(self.get_object())
        dependant_recipes = set([x.recipe for x in dependants])
        context['protected'] = dependant_recipes

        return context

    def get_success_url(self):
        return reverse(self.success_view_name)


class IngredientDetailView(DetailView):

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Also calculate how big the tag should be displayed, up to a max of 28px
        similar_objects = [(x, min(x.similar_tags * 2 + 12, 28)) for x in self.object.tags.similar_objects()]
        context['similar_objects'] = similar_objects

        return context
