from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import transaction
from django.forms.models import inlineformset_factory
from django.http import HttpResponseRedirect
from django.views.generic import UpdateView, DeleteView, CreateView
from permissionsx.contrib.django.views import DjangoViewMixin

from recipes.forms import RecipeGrainForm, RecipeHopForm, RecipeOtherForm, \
    AllGrainRecipeForm, RecipeProfileForm, BaseRecipeForm, \
    ExtractRecipeForm, RecipeGrainBaseFormSet, RecipeOtherBaseFormSet, RecipeHopBaseFormSet
from recipes.models import AllGrainRecipe, RecipeGrain, RecipeHop, RecipeOther, RecipeProfile, BaseRecipe, ExtractRecipe
from homebrew.permissions import UserIsAuthenticatedPermissions
from recipes.permissions import RecipeOwnerPermissions
from recipes.views.filters import RecipeFilter, RecipeSearchFilter
from recipes.views.generic import PagedFilteredTableView, class_gen_with_kwarg, RememberPageParametersMixin, \
    FormViewMessageMixin
from recipes.views.tables import RecipeTable


__author__ = 'sam'


class CreateUpdateRecipeMixin(object):

    def get_recipegrain_formset(self):
        return inlineformset_factory(
            BaseRecipe,
            RecipeGrain,
            formset=class_gen_with_kwarg(RecipeGrainBaseFormSet, user=self.request.user),
            form=class_gen_with_kwarg(RecipeGrainForm, steep=(self.model == ExtractRecipe)),
            extra=1

        )

    def get_recipeother_formset(self):
        return inlineformset_factory(
            self.model,
            RecipeOther,
            formset=class_gen_with_kwarg(RecipeOtherBaseFormSet, user=self.request.user),
            form=RecipeOtherForm,
            extra=1
        )

    def get_recipehop_formset(self):
        return inlineformset_factory(
            self.model,
            RecipeHop,
            formset=class_gen_with_kwarg(RecipeHopBaseFormSet, user=self.request.user),
            form=RecipeHopForm,
            extra=1
        )

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        recipegrain_formset = self.get_recipegrain_formset()(instance=self.object)
        recipeother_formset = self.get_recipeother_formset()(instance=self.object)

        context_data_kwargs = {'form': form,
                               'recipegrain_formset': recipegrain_formset,
                               'recipeother_formset': recipeother_formset}

        # Add the hops formset if extract or all grain
        if self.model == ExtractRecipe or self.model == AllGrainRecipe:
            recipehop_formset = self.get_recipehop_formset()(instance=self.object)
            context_data_kwargs['recipehop_formset'] = recipehop_formset

        return self.render_to_response(self.get_context_data(**context_data_kwargs))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        recipegrain_formset = self.get_recipegrain_formset()(request.POST, instance=self.object)
        recipeother_formset = self.get_recipeother_formset()(request.POST, instance=self.object)

        recipehop_formset = None
        if self.model == ExtractRecipe or self.model == AllGrainRecipe:
            recipehop_formset = self.get_recipehop_formset()(request.POST, instance=self.object)

        if all((form.is_valid(), recipegrain_formset.is_valid(), recipeother_formset.is_valid()))\
                and (recipehop_formset is None or recipehop_formset.is_valid()):
            return self.form_valid(form, recipegrain_formset, recipeother_formset, recipehop_formset)
        else:
            return self.form_invalid(form, recipegrain_formset, recipeother_formset, recipehop_formset)

    def form_valid(self, form, recipegrain_formset, recipeother_formset, recipehop_formset):
        with transaction.atomic() as trans:
            self.object = form.save()

            recipegrain_formset.instance = self.object
            recipegrain_formset.save()
            recipeother_formset.instance = self.object
            recipeother_formset.save()

            if self.model == ExtractRecipe or self.model == AllGrainRecipe:
                self.object = self.object.extractrecipe
                recipehop_formset.instance = self.object
                recipehop_formset.save()

                if self.model == AllGrainRecipe:
                    self.object = self.object.allgrainrecipe

            self.object.calculate_fields(only_empty=True)
            self.object.save()
            return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, recipegrain_formset, recipeother_formset, recipehop_formset):
        return self.render_to_response(self.get_context_data(form=form,
                                                             recipegrain_formset=recipegrain_formset,
                                                             recipeother_formset=recipeother_formset,
                                                             recipehop_formset=recipehop_formset))


class RecipeUpdateView(FormViewMessageMixin, CreateUpdateRecipeMixin, DjangoViewMixin, UpdateView):
    template_name = 'recipe/recipe_update_basic.html'
    model = BaseRecipe
    form_class = BaseRecipeForm

    # Authorisation
    permissions = RecipeOwnerPermissions()

    def get_object(self, queryset=None):
        obj = super().get_object(queryset)
        if hasattr(obj, 'extractrecipe'):
            obj = obj.extractrecipe
            self.model = ExtractRecipe
            self.form_class = ExtractRecipeForm
            self.template_name = 'recipe/recipe_update_extract.html'

            if hasattr(obj, 'allgrainrecipe'):
                obj = obj.allgrainrecipe
                self.model = AllGrainRecipe
                self.form_class = AllGrainRecipeForm
                self.template_name = 'recipe/recipe_update_all_grain.html'

        return obj


class RecipeCreateView(FormViewMessageMixin, CreateUpdateRecipeMixin, DjangoViewMixin, CreateView):
    template_name = 'recipe/recipe_create_all_grain.html'

    # Authorisation
    permissions = UserIsAuthenticatedPermissions()

    def get_object(self, queryset=None):
        return None

    def get_initial(self):
        initial = super().get_initial()
        initial['owner'] = self.request.user
        return initial


class RecipeDeleteView(FormViewMessageMixin, DjangoViewMixin, DeleteView):
    model = BaseRecipe
    template_name = 'recipe/recipe_delete.html'
    success_message = '{} successfully deleted'
    failure_message = 'Error while deleting {}'

    # Authorisation
    permissions = RecipeOwnerPermissions()

    def get_success_url(self):
        return reverse('recipe-list')


class RecipeListView(RememberPageParametersMixin, PagedFilteredTableView):
    template_name = 'recipe/recipe_list.html'
    model = BaseRecipe
    table_class = RecipeTable
    filter_class = RecipeFilter
    # formhelper_class = RecipeFilterFormHelper

    url_name = 'recipe-list'

    def get(self, request, *args, **kwargs):
        request.session['list_page'] = request.path
        self.queryset = self.model.objects.public_or_owner(request.user).select_related('style__name')
        return super().get(request, *args, **kwargs)


class IngredientSearchListView(RememberPageParametersMixin, PagedFilteredTableView):
    template_name = 'recipe/recipe_ingredient_search.html'
    # formhelper_class = RecipeSearchFilterFormHelper
    model = AllGrainRecipe
    table_class = RecipeTable
    filter_class = RecipeSearchFilter

    url_name = 'recipe-search'

    def get_filter(self, queryset):
        return self.filter_class(self.request.user, self.request.GET, queryset=queryset)

    def get_queryset(self, **kwargs):
        qs = super().get_queryset(**kwargs)
        return qs.distinct()

    def get(self, request, *args, **kwargs):
        request.session['list_page'] = request.path
        self.queryset = AllGrainRecipe.objects.public_or_owner(request.user).select_related('style__name')
        return super().get(request, *args, **kwargs)


class RecipeProfileUpdateView(FormViewMessageMixin, DjangoViewMixin, UpdateView):
    model = RecipeProfile
    template_name = 'account/recipe_profile.html'
    form_class = RecipeProfileForm
    success_message = 'Recipe calculation settings saved'
    failure_message = 'Error while saving recipe calculation settings'

    permissions = UserIsAuthenticatedPermissions()

    def get_object(self, queryset=None):
        return RecipeProfile.objects.get(pk=self.request.user.recipeprofile.pk)
