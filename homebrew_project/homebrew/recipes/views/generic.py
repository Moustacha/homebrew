import urllib
from django.contrib import messages

from django.contrib.admin.util import NestedObjects
from django.db import ConnectionRouter
from django.http import QueryDict


__author__ = 'sam'

from django_tables2 import SingleTableView, RequestConfig


class PagedFilteredTableView(SingleTableView):
    filter_class = None
    # formhelper_class = BasicFormHelper
    context_filter_name = 'filter'

    def get_filter(self, queryset):
        return self.filter_class(self.request.GET, queryset=queryset)

    def get_queryset(self, **kwargs):
        qs = super(PagedFilteredTableView, self).get_queryset()
        self.filter = self.get_filter(qs)
        # self.filter.form.helper = self.formhelper_class()
        return self.filter.qs

    def get_table(self, **kwargs):
        table = super(PagedFilteredTableView, self).get_table()
        RequestConfig(self.request, paginate={'page': self.kwargs.get('page', 1),
                                              "per_page": self.paginate_by}).configure(table)
        return table

    def get_context_data(self, **kwargs):
        context = super(PagedFilteredTableView, self).get_context_data()
        context[self.context_filter_name] = self.filter
        return context


def get_dependant_objects(obj):
    """

    :param obj: Object to be deleted
    :return: Set of objects that depend on the given obj
    """
    router = ConnectionRouter()
    using = router.db_for_write(obj._meta.model)
    collector = NestedObjects(using=using)
    collector.collect([obj])

    return collector.protected


def class_gen_with_kwarg(cls, **additionalkwargs):
    """class generator for subclasses with additional 'stored' parameters (in a closure)
       This is required to use a formset_factory with a form that need additional
       initialization parameters
       See: http://stackoverflow.com/questions/622982/django-passing-custom-form-parameters-to-formset
    """

    class ClassWithKwargs(cls):
        def __init__(self, *args, **kwargs):
            kwargs.update(additionalkwargs)
            super(ClassWithKwargs, self).__init__(*args, **kwargs)

    return ClassWithKwargs


class RememberPageParametersMixin(object):
    url_name = None

    def is_query_params_specified(self, query_params):
        """ Are any of the query parameters we are interested in on this request URL?"""
        for current_param in self.request.GET:
            if current_param in query_params:
                return True
        return False

    def params_from_last_time(self, key_prefix):
        """ Gets a dictionary of JUST the params from the last render with values """
        params = {}
        stored_params = [x for x in self.request.session.keys() if x.startswith(key_prefix)]
        for stored_param in stored_params:
            last_value = self.request.session.get(stored_param)

            page_param = stored_param.replace(key_prefix, '')

            if isinstance(last_value, list) and len(last_value) == 1:
                params[page_param] = last_value[0]
            elif last_value:
                params[page_param] = last_value
        return params

    def get(self, request, *args, **kwargs):
        key_prefix = self.url_name + '_'

        # If clear is in the parameters then clear the session
        if 'clear' in self.request.GET:
            # Remove the last saved parameters
            page_params = [x for x in self.request.session.keys() if str(x).startswith(key_prefix)]
            for param in page_params:
                del self.request.session[param]

        # There are page parameters that need to be stored
        elif len(self.request.GET) > 0:
            page_params = self.request.GET.copy()

            # Need to pop items because we want the object. If we do a get then for things where it's
            # a list only return one value. Seems to be how QueryDict/MultiValueDict is implemented.
            while len(page_params) > 0:
                query_param, value = page_params.popitem()
                self.request.session[key_prefix + query_param] = value

        # There are no page parameters, so we'll retrieve and set
        else:
            last_params = self.params_from_last_time(key_prefix)
            if last_params and len(last_params.keys()) > 0:
                page_params = urllib.parse.urlencode(last_params, doseq=True)
                self.request.GET = QueryDict(page_params)

        return super().get(request, *args, **kwargs)


class FormViewMessageMixin(object):
    success_message = None
    failure_message = None

    def get_model_name(self):
        return self.model._meta.verbose_name.title()

    def form_valid(self, form, *args, **kwargs):
        if self.success_message:
            message = self.success_message.format(self.get_model_name())
        else:
            message = '{} saved successfully.'.format(self.get_model_name())
        messages.success(self.request, message)

        return super().form_valid(form, *args, **kwargs)

    def form_invalid(self, form, *args, **kwargs):
        if self.failure_message:
            message = self.failure_message.format(self.get_model_name())
        else:
            self.failure_message = 'Errors while saving {}.'.format(self.get_model_name())
        messages.error(self.request, self.failure_message)

        return super().form_invalid(form, *args, **kwargs)

