from distutils.util import strtobool
from django.db.models import Q
from django.forms import Select
from django_filters.widgets import LookupTypeWidget
from django_filters.fields import Lookup
import django_filters as filters
from django import forms

from recipes.models import AllGrainRecipe, Fermentable, Hop, Other, Yeast, BaseRecipe


__author__ = 'sam'


def exclude_cloned(qs, *args, **kwargs):
    if args[0]:
        return qs.filter(cloned_from=None)
    else:
        return qs


BOOLEAN_CHOICES = (('false', 'No'), ('true', 'Yes'),)


class RecipeFilter(filters.FilterSet):
    name = filters.CharFilter(lookup_type='icontains')
    cloned_from = filters.TypedChoiceFilter(label='Original Recipes Only', action=exclude_cloned,
                                            choices=BOOLEAN_CHOICES, coerce=strtobool)

    class Meta:
        model = BaseRecipe
        fields = ('name', 'style', 'cloned_from',)


# class RecipeFilterFormHelper(FormHelper):
#     form_method = 'GET'
#     layout = Layout(
#         'name',
#         'style',
#         'cloned_from',
#         Submit('submit', 'Search'),
#         HTML('<a href="{% url "recipe-list" %}?clear" class="btn btn-default">Reset</a>')
#     )
#
#
# class BasicFormHelper(FormHelper):
#     form_tag = False


class MyMultiValueField(forms.MultiValueField):
    def __init__(self, *args, **kwargs):
        queryset = kwargs.pop('queryset')

        fields = (
            forms.ModelMultipleChoiceField(queryset, **kwargs),
            forms.ChoiceField(choices=(('any', 'Any'), ('all', 'All')),
                              widget=Select(attrs={'class': 'form-control search-combo'}))
        )
        defaults = {
            'widgets': [f.widget for f in fields],
        }
        widget = LookupTypeWidget(**defaults)
        kwargs['widget'] = widget
        super().__init__(fields, *args, **kwargs)

    def compress(self, data_list):
        if len(data_list) == 2:
            return Lookup(value=data_list[0], lookup_type=data_list[1] or 'any')
        return Lookup(value=None, lookup_type='any')


class MyModelMultipleChoiceFilter(filters.Filter):
    field_class = MyMultiValueField

    def __init__(self, name=None, label=None, widget=None, action=None, lookup_type='exact', required=False,
                 distinct=False, **kwargs):
        super().__init__(name, label, widget, action, lookup_type, required, distinct, **kwargs)

    def filter(self, qs, value):
        value = value or ()
        q = Q()
        if value[0]:
            lookup_type = value[1]

            for v in value[0]:
                if lookup_type == 'any':
                    q |= Q(**{self.name: v})
                elif lookup_type == 'all':
                    qs = qs.filter(**{self.name: v})
        return qs.filter(q).distinct()


class RecipeSearchFilter(filters.FilterSet):
    fermentables = MyModelMultipleChoiceFilter(label='Fermentables', queryset=Fermentable.objects.all())
    hops = MyModelMultipleChoiceFilter(label='Hops', queryset=Hop.objects.all())
    others = MyModelMultipleChoiceFilter(label='Others', queryset=Other.objects.all())
    yeast = filters.ModelChoiceFilter(queryset=Yeast.objects.select_related('lab').all())

    class Meta:
        model = AllGrainRecipe
        fields = ['fermentables', 'hops', 'others', 'yeast']

    def __init__(self, user=None, data=None, queryset=None, prefix=None, strict=None):
        super().__init__(data, queryset, prefix, strict)
        self.filters['fermentables'].extra['queryset'] = Fermentable.objects.public_or_owner(user)
        self.filters['hops'].extra['queryset'] = Hop.objects.public_or_owner(user)
        self.filters['others'].extra['queryset'] = Other.objects.public_or_owner(user)
        self.filters['yeast'].extra['queryset'] = Yeast.objects.public_or_owner(user)


# class RecipeSearchFilterFormHelper(FormHelper):
#     form_method = 'GET'
#     layout = Layout(
#         'fermentables',
#         'hops',
#         'others',
#         'yeast',
#         Submit('submit', 'Search'),
#         HTML('<a href="{% url "recipe-search" %}?clear" class="btn btn-default">Reset</a>')
#     )


EMPTY_CHOICE = [('', '---------')]


class FermentableFilter(filters.FilterSet):
    name = filters.CharFilter(lookup_type='icontains', )

    class Meta:
        model = Fermentable
        fields = ['name', 'type']

    def __init__(self, data=None, queryset=None, prefix=None, strict=None):
        super().__init__(data, queryset, prefix, strict)
        self.filters['type'].extra['choices'] = EMPTY_CHOICE + list(self.filters['type'].extra['choices'])


class HopFilter(filters.FilterSet):
    name = filters.CharFilter(lookup_type='icontains', )

    class Meta:
        model = Hop
        fields = ['name']


class OtherFilter(filters.FilterSet):
    name = filters.CharFilter(lookup_type='icontains', )

    class Meta:
        model = Other
        fields = ['name']


class YeastFilter(filters.FilterSet):
    name = filters.CharFilter(lookup_type='icontains', )

    class Meta:
        model = Yeast
        fields = ['lab', 'name']
