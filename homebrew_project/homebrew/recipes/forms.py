import copy
from django.forms import ModelChoiceField, BaseInlineFormSet
from django import forms
from django.forms.utils import ErrorList
from django.utils.functional import cached_property
import taggit.forms as taggit_forms

from recipes.models import RecipeGrain, AllGrainRecipe, RecipeHop, RecipeOther, Yeast, Fermentable, Hop, Other, RecipeProfile, \
    BaseRecipe, ExtractRecipe


__author__ = 'sam'

# RecipeGrainLayout = Layout(
#     Div(
#         'fermentable',
#         'quantity',
#         'unit',
#         'steeped',
#         Field('recipe', type='hidden'),
#         'DELETE',
#         css_class='form-inline'
#     )
# )
#
# RecipeHopLayout = Layout(
#     Div(
#         'hop',
#         'alpha_acid_rating',
#         'quantity',
#         'ibu',
#         'when_to_add',
#         Field('recipe', type='hidden'),
#         'DELETE',
#         css_class='form-inline'
#     )
# )
#
# RecipeOtherLayout = Layout(
#     Div(
#         'other',
#         'quantity',
#         'unit',
#         'when_to_add',
#         Field('recipe', type='hidden'),
#         'DELETE',
#         css_class='form-inline'
#     )
# )
#
#
# class Panel(Div):
#     template = "bootstrap3/panel.html"
#
#     def __init__(self, name, *fields, **kwargs):
#         super().__init__(*fields, **kwargs)
#         self.name = name
#         if not self.css_id:
#             self.css_id = slugify(self.name)


class RecipeGrainForm(forms.ModelForm):

    class Meta:
        model = RecipeGrain
        exclude = []

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
                 label_suffix=None, empty_permitted=False, instance=None, **kwargs):
        steep = kwargs.pop('steep')
        fermentable_choices = kwargs.pop('fermentable_choices', None)
        super().__init__(data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, instance)

        if fermentable_choices:
            self.fields['fermentable'].choices = fermentable_choices

        if steep is False:
            del self.fields['steeped']


class RecipeHopForm(forms.ModelForm):
    # helper = FormHelper()
    # helper.form_tag = False
    # helper.template = 'bootstrap3/table_inline_formset.html'
    # helper.layout = RecipeHopLayout
    # helper.disable_csrf = True
    # helper.form_show_errors = True

    class Meta:
        model = RecipeHop
        exclude = []

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
                 label_suffix=None, empty_permitted=False, instance=None, **kwargs):
        hop_choices = kwargs.pop('hop_choices', None)
        super().__init__(data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, instance)
        if hop_choices:
            self.fields['hop'].choices = hop_choices

    def save(self, commit=True):
        # Only try and calculate the fields if the recipe has already been saved
        if self.instance.recipe.pk:
            self.instance.calculate_fields(only_empty=True)
        return super().save(commit)


class RecipeOtherForm(forms.ModelForm):
    # helper = FormHelper()
    # helper.form_tag = False
    # helper.template = 'bootstrap3/table_inline_formset.html'
    # helper.layout = RecipeOtherLayout
    # helper.disable_csrf = True
    # helper.form_show_errors = True

    class Meta:
        model = RecipeOther
        exclude = []

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
                 label_suffix=None, empty_permitted=False, instance=None, **kwargs):
        other_choices = kwargs.pop('other_choices', None)
        super().__init__(data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, instance)
        if other_choices:
            self.fields['other'].choices = other_choices



# RECIPE_GENERAL_PANEL = Panel(
#     'General',
#     'public',
#     'style',
#     AppendedText('target_volume', 'L'),
#     Field('description', spellcheck='true'),
#     'original_gravity',
#     'expected_final_gravity',
#     Field('owner', type='hidden'),
# )
#
# RECIPE_FERMENTATION_PANEL = Panel(
#     'Fermentation',
#     'yeast',
#     AppendedText('fermentation_temp', '°C'),
#     'conditioning_time',
#     'conditioning_time_quantifier',
#     AppendedText('conditioning_temp', '°C'),
# )
#
# RECIPE_OTHER_ADDITIONS_PANEL = Panel(
#     'Other Additions',
#     HTML(
#         '{% load crispy_forms_tags %}'
#         '{% crispy recipeother_formset recipeother_formset.form.helper %}'
#     )
# )
#
# RECIPEGRAIN_FORMSET = HTML(
#     '{% load crispy_forms_tags %}'
#     '{% crispy recipegrain_formset recipegrain_formset.form.helper %}'
# )


class BaseRecipeForm(forms.ModelForm):
    yeast = ModelChoiceField(queryset=Yeast.objects.select_related('lab').all())

    # helper = FormHelper()
    # helper.layout = Layout(
    #     Field('name', spellcheck='true'),
    #     RECIPE_GENERAL_PANEL,
    #     Panel(
    #         'Fermentables',
    #         RECIPEGRAIN_FORMSET
    #     ),
    #     RECIPE_OTHER_ADDITIONS_PANEL,
    #     RECIPE_FERMENTATION_PANEL,
    # )
    # helper.form_show_errors = True
    # helper.error_text_inline = True
    # helper.form_id = 'recipe_form'

    class Meta:
        model = BaseRecipe
        fields = ['name', 'public', 'style', 'description', 'target_volume', 'original_gravity',
                  'expected_final_gravity', 'owner', 'yeast', 'fermentation_temp', 'conditioning_time',
                  'conditioning_time_quantifier', 'conditioning_temp']
        widgets = {'owner': forms.HiddenInput(),
                   'name': forms.TextInput(attrs={'spellcheck': 'true'}),
                   'description': forms.Textarea(attrs={'spellcheck': 'true'}), }


# RECIPEHOP_FORMSET = HTML(
#     '{% load crispy_forms_tags %}'
#     '{% crispy recipehop_formset recipehop_formset.form.helper %}'
# )
#
# RECIPEBOIL_PANEL = Panel(
#     'Boil',
#     AppendedText('boil_liquor', 'L'),
#     AppendedText('boil_time', 'minutes'),
#     RECIPEHOP_FORMSET
# )


class ExtractRecipeForm(forms.ModelForm):
    yeast = ModelChoiceField(queryset=Yeast.objects.select_related('lab').all())

    # helper = FormHelper()
    # helper.layout = Layout(
    #     'name',
    #     RECIPE_GENERAL_PANEL,
    #     Panel(
    #         'Fermentables',
    #         AppendedText('steep_liquor', 'L'),
    #         AppendedText('steep_time', 'minutes'),
    #         AppendedText('steep_temperature', '°C'),
    #         RECIPEGRAIN_FORMSET
    #     ),
    #     RECIPEBOIL_PANEL,
    #     RECIPE_OTHER_ADDITIONS_PANEL,
    #     RECIPE_FERMENTATION_PANEL,
    #
    # )
    # helper.form_show_errors = True
    # helper.error_text_inline = True
    # helper.form_id = 'recipe_form'

    class Meta:
        model = ExtractRecipe
        fields = ['name', 'public', 'style', 'description', 'target_volume', 'original_gravity',
                  'expected_final_gravity', 'owner', 'yeast', 'fermentation_temp', 'conditioning_time',
                  'conditioning_time_quantifier', 'conditioning_temp', 'steep_liquor', 'steep_time',
                  'steep_temperature', 'boil_liquor', 'boil_time']
        widgets = {'owner': forms.HiddenInput(),
                   'name': forms.TextInput(attrs={'spellcheck': 'true'}),
                   'description': forms.Textarea(attrs={'spellcheck': 'true'}), }


class AllGrainRecipeForm(forms.ModelForm):
    yeast = ModelChoiceField(queryset=Yeast.objects.select_related('lab').all())

    # helper = FormHelper()
    # helper.layout = Layout(
    #     'name',
    #     RECIPE_GENERAL_PANEL,
    #     Panel(
    #         'Mash',
    #         AppendedText('mash_liquor', 'L'),
    #         AppendedText('mash_thickness', 'kg/L'),
    #         AppendedText('sparge_volume', 'L'),
    #         AppendedText('mash_time', 'minutes'),
    #         AppendedText('mash_temperature', '°C'),
    #         RECIPEGRAIN_FORMSET
    #     ),
    #     RECIPEBOIL_PANEL,
    #     RECIPE_OTHER_ADDITIONS_PANEL,
    #     RECIPE_FERMENTATION_PANEL,
    #
    # )
    # helper.form_show_errors = True
    # helper.error_text_inline = True
    # helper.form_id = 'recipe_form'

    class Meta:
        model = AllGrainRecipe
        fields = ['name', 'public', 'style', 'description', 'target_volume', 'original_gravity',
                  'expected_final_gravity', 'owner', 'mash_liquor', 'mash_thickness', 'sparge_volume',
                  'mash_time', 'mash_temperature', 'boil_liquor', 'boil_time', 'yeast',
                  'fermentation_temp', 'conditioning_time', 'conditioning_time_quantifier', 'conditioning_temp']
        widgets = {'owner': forms.HiddenInput(),
                   'name': forms.TextInput(attrs={'spellcheck': 'true'}),
                   'description': forms.Textarea(attrs={'spellcheck': 'true'}), }

    def save(self, commit=True):
        self.instance.calculate_fields(only_empty=True)
        return super().save(commit)


# OWNER_PUBLIC_HIDDEN_FIELDS = Div(Field('owner', type='hidden'), Field('public', type='hidden'))


class FermentableForm(forms.ModelForm):
    list_viewname = 'fermentable-list'

    class Meta:
        model = Fermentable
        fields = ['name', 'description', 'type', 'lovibond_rating', 'requires_mashing', 'extract_potential', 'owner',
                  'public', 'tags']
        widgets = {
            'owner': forms.HiddenInput(),
            'public': forms.HiddenInput(),
            'name': forms.TextInput(attrs={'spellcheck': 'true'}),
            'description': forms.Textarea(attrs={'spellcheck': 'true'}),
            'tags': taggit_forms.TagWidget(attrs={'class': 'form-control', 'spellcheck': 'true'})
        }


class HopForm(forms.ModelForm):
    list_viewname = 'hop-list'

    class Meta:
        model = Hop
        fields = ['name', 'description', 'country_of_origin', 'alpha_acid_low', 'alpha_acid_high', 'intensity',
                  'owner', 'public', 'tags']
        widgets = {
            'owner': forms.HiddenInput(),
            'public': forms.HiddenInput(),
            'name': forms.TextInput(attrs={'spellcheck': 'true'}),
            'description': forms.Textarea(attrs={'spellcheck': 'true'}),
            'tags': taggit_forms.TagWidget(attrs={'class': 'form-control', 'spellcheck': 'true'})
        }


class OtherForm(forms.ModelForm):
    list_viewname = 'other-list'

    class Meta:
        model = Other
        fields = ['name', 'description', 'owner', 'public', 'tags']
        widgets = {
            'owner': forms.HiddenInput(),
            'public': forms.HiddenInput(),
            'name': forms.TextInput(attrs={'spellcheck': 'true'}),
            'description': forms.Textarea(attrs={'spellcheck': 'true'}),
            'tags': taggit_forms.TagWidget(attrs={'class': 'form-control', 'spellcheck': 'true'})
        }


class YeastForm(forms.ModelForm):
    list_viewname = 'yeast-list'

    class Meta:
        model = Yeast
        fields = ['lab', 'name', 'description', 'attenuation', 'style', 'alternatives', 'owner', 'public', 'tags']
        widgets = {
            'owner': forms.HiddenInput(),
            'public': forms.HiddenInput(),
            'name': forms.TextInput(attrs={'spellcheck': 'true'}),
            'description': forms.Textarea(attrs={'spellcheck': 'true'}),
            'tags': taggit_forms.TagWidget(attrs={'class': 'form-control', 'spellcheck': 'true'})
        }

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
                 label_suffix=None, empty_permitted=False, instance=None):
        super().__init__(data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, instance)
        self.fields['alternatives'].queryset = Yeast.objects.select_related('lab').all().order_by('lab', 'name')


class RecipeProfileForm(forms.ModelForm):
    # helper = FormHelper()
    # helper.layout = Layout(
    #     AppendedText('mash_efficiency', '%'),
    #     AppendedText('grain_absorption', 'L/kg'),
    #     AppendedText('evaporation_rate', 'L/hr'),
    #     AppendedText('trub_loss', 'L'),
    #     Div(
    #         Submit('submit', 'Save'),
    #         css_class='btn-group'
    #     )
    # )

    class Meta:
        model = RecipeProfile
        fields = ['mash_efficiency', 'grain_absorption', 'evaporation_rate', 'trub_loss']


class BaseBaseFormSet(BaseInlineFormSet):
    """
    Base that just sets self.user with the user kwarg. The formsets that extend this class
    use the user to limit the objects to those that the user is allowed to view.
    """

    empty_choice = [(None, '---------')]

    def __init__(self, data=None, files=None, instance=None, save_as_new=False, prefix=None, queryset=None, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(data, files, instance, save_as_new, prefix, queryset, **kwargs)


class RecipeGrainBaseFormSet(BaseBaseFormSet):
    @cached_property
    def forms(self):
        kwargs = {
            'fermentable_choices': self.empty_choice + [(f['id'], f['name']) for f in
                                                        Fermentable.objects.public_or_owner(self.user).values(
                                                            'id', 'name')]
        }

        forms = [self._construct_form(i, **kwargs) for i in range(self.total_form_count())]
        return forms


class RecipeHopBaseFormSet(BaseBaseFormSet):
    @cached_property
    def forms(self):
        kwargs = {'hop_choices': self.empty_choice + [(f['id'], f['name']) for f in
                                                      Hop.objects.public_or_owner(self.user).values('id', 'name')]}

        forms = [self._construct_form(i, **kwargs) for i in range(self.total_form_count())]
        return forms


class RecipeOtherBaseFormSet(BaseBaseFormSet):
    @cached_property
    def forms(self):
        kwargs = {'other_choices': self.empty_choice + [(f['id'], f['name']) for f in
                                                        Other.objects.public_or_owner(self.user).values('id', 'name')]}

        forms = [self._construct_form(i, **kwargs) for i in range(self.total_form_count())]
        return forms