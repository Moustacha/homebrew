__author__ = 'sam'

from django import template

register = template.Library()


@register.filter
def boolean(value):
    if value:
        return 'Yes'
    else:
        return 'No'
