__author__ = 'sam'

from django import template

register = template.Library()

@register.simple_tag(name='is_stocked')
def is_stocked(request, object):
    if object.is_stocked(request.user):
        return_tag = """<span class="label label-success"><i class="glyphicon glyphicon-ok"></i></span>"""
    else:
        return_tag = """<span class="label label-danger"><i class="glyphicon glyphicon-remove"></i></span>"""
    return return_tag