from django.forms import DecimalField

__author__ = 'sam'

from django import template
import markdown
from django.utils.safestring import mark_safe


register = template.Library()

@register.simple_tag(name='field_step')
def field_step(field):
    if isinstance(field, DecimalField):
        return 1/(10**field.decimal_places)
    else:
        return 1
    
@register.filter
def markdownify(text):
    return mark_safe(markdown.markdown(text))