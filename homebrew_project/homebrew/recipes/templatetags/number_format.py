__author__ = 'sam'

from django import template

register = template.Library()


@register.filter
def percentage(value):
    return '{:0.2f}%'.format(value)
