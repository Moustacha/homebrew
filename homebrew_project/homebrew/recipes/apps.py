__author__ = 'sam'

from django.apps import AppConfig


class RecipesConfig(AppConfig):
    name = 'recipes'
    verbose_name = "Homebrew Recipes"
