from django.conf.urls import patterns, url
from django.views.generic import DetailView
from django.views.generic.base import RedirectView

from .forms import FermentableForm, OtherForm, YeastForm, HopForm, BaseRecipeForm, ExtractRecipeForm, AllGrainRecipeForm
from .models import Fermentable, Hop, Other, Yeast, BaseRecipe, ExtractRecipe, AllGrainRecipe
from .views.recipe import RecipeUpdateView, RecipeCreateView, \
    RecipeListView, RecipeDeleteView, \
    IngredientSearchListView, RecipeProfileUpdateView
from .views.filters import FermentableFilter, OtherFilter, YeastFilter, HopFilter
from .views.ingredient import IngredientUpdateView, IngredientDeleteView, IngredientCreateView, IngredientListView, \
    IngredientDetailView
from .views.tables import FermentableTable, OtherTable, YeastTable, HopTable


__author__ = 'sam'

urlpatterns = patterns(
    '',
    url(r'^$', RedirectView.as_view(pattern_name='recipe-list', permanent=True), name='recipe-home'),
    url(r'^list$', RecipeListView.as_view(), name='recipe-list'),
    url(r'^search', IngredientSearchListView.as_view(), name='recipe-search'),
    url(r'^create_basic$', RecipeCreateView.as_view(model=BaseRecipe, form_class=BaseRecipeForm,
                                                        template_name='recipe/recipe_create_basic.html'),
        name='recipe-create-basic'),
    url(r'^create_extract$', RecipeCreateView.as_view(model=ExtractRecipe, form_class=ExtractRecipeForm,
                                                        template_name='recipe/recipe_create_extract.html'),
        name='recipe-create-extract'),
    url(r'^create_all_grain$', RecipeCreateView.as_view(model=AllGrainRecipe, form_class=AllGrainRecipeForm,
                                                        template_name='recipe/recipe_create_all_grain.html'),
        name='recipe-create-all_grain'),
    url(r'^update/(?P<slug>[\w-]+)/$', RecipeUpdateView.as_view(), name='recipe-update'),

    url(r'^(?P<slug>[\w-]+)/$',
        DetailView.as_view(model=BaseRecipe, template_name='recipe/recipe_detail.html',
                           queryset=BaseRecipe.objects.all().select_related('style', 'yeast',
                                                                        'yeast__lab')),
        name='recipe-detail'),
    url(r'^(?P<slug>[\w-]+)/delete$', RecipeDeleteView.as_view(), name='recipe-delete'),

    # Ingredient views
    url(r'^fermentable/list$',
        IngredientListView.as_view(model=Fermentable, table_class=FermentableTable,
                                   filter_class=FermentableFilter, url_name='fermentable-list'),
        name='fermentable-list'),
    url(r'^hop/list$',
        IngredientListView.as_view(model=Hop, table_class=HopTable, filter_class=HopFilter,
                                   url_name='hop-list'),
        name='hop-list'),
    url(r'^other/list$',
        IngredientListView.as_view(model=Other, table_class=OtherTable, filter_class=OtherFilter,
                                   url_name='other-list'),
        name='other-list'),
    url(r'^yeast/list$',
        IngredientListView.as_view(model=Yeast, table_class=YeastTable, filter_class=YeastFilter,
                                   url_name='yeast-list'),
        name='yeast-list'),

    url(r'^fermentable/update/(?P<slug>[\w-]+)/$',
        IngredientUpdateView.as_view(model=Fermentable, form_class=FermentableForm),
        name='fermentable-update'),
    url(r'^hop/update/(?P<slug>[\w-]+)/$',
        IngredientUpdateView.as_view(model=Hop, form_class=HopForm), name='hop-update'),
    url(r'^other/update/(?P<slug>[\w-]+)/$',
        IngredientUpdateView.as_view(model=Other, form_class=OtherForm), name='other-update'),
    url(r'^yeast/update/(?P<slug>[\w-]+)/$',
        IngredientUpdateView.as_view(model=Yeast, form_class=YeastForm), name='yeast-update'),

    url(r'^fermentable/view/(?P<slug>[\w-]+)/$',
        IngredientDetailView.as_view(model=Fermentable, template_name='ingredient/fermentable_detail.html'),
        name='fermentable-detail'),
    url(r'^hop/view/(?P<slug>[\w-]+)/$',
        IngredientDetailView.as_view(model=Hop, template_name='ingredient/hop_detail.html'),
        name='hop-detail'),
    url(r'^other/view/(?P<slug>[\w-]+)/$',
        IngredientDetailView.as_view(model=Other, template_name='ingredient/other_detail.html'),
        name='other-detail'),
    url(r'^yeast/view/(?P<slug>[\w-]+)/$',
        IngredientDetailView.as_view(model=Yeast, template_name='ingredient/yeast_detail.html'),
        name='yeast-detail'),

    url(r'^fermentable/delete/(?P<slug>[\w-]+)/$',
        IngredientDeleteView.as_view(model=Fermentable, success_view_name='fermentable-list'),
        name='fermentable-delete'),
    url(r'^hop/delete/(?P<slug>[\w-]+)/$',
        IngredientDeleteView.as_view(model=Hop, success_view_name='hop-list'), name='hop-delete'),
    url(r'^other/delete/(?P<slug>[\w-]+)/$',
        IngredientDeleteView.as_view(model=Other, success_view_name='other-list'),
        name='other-delete'),
    url(r'^yeast/delete/(?P<slug>[\w-]+)/$',
        IngredientDeleteView.as_view(model=Yeast, success_view_name='yeast-list'),
        name='yeast-delete'),

    url(r'^fermentable/create/$',
        IngredientCreateView.as_view(model=Fermentable, form_class=FermentableForm),
        name='fermentable-create'),
    url(r'^hop/create/$', IngredientCreateView.as_view(model=Hop, form_class=HopForm),
        name='hop-create'),
    url(r'^other/create/$', IngredientCreateView.as_view(model=Other, form_class=OtherForm),
        name='other-create'),
    url(r'^yeast/create/$', IngredientCreateView.as_view(model=Yeast, form_class=YeastForm),
        name='yeast-create'),

    # Recipe Profile
    url(r'^accounts/settings/recipes$', RecipeProfileUpdateView.as_view(),
        name='account_recipe_profile'),
)
