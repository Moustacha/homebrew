from decimal import Decimal
from django.conf import settings
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models, transaction
from django.db.models import Q, Sum
from taggit.managers import TaggableManager
from taggit.models import TagBase, ItemBase
from uuslug import uuslug

from recipes import calculations


SRM_HTML_COLOUR = ['#FFE699', '#FFD878', '#FFCA5A', '#FFBF42', '#FBB123', '#F8A600', '#F39C00', '#EA8F00', '#E58500',
                   '#DE7C00',
                   '#D77200', '#CF6900', '#CB6200', '#C35900', '#BB5100', '#B54C00', '#B04500', '#A63E00', '#A13700',
                   '#9B3200',
                   '#952D00', '#8E2900', '#882300', '#821E00', '#7B1A00', '#771900', '#701400', '#6A0E00', '#660D00',
                   '#5E0B00',
                   '#5A0A02', '#600903', '#520907', '#4C0505', '#470606', '#440607', '#3F0708', '#3B0607', '#3A070B',
                   '#36080A']


def html_colour_for_srm(srm):
    # an SRM of less than 1 doesn't have a lookup
    index = srm if srm >= 1 else 1

    # an SRM of greater than 40 doesn't have a lookup either
    index = index if index <= 40 else 40
    return SRM_HTML_COLOUR[index - 1]

############################################
#### Tagging
############################################

# taggit manager needs to implement get_internal_type() to stop the problems happening with migrations.
# This can be removed once taggit has been updated with the fix
# https://github.com/alex/django-taggit/pull/286
class FixedTaggableManager(TaggableManager):

    def get_internal_type(self):
        return 'ManyToManyField'


class LowercaseTag(TagBase):
    """
    A simple tag that just makes sure that the tag being saved in lowercase for data consistency
    """
    class Meta:
        verbose_name = "Tag"
        verbose_name_plural = "Tags"
        app_label = 'recipes'

    def save(self, *args, **kwargs):
        self.name = self.name.lower()

        # Check if this name already exists
        qs = LowercaseTag.objects.filter(name=self.name)
        if qs.count() == 1:
            existing = qs[0]

            # Copy the existing tag's details to this instance
            self.pk = existing.pk
            self.id = existing.id
            self.slug = existing.slug
        else:
            return super().save(*args, **kwargs)


class LowercaseTaggedItemBase(ItemBase):
    """
    Reimplementation of TaggedItemBase to use LowercaseTag
    """
    tag = models.ForeignKey(LowercaseTag,
                            related_name="%(app_label)s_%(class)s_items")

    class Meta:
        abstract = True
        app_label = 'recipes'

    @classmethod
    def tags_for(cls, model, instance=None):
        if instance is not None:
            return cls.tag_model().objects.filter(**{
                '%s__content_object' % cls.tag_relname(): instance
            })
        return cls.tag_model().objects.filter(**{
            '%s__content_object__isnull' % cls.tag_relname(): False
        }).distinct()


class TaggedFermentable(LowercaseTaggedItemBase):
    content_object = models.ForeignKey('Fermentable')

    class Meta:
        app_label = 'recipes'


class TaggedHop(LowercaseTaggedItemBase):
    content_object = models.ForeignKey('Hop')

    class Meta:
        app_label = 'recipes'


class TaggedOther(LowercaseTaggedItemBase):
    content_object = models.ForeignKey('Other')

    class Meta:
        app_label = 'recipes'


class TaggedYeast(LowercaseTaggedItemBase):
    content_object = models.ForeignKey('Yeast')

    class Meta:
        app_label = 'recipes'


############################################
#### Managers
############################################


class PublicManager(models.Manager):
    def public(self):
        return self.get_queryset().filter(public=True)

    def public_or_owner(self, user):
        q = Q(public=True)
        if user.is_authenticated():
            q = q | Q(owner=user)
        return self.get_queryset().filter(q)


class YeastManager(PublicManager):
    def public_or_owner(self, user):
        return super().public_or_owner(user).select_related('lab')


class RecipeManager(PublicManager):
    def public_or_owner(self, user):
        return super().public_or_owner(user)

    def public(self):
        return super().public()

    def latest_recipes(self, user):
        return self.public_or_owner(user).order_by('-created_date')[:5]

############################################
#### Application Models
############################################


class RecipeProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    mash_efficiency = models.DecimalField(max_digits=5, decimal_places=2, default=70,
                                          validators=[MinValueValidator(0), MaxValueValidator(100)])
    grain_absorption = models.DecimalField(max_digits=5, decimal_places=2,
                                           help_text='How much water is absorbed by the grain, per kg', default=1.1)
    evaporation_rate = models.DecimalField(max_digits=5, decimal_places=2,
                                           help_text='How much water evaporates from the boil, per hour', default=2.1)
    trub_loss = models.DecimalField(max_digits=5, decimal_places=2,
                                    help_text='How much you expect to lose to the hot break and hops after the boil',
                                    default=1)

    class Meta:
        app_label = 'recipes'

    def get_absolute_url(self):
        return reverse('account_recipe_profile')


class Ingredient(models.Model):
    '''
        Models that extend this should implement a custom __str__
        that is used for the slug field.
    '''

    class Meta:
        abstract = True
        app_label = 'recipes'

    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    slug = models.SlugField(editable=False)

    owner = models.ForeignKey(User)
    public = models.BooleanField(default=False)

    objects = PublicManager()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.id:
            self.slug = uuslug(self.name, instance=self, max_length=50)
        super().save(force_insert, force_update, using, update_fields)

    def is_stocked(self, user):
        return self.stock.filter(owner=user).exists()


class Fermentable(Ingredient):
    TYPE_CHOICES = (
        ('MALTGR', 'Malted Grain'),
        ('MALTEX', 'Malted Extract'),
        ('HOPMALTEX', 'Hopped Malted Extract'),
        ('ADJUNCT', 'Adjunct'),
        ('SUGAR', 'Sugar')
    )
    type = models.CharField(max_length=20, choices=TYPE_CHOICES)
    lovibond_rating = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        validators=[MinValueValidator(0)]
    )
    requires_mashing = models.BooleanField(default=False)
    extract_potential = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        default=70,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        help_text='The theoretical maximum percentage that will be converted to sugar'
    )

    tags = FixedTaggableManager(blank=True, through=TaggedFermentable)

    class Meta:
        ordering = ('name', 'type', )
        index_together = [
            ['public', 'owner']
        ]
        app_label = 'recipes'

    def __str__(self):
        return '{name} ({type})'.format(type=self.get_type_display(), name=self.name,
                                        description=self.description).title()

    def get_absolute_url(self):
        return reverse('fermentable-detail', kwargs={'slug': self.slug})

    def get_ebc(self):
        return calculations.srm_to_ebc(self.get_srm())

    def get_srm(self):
        return calculations.lovibond_to_srm(self.lovibond_rating)

    def html_colour(self):
        srm = int(format(self.get_srm(), '.0f'))
        return html_colour_for_srm(srm)


class Hop(Ingredient):
    country_of_origin = models.CharField(max_length=100)
    alpha_acid_low = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(100)])
    alpha_acid_high = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(100)])
    intensity = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(10)],
                                    help_text='1 = low; 10 = high')

    tags = FixedTaggableManager(blank=True, through=TaggedHop)

    class Meta:
        ordering = ('name', )
        index_together = [
            ['public', 'owner']
        ]
        app_label = 'recipes'

    def __str__(self):
        return '{name}'.format(name=self.name, description=self.description).title()

    def get_absolute_url(self):
        return reverse('hop-detail', kwargs={'slug': self.slug})


class Other(Ingredient):

    tags = FixedTaggableManager(blank=True, through=TaggedOther)

    class Meta:
        ordering = ('name', )
        index_together = [
            ['public', 'owner']
        ]
        app_label = 'recipes'

    def __str__(self):
        return '{name}'.format(name=self.name, description=self.description).title()

    def get_absolute_url(self):
        return reverse('other-detail', kwargs={'slug': self.slug})


class Lab(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        ordering = ('name', )
        app_label = 'recipes'

    def __str__(self):
        return self.name


class Style(models.Model):
    name = models.CharField(max_length=100)
    co_volume = models.DecimalField(max_digits=3, decimal_places=2, default=2,
                                    help_text='Suggested volumes of CO\u2082 per volume of beer')

    class Meta:
        ordering = ('name', )
        app_label = 'recipes'

    def __str__(self):
        return self.name


class Yeast(Ingredient):
    style = models.ManyToManyField(Style, blank=True)
    lab = models.ForeignKey(Lab, help_text='Brand/Manufacturer/Lab', on_delete=models.PROTECT)
    alternatives = models.ManyToManyField('self', blank=True)
    attenuation = models.DecimalField(max_digits=5, decimal_places=2, default=75,
                                      validators=[MinValueValidator(0), MaxValueValidator(100)])

    objects = YeastManager()
    tags = FixedTaggableManager(blank=True, through=TaggedYeast)

    class Meta:
        unique_together = (('name', 'lab'),)
        ordering = ('lab', 'name')
        app_label = 'recipes'

    def __str__(self):
        return '{lab} {name}'.format(lab=self.lab, name=self.name).title()

    def get_absolute_url(self):
        return reverse('yeast-detail', kwargs={'slug': self.slug})


class BaseRecipe(models.Model):

    class Meta:
        index_together = [
            ['public', 'owner']
        ]
        app_label = 'recipes'

    TIME_QUANTIFIER_CHOICES = (
        ('d', 'Days'),
        ('w', 'Weeks'),
        ('y', 'Years'),
    )
    objects = RecipeManager()

    name = models.CharField(max_length=200)
    style = models.ForeignKey(Style, on_delete=models.PROTECT)
    description = models.TextField(null=True, blank=True)

    target_volume = models.IntegerField(help_text='Target volume of beer in L')

    original_gravity = models.DecimalField(max_digits=4, decimal_places=3,
                                           validators=[MinValueValidator(0), MaxValueValidator(2)], default=0,
                                           help_text='Leave as 0 to calculate')
    expected_final_gravity = models.DecimalField(max_digits=4, decimal_places=3,
                                                 validators=[MinValueValidator(0), MaxValueValidator(2)], default=0,
                                                 help_text='Leave as 0 to calculate')

    fermentation_temp = models.IntegerField()
    conditioning_time = models.IntegerField()
    conditioning_time_quantifier = models.CharField(max_length=1, choices=TIME_QUANTIFIER_CHOICES)
    conditioning_temp = models.IntegerField()
    yeast = models.ForeignKey(Yeast, on_delete=models.PROTECT)

    fermentables = models.ManyToManyField(Fermentable, through='RecipeGrain', through_fields=('recipe', 'fermentable'))
    others = models.ManyToManyField(Other, through='RecipeOther', through_fields=('recipe', 'other'))

    owner = models.ForeignKey(User)
    public = models.BooleanField(help_text='Do you want other people to be able to view this recipe?', default=False)
    slug = models.SlugField(editable=False)
    created_date = models.DateTimeField(auto_now_add=True)
    last_updated_date = models.DateTimeField(auto_now=True)

    cloned_from = models.ForeignKey('self', null=True, editable=False, blank=True)

    def __str__(self):
        return self.name.title()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.id:
            self.slug = uuslug(self.name, instance=self, max_length=50)
        super().save(force_insert, force_update, using, update_fields)

    def get_absolute_url(self):
        return reverse('recipe-detail', kwargs={'slug': self.slug})

    def get_update_url(self):
        return reverse('recipe-update', kwargs={'slug': self.slug})

    def get_delete_url(self):
        return reverse('recipe-delete', kwargs={'slug': self.slug})

    def get_bitterness_rating(self):
        pass

    def calculate_final_gravity(self):
        """
            Requires the original gravity and yeast have been associated
        :return:
        """
        return calculations.final_gravity(self.original_gravity, self.yeast.attenuation)

    def estimated_original_gravity(self):
        """
            Requires that there have been fermentables associated with this recipe
        :return:
        """
        gravity_points = self.get_gravity_points()
        return calculations.original_gravity(gravity_points, self.target_volume)

    def get_gravity_points(self):
        return sum([
            calculations.fermentable_gravity_points(x.fermentable.extract_potential, x.get_weight_in_kg(),
                                                    95 if x.steeped == False else self.owner.recipeprofile.mash_efficiency) # Because it's extract efficiency will be 95%
            for x in self.recipegrain_set.all()])

    def alcohol_by_volume(self):
        return calculations.alcohol_by_volume(self.original_gravity, self.expected_final_gravity)

    def calculate_fields(self, only_empty=True):
        if only_empty:
            if self.original_gravity == 0:
                self.original_gravity = self.estimated_original_gravity()
            if self.expected_final_gravity == 0:
                self.expected_final_gravity = self.calculate_final_gravity()
        else:
            self.original_gravity = self.estimated_original_gravity()
            self.expected_final_gravity = self.calculate_final_gravity()

    def html_colour(self):
        srm = int(format(self.get_srm(), '.0f'))
        return html_colour_for_srm(srm)

    def get_srm(self):
        return calculations.ebc_to_srm(self.get_ebc())

    def get_ebc(self):
        malt_colour_units = sum(
            [calculations.malt_colour_units(x.fermentable.lovibond_rating, x.get_weight_in_kg(), self.target_volume) for x in
             self.recipegrain_set.all().select_related('fermentable__lovibond_rating')])
        return calculations.ebc(malt_colour_units)

    @transaction.atomic
    def clone_for_batch(self, user):
        self.cloned_from = BaseRecipe.objects.get(pk=self.pk)

        self.pk = None
        self.id = None
        self.owner = user
        self.public = False
        self.name = 'Custom ' + self.name
        self.save()

        self._update_children(RecipeOther.objects.filter(recipe=self.cloned_from))
        self._update_children(RecipeGrain.objects.filter(recipe=self.cloned_from))

    def _update_children(self, children):
        """
        Used by clone_for_batch for updating child objects (fermentables, others, etc)

        Have to do this because of the intermediary model
        :param children:
        :return:
        """
        for o in children:
            o.pk = None
            o.id = None
            o.recipe = self
            o.save()

    def get_recipegrain_ordered(self):
        return self.recipegrain_set.select_related('fermentable').all().order_by('-unit', '-quantity')

    def get_recipeother_ordered(self):
        return self.recipeother_set.select_related('other').all().order_by('id')


class ExtractRecipe(BaseRecipe):
    boil_liquor = models.DecimalField(max_digits=10, decimal_places=2, default=0, help_text='Leave as 0 to calculate')
    boil_time = models.IntegerField(help_text='Time in minutes')

    steep_liquor = models.DecimalField(max_digits=10, decimal_places=2, default=0, help_text='Leave as 0 to calculate',
                                       blank=True, null=True)
    steep_time = models.IntegerField(help_text='Time in minutes', blank=True, null=True)
    steep_temperature = models.IntegerField(blank=True, null=True)

    hops = models.ManyToManyField(Hop, through='RecipeHop', through_fields=('recipe', 'hop'))

    objects = RecipeManager()

    class Meta:
        app_label = 'recipes'

    def get_bitterness_rating(self):
        return self.recipehop_set.aggregate(Sum('ibu'))['ibu__sum']

    def calculate_boil_liquor(self):
        """
            No pre-requisites
        :return:
        """
        return calculations.boil_volume(self.target_volume,
                                        self.boil_time,
                                        self.owner.recipeprofile.evaporation_rate,
                                        self.owner.recipeprofile.trub_loss)

    def starting_volume(self):
        return self.boil_liquor

    def calculate_fields(self, only_empty=True):
        super().calculate_fields(only_empty=only_empty)

        if only_empty:
            if self.boil_liquor == 0:
                self.boil_liquor = self.calculate_boil_liquor()
        else:
            self.boil_liquor = self.calculate_boil_liquor()

        # The steeping and boil liquor should be the same. However they should be the larger
        # of the two values if they are different.
        if (self.steep_liquor is not None and self.steep_liquor != 0) or \
                self.recipegrain_set.filter(steeped=True).count() > 0:
            # Steep liquor might be None, so take care
            self.steep_liquor = self.boil_liquor = Decimal(
                max(self.boil_liquor, self.steep_liquor if self.steep_liquor is not None else 0))

        for rh in self.recipehop_set.all():
            rh.calculate_fields(only_empty)
            rh.save()

    @transaction.atomic
    def clone_for_batch(self, user):
        super().clone_for_batch(user)
        self._update_children(RecipeHop.objects.filter(recipe=self.cloned_from))

    def average_boil_gravity(self):
        return calculations.average_boil_gravity(self.get_gravity_points(),
                                                 self.starting_volume(),
                                                 0, # No grain, no water loss
                                                 calculations.water_loss_to_evaporation(
                                                     self.boil_time,
                                                     self.owner.recipeprofile.evaporation_rate))

    def get_recipehop_ordered(self):
        return self.recipehop_set.select_related('hop').all().order_by('id')

    def get_recipegrain_ordered(self):
        # Order by if the fermentable needs steeping
        return super().get_recipegrain_ordered().order_by('steeped')


class AllGrainRecipe(ExtractRecipe):

    mash_liquor = models.DecimalField(max_digits=10, decimal_places=2, default=0, help_text='Leave as 0 to calculate')
    sparge_volume = models.DecimalField(max_digits=10, decimal_places=2, default=0, help_text='Leave as 0 to calculate')
    mash_thickness = models.DecimalField(max_digits=10, decimal_places=2, default=2.1, help_text='The thickness of the mash, in L/kg')

    mash_time = models.IntegerField(help_text='Time in minutes')
    mash_temperature = models.IntegerField()

    objects = RecipeManager()

    class Meta:
        app_label = 'recipes'

    def total_grain_weight(self):
        kg_weight = self.recipegrain_set.filter(
            unit='kg').aggregate(Sum('quantity'))['quantity__sum']
        g_weight = self.recipegrain_set.filter(unit='g').aggregate(Sum('quantity'))['quantity__sum']

        if kg_weight is None:
            kg_weight = 0

        # There might be nothing for g_weight, so check before trying to calculate
        if g_weight is None:
            g_weight = 0
        else:
            g_weight = g_weight / 1000

        return kg_weight + g_weight

    def calculate_mash_liquor(self):
        """
            Requires that there have been fermentables associated with this recipe
        :return:
        """
        return calculations.strike_volume(self.total_grain_weight(), self.mash_thickness)

    def calculate_sparge_volume(self):
        first_runnings = calculations.first_runnings_volume(self.mash_liquor, self.total_grain_weight(),
                                                            self.owner.recipeprofile.grain_absorption)
        return calculations.sparge_volume(self.boil_liquor, first_runnings)

    def get_target_gravity_points(self):
        return calculations.recipe_gravity_points(self.original_gravity, self.target_volume)

    def get_gravity_points(self):
        return sum([
            calculations.fermentable_gravity_points(x.fermentable.extract_potential, x.get_weight_in_kg(),
                                                    self.owner.recipeprofile.mash_efficiency)
            for x in self.recipegrain_set.all()])

    def starting_volume(self):
        """
            Requires mash liquor and boil liquor have been entered or calculated
        :return:
        """
        return self.target_volume + calculations.water_loss_to_grain(
            self.total_grain_weight(),
            self.owner.recipeprofile.grain_absorption) + calculations.water_loss_to_evaporation(
            self.boil_time,
            self.owner.recipeprofile.evaporation_rate) + self.owner.recipeprofile.trub_loss

    def calculate_fields(self, only_empty=True):
        super().calculate_fields(only_empty=only_empty)
        if only_empty:
            if self.mash_liquor == 0:
                self.mash_liquor = self.calculate_mash_liquor()
            if self.sparge_volume == 0:
                self.sparge_volume = self.calculate_sparge_volume()
            # Update the mash thickness to keep it correct
            # Can only calculate if there is actually any grain added
            total_grain_weight = self.total_grain_weight()
            if total_grain_weight > 0:
                self.mash_thickness = calculations.mash_thickness(self.mash_liquor, total_grain_weight)
        else:
            self.mash_liquor = self.calculate_mash_liquor()
            self.sparge_volume = self.calculate_sparge_volume()



    def average_boil_gravity(self):
        return calculations.average_boil_gravity(self.get_gravity_points(),
                                                 self.starting_volume(),
                                                 calculations.water_loss_to_grain(
                                                     self.total_grain_weight(),
                                                     self.owner.recipeprofile.grain_absorption),
                                                 calculations.water_loss_to_evaporation(
                                                     self.boil_time,
                                                     self.owner.recipeprofile.evaporation_rate))

    @transaction.atomic
    def clone_for_batch(self, user):
        # Have to manually set the baserecipe_ptr_id to None so that
        # a new BaseRecipe is created. Seems to be that when the id of this instance
        # is set to None it sets the parent id (ExtractRecipe) to None, but not all
        # the way to the top. At some point when saving it checks for any ids up the
        # hierarchy and finds the BaseRecipe has an id, and uses that instead of creating
        # a new object.
        self.baserecipe_ptr_id = None
        super().clone_for_batch(user)

    def get_recipegrain_ordered(self):
        # Reset ordering here to be the same as in BaseRecipe
        return self.recipegrain_set.select_related('fermentable').all().order_by('-unit', '-quantity')


class RecipeGrain(models.Model):
    UNIT_CHOICES = (
        ('g', 'Grams'),
        ('kg', 'Kilograms')
    )
    recipe = models.ForeignKey(BaseRecipe)
    fermentable = models.ForeignKey(Fermentable, on_delete=models.PROTECT)
    quantity = models.DecimalField(max_digits=10, decimal_places=2)
    unit = models.CharField(max_length=10, choices=UNIT_CHOICES)
    steeped = models.BooleanField(default=False, blank=True)

    class Meta:
        app_label = 'recipes'

    def get_weight_in_kg(self):
        weight_in_kg = 0
        if self.unit == 'g':
            weight_in_kg = self.quantity / 1000
        elif self.unit == 'kg':
            weight_in_kg = self.quantity

        return weight_in_kg


class RecipeHop(models.Model):
    recipe = models.ForeignKey(ExtractRecipe)
    hop = models.ForeignKey(Hop, on_delete=models.PROTECT)
    alpha_acid_rating = models.DecimalField(max_digits=5, decimal_places=2,
                                            validators=[MinValueValidator(0), MaxValueValidator(100)])
    # These values should be *something*. They can be blank for validation purposes, but should still be a value.
    quantity = models.IntegerField(help_text='In grams', blank=True, default=0)
    ibu = models.DecimalField(max_digits=3, decimal_places=1, blank=True, default=0)
    when_to_add = models.IntegerField(
        help_text='Time in minutes from the end (If it is added at the start, enter the whole boil time)')

    class Meta:
        app_label = 'recipes'

    def calculate_ibu(self):
        return calculations.ibu(self.recipe.average_boil_gravity(), self.when_to_add, self.quantity,
                                self.alpha_acid_rating, self.recipe.target_volume)

    def calculate_quantity(self):
        # Need to calculate the hop utilisation first
        utilisation = calculations.alpha_acid_utilisation(self.recipe.average_boil_gravity(), self.when_to_add)

        return calculations.hop_weight(self.recipe.target_volume, self.ibu, self.alpha_acid_rating, utilisation)

    def calculate_fields(self, only_empty=True):
        # IBU and quantity are different sides of the same coin, so one can't be calculated without the other
        if only_empty:
            if self.ibu == 0 and self.quantity != 0:
                self.ibu = self.calculate_ibu()
            if self.quantity == 0 and self.ibu != 0:
                self.quantity = self.calculate_quantity()
        else:
            if self.quantity != 0:
                self.ibu = self.calculate_ibu()
            if self.ibu != 0:
                self.quantity = self.calculate_quantity()

    def display_when_to_add(self):
        if self.when_to_add == self.recipe.boil_time:
            return 'At the start of the boil'
        elif self.when_to_add == 0:
            return 'At the end of the boil'
        else:
            return 'For the last {} mins of the boil'.format(self.when_to_add)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        # Need to be defaulted to 0 manually as form validation allows them to be blank
        self.ibu = 0 if self.ibu is None else self.ibu
        self.quantity = 0 if self.quantity is None else self.quantity
        super().save(force_insert, force_update, using, update_fields)


class RecipeOther(models.Model):
    UNIT_CHOICES = (
        ('g', 'Grams'),
        ('kg', 'Kilograms'),
        ('tsp', 'Teaspoon'),
        ('cup', 'Cup'),
        ('sticks', 'Sticks'),
        ('pods', 'Pods'),
        ('ml', 'Millilitres')
    )
    recipe = models.ForeignKey(BaseRecipe)
    other = models.ForeignKey(Other, on_delete=models.PROTECT)
    quantity = models.IntegerField()
    unit = models.CharField(max_length=10, choices=UNIT_CHOICES)
    when_to_add = models.CharField(max_length=100)

    class Meta:
        app_label = 'recipes'
