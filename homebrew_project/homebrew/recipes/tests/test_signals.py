from django.contrib.auth.models import User
from django.test import TestCase

from recipes.models import RecipeProfile


__author__ = 'sam'


class SignalsTestCase(TestCase):
    def test_recipe_profile(self):
        user = User(username='test', password='test')
        user.save()

        self.assertEqual(1, RecipeProfile.objects.all().count())

        user.is_staff = True
        user.save()

        self.assertEqual(1, RecipeProfile.objects.all().count())
