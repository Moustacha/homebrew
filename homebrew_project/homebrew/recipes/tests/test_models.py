from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.test import TestCase

from recipes.models import Fermentable, Hop, Yeast, Lab, Style, AllGrainRecipe, RecipeHop
from storage.models import FermentableStock

__author__ = 'sam'


class ModelTestCase(TestCase):
    fixtures = ['Users', ]

    def setUp(self):
        self.user = User.objects.get(pk=1)
        super().setUp()

    def tearDown(self):
        super().tearDown()

    def testFermentableSave(self):
        f = Fermentable()
        f.type = Fermentable.TYPE_CHOICES[0][0]
        f.requires_mashing = False
        f.name = 'Test Grain'
        f.description = 'Test Description'
        f.lovibond_rating = 1.3
        f.owner = self.user
        f.public = False

        f.save()
        self.assertEqual(1, Fermentable.objects.count())

    def testHopSave(self):
        h = Hop()
        h.name = "Cascade"
        h.description = "Lychee, floral, grapefruit"
        h.country_of_origin = "USA"
        h.alpha_acid_low = 5
        h.alpha_acid_high = 9
        h.intensity = 9
        h.owner = self.user
        h.public = False

        h.save()
        self.assertEqual(1, Hop.objects.count())

    def testHopValidation(self):
        h = Hop()
        h.name = "Cascade"
        h.description = "Lychee, floral, grapefruit"
        h.country_of_origin = "USA"
        h.alpha_acid_low = 0
        h.alpha_acid_high = 9
        h.intensity = 9
        h.owner = self.user

        # Everything should be good with 0
        h.full_clean()

        # Alpha acid can only be between 0 and 100
        h.alpha_acid_low = -1
        self.assertRaises(ValidationError, h.full_clean)

        h.alpha_acid_low = 100
        h.full_clean()

        h.alpha_acid_low = 101
        self.assertRaises(ValidationError, h.full_clean)
        h.alpha_acid_low = 0

        h.alpha_acid_high = -1
        self.assertRaises(ValidationError, h.full_clean)

        h.alpha_acid_high = 100
        h.full_clean()

        h.alpha_acid_high = 101
        self.assertRaises(ValidationError, h.full_clean)
        h.alpha_acid_high = 1

        # Intensity can only be between 1 and 10
        h.intensity = 0
        self.assertRaises(ValidationError, h.full_clean)
        h.intensity = 1
        h.full_clean()
        h.intensity = 10
        h.full_clean()
        h.intensity = 11
        self.assertRaises(ValidationError, h.full_clean)

    def testYeastSave(self):
        b = Lab()
        b.name = 'Safale'
        b.save()
        b2 = Lab()
        b2.name = 'Brand 2'
        b2.save()
        style = Style(name='Light Lager', co_volume=2.5)
        style.save()

        y = Yeast(lab=b, name='S25', owner=self.user, public=False)
        y.save()
        y.style.add(style)
        self.assertEqual(1, Yeast.objects.count())

        y2 = Yeast(lab=b2, name='B25', owner=self.user, public=False)
        y2.save()
        y2.style.add(style)
        y2.alternatives.add(y)
        self.assertEqual(2, Yeast.objects.count())

    def test_fermentable_get_html(self):
        # SRM = 1
        f = Fermentable(lovibond_rating=1.2992765391997638)
        self.assertEqual('1', format(f.get_srm(), '.0f'))
        self.assertEqual('#FFE699', f.html_colour())

        # SRM = 1.2
        f = Fermentable(lovibond_rating=1.446921600472464)
        self.assertEqual('1', format(f.get_srm(), '.0f'))
        self.assertEqual('#FFE699', f.html_colour())

        # SRM = 1.7
        f = Fermentable(lovibond_rating=1.8160342536542151)
        self.assertEqual('2', format(f.get_srm(), '.0f'))
        self.assertEqual('#FFD878', f.html_colour())

        # SRM = 40
        f = Fermentable(lovibond_rating=30.090063487376344)
        self.assertEqual('40', format(f.get_srm(), '.0f'))
        self.assertEqual('#36080A', f.html_colour())

        # SRM = 45
        f = Fermentable(lovibond_rating=33.78119001919386)
        self.assertEqual('45', format(f.get_srm(), '.0f'))
        self.assertEqual('#36080A', f.html_colour())

    def test_recipehop_display_when_to_add(self):
        s = Style(name='Light', co_volume=2)
        s.save()
        b = Lab(name='Safale')
        b.save()
        y = Yeast(lab=b, name='S25', owner=self.user, public=False)
        y.save()
        recipe = AllGrainRecipe(owner=self.user, name='Test Recipe', style=s,
                        target_volume=16, original_gravity=1.085, expected_final_gravity=1.021,
                        mash_liquor=23, mash_time=60, mash_temperature=65,
                        boil_liquor=19.1, boil_time=60, fermentation_temp=17,
                        conditioning_time=4, conditioning_time_quantifier='w',
                        conditioning_temp=12, yeast=y)
        recipe.save()

        rh = RecipeHop(recipe=recipe, when_to_add=60)

        self.assertEqual('At the start of the boil', rh.display_when_to_add())

        rh.when_to_add = 50

        self.assertEqual('For the last 50 mins of the boil', rh.display_when_to_add())

        rh.when_to_add = 0

        self.assertEqual('At the end of the boil', rh.display_when_to_add())

    def test_fermentable_is_stocked(self):
        ferm = Fermentable(type=Fermentable.TYPE_CHOICES[0][0], requires_mashing=False,
                           name='Test Grain', description='Test description', lovibond_rating=1.3,
                           owner=self.user, public=False)
        ferm.save()

        self.assertFalse(ferm.is_stocked(self.user))

        ferm_stock = FermentableStock(unit=FermentableStock.UNIT_CHOICES[0][0], quantity=1,
                                      owner=self.user, ingredient=ferm)
        ferm_stock.save()

        self.assertTrue(ferm.is_stocked(self.user))
