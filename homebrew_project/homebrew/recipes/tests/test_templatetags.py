from django.http import HttpRequest
from django.test import TestCase

from recipes.templatetags import humanize, number_format, active_page


__author__ = 'sam'


class TemplateTagTest(TestCase):
    def test_humanize_boolean(self):
        self.assertEqual('Yes', humanize.boolean(True))
        self.assertEqual('No', humanize.boolean(False))

    def test_number_format_percentage(self):
        self.assertEqual('10.00%', number_format.percentage(10))
        self.assertEqual('25.50%', number_format.percentage(25.5))
        self.assertEqual('83.25%', number_format.percentage(83.254))

    def test_active_page_active_page(self):
        self.assertEqual('', active_page.active_page(None, 'some-page'))

        request = HttpRequest()
        request.path_info = '/'
        self.assertEqual('active', active_page.active_page(request, 'home'))

        self.assertEqual('', active_page.active_page(request, 'recipe-detail'))

        self.assertEqual('', active_page.active_page(request, None))
