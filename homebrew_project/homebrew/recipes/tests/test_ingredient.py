from django.core.urlresolvers import reverse
from django.test import TestCase, Client

from recipes.models import Fermentable


__author__ = 'sam'


class IngredientTestCase(TestCase):
    fixtures = ['Users', 'SampleDB']

    def setUp(self):
        self.user_name = 'test_user'
        self.user_password = 'test_password'

        self.user_two_name = 'test_two'
        self.user_two_password = 'test_password'

        self.superuser_name = 'superuser'
        self.superuser_password = 'superpassword'
        super().setUp()

    def tearDown(self):
        super().tearDown()

    def test_create_view_user_permissions(self):
        create_url = reverse('fermentable-create')

        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(create_url)
        self.assertTemplateUsed(response, 'ingredient/ingredient_create.html')
        self.assertEqual(200, response.status_code)
        self.client.logout()

        # User is not logged in; expected result: fail
        response = self.client.get(create_url)
        # redirect to login
        self.assertRedirects(response, reverse('account_login') + '?next=' + create_url)

    def test_update_view_user_permissions(self):
        fermentable = Fermentable.objects.get(pk=37)
        update_url = reverse('fermentable-update', kwargs={'slug': fermentable.slug})

        # User owns the recipe; expected result: success


        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(update_url)
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, 'ingredient/ingredient_update.html')
        self.client.logout()

        # User doesn't own the recipe; expected result: fail
        self.client.login(username=self.user_two_name, password=self.user_two_password)
        response = self.client.get(update_url)
        # Redirect to home page
        self.assertRedirects(response, reverse('home'))
        self.client.logout()

        # User is a superuser; expected result: success
        self.client.login(username=self.superuser_name, password=self.superuser_password)
        response = self.client.get(update_url)
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, 'ingredient/ingredient_update.html')
        self.client.logout()

        # User is not logged in; expected result: fail
        response = self.client.get(update_url)
        # Redirect to login page
        self.assertRedirects(response, reverse('account_login') + '?next=' + update_url)

    def test_delete_view_user_permissions(self):
        fermentable = Fermentable.objects.get(pk=37)
        delete_url = reverse('fermentable-delete', kwargs={'slug': fermentable.slug})

        # User owns the recipe; expected result: success


        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(delete_url)
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, 'ingredient/ingredient_delete.html')
        self.client.logout()

        # User doesn't own the recipe; expected result: fail
        self.client.login(username=self.user_two_name, password=self.user_two_password)
        response = self.client.get(delete_url)
        # Redirect to home page
        self.assertRedirects(response, reverse('home'))
        self.client.logout()

        # User is a superuser; expected result: success
        self.client.login(username=self.superuser_name, password=self.superuser_password)
        response = self.client.get(delete_url)
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, 'ingredient/ingredient_delete.html')
        self.client.logout()

        # User is not logged in; expected result: fail
        response = self.client.get(delete_url)
        # Redirect to login page
        self.assertRedirects(response, reverse('account_login') + '?next=' + delete_url)
