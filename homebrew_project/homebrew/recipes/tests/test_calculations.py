from django.contrib.auth.models import User
from django.test import TestCase

from recipes import calculations
from recipes.models import Fermentable


__author__ = 'sam'


class CalculationTestCase(TestCase):
    fixtures = ['Users', ]

    def setUp(self):
        self.user = User.objects.get_by_natural_key('test_user')
        self.target_size = 16
        self.target_og = 1.085

        self.pale_malt = Fermentable(type='MALTGR', name='Pale Alt Malt',
                                     extract_potential=80, owner=self.user,
                                     lovibond_rating=3.5)
        self.pale_malt.save()

        self.crystal_malt = Fermentable(type='MALTGR', name='Crystal Malt',
                                        extract_potential=76, owner=self.user,
                                        lovibond_rating=60)
        self.crystal_malt.save()

    def test_gravity_points(self):
        gravity_points = calculations.recipe_gravity_points(original_gravity=self.target_og, volume=self.target_size)
        # Only interested in the whole number
        self.assertEqual('1360', format(gravity_points, '.0f'))

    def test_fermentable_weight(self):
        pale_ale_weight = calculations.fermentable_weight(self.pale_malt, 1224, 70)
        # Only interested in the first two decimal places
        self.assertEqual('5.69', format(pale_ale_weight, '.2f'))

        crystal_weight = calculations.fermentable_weight(self.crystal_malt, 136, 70)
        self.assertEqual('0.67', format(crystal_weight, '.2f'))

    def test_fermentable_gravity_points(self):
        pale_ale_gravity_points = calculations.fermentable_gravity_points(self.pale_malt.extract_potential, 5.69, 70)
        self.assertEqual('1224', format(pale_ale_gravity_points, '.0f'))

        crystal_gravity_points = calculations.fermentable_gravity_points(self.crystal_malt.extract_potential, 0.67, 70)
        # actual value is 136.87295999999998, which rounds up to 137 when formatted.
        self.assertEqual('137', format(crystal_gravity_points, '.0f'))

    def test_malt_colour_units(self):
        pale_ale_mcu = calculations.malt_colour_units(self.pale_malt.lovibond_rating, 5.69, self.target_size)
        self.assertEqual('10.4', format(pale_ale_mcu, '.1f'))

        crystal_mcu = calculations.malt_colour_units(self.crystal_malt.lovibond_rating, 0.67, self.target_size)
        self.assertEqual('21.0', format(crystal_mcu, '.1f'))

    def test_ebc(self):
        ebc = calculations.ebc(31)
        self.assertEqual('31', format(ebc, '.0f'))

    def test_final_gravity(self):
        final_gravity = calculations.final_gravity(1.085, 75)
        self.assertEqual('1.021', format(final_gravity, '.3f'))

    def test_alcohol_by_volume(self):
        abv = calculations.alcohol_by_volume(1.085, 1.021)
        self.assertEqual('8.4', format(abv, '.1f'))

    def test_starting_volume(self):
        starting_volume = calculations.starting_volume(16, 5.69 + 0.67, 1.1, 60, 2.1, 1)
        self.assertEqual('26.1', format(starting_volume, '.1f'))

    def test_alpha_acid_units(self):
        aau = calculations.alpha_acid_units(12, 40)
        self.assertEqual(480, aau)

    def test_hop_weight(self):
        hop_weight = calculations.hop_weight_aau(480, 10)
        self.assertEqual(48, hop_weight)

        hop_weight = calculations.hop_weight(16, 40, 5, 0.169)
        self.assertEqual('76', format(hop_weight, '.0f'))

        hop_weight = calculations.hop_weight(16, 25, 4.5, 0.13)
        self.assertEqual('68', format(hop_weight, '.0f'))

        hop_weight = calculations.hop_weight(16, 15, 4.5, 0.036)
        self.assertEqual('148', format(hop_weight, '.0f'))

    def test_average_boil_gravity(self):
        abg = calculations.average_boil_gravity(1360, 26, 7, 2.1)
        self.assertEqual('1.076', format(abg, '.3f'))

    def test_alpha_acid_utilisation(self):
        aau = calculations.alpha_acid_utilisation(1.03, 120)
        self.assertEqual('0.301', format(aau, '.3f'))

        aau = calculations.alpha_acid_utilisation(1.07, 60)
        self.assertEqual('0.193', format(aau, '.3f'))

        aau = calculations.alpha_acid_utilisation(1.05, 30)
        self.assertEqual('0.177', format(aau, '.3f'))

        aau = calculations.alpha_acid_utilisation(1.04, 15)
        self.assertEqual('0.125', format(aau, '.3f'))

    def test_ibu(self):
        ibu = calculations.ibu(1.076, 60, 70, 5, 16)
        self.assertEqual('40', format(ibu, '.0f'))

        ibu = calculations.ibu(1.076, 30, 63, 4.5, 16)
        self.assertEqual('25', format(ibu, '.0f'))

        ibu = calculations.ibu(1.076, 5, 148, 4.5, 16)
        self.assertEqual('15', format(ibu, '.0f'))

    def test_original_gravity(self):
        original_gravity = calculations.original_gravity(1360, 16)
        self.assertEqual('1.085', format(original_gravity, '.3f'))

    def test_priming_sucrose(self):
        sucrose = calculations.priming_sucrose(23, 20, 2)
        self.assertEqual('104.7', format(sucrose, '.1f'))

        sucrose = calculations.priming_sucrose(23, 18, 2)
        self.assertEqual('99.8', format(sucrose, '.1f'))

        sucrose = calculations.priming_sucrose(23, 16, 2)
        self.assertEqual('94.2', format(sucrose, '.1f'))

        sucrose = calculations.priming_sucrose(23, 12, 2)
        self.assertEqual('81.2', format(sucrose, '.1f'))

        sucrose = calculations.priming_sucrose(23, 10, 2)
        self.assertEqual('73.7', format(sucrose, '.1f'))

        sucrose = calculations.priming_sucrose(23, 20, 2.5)
        self.assertEqual('150.7', format(sucrose, '.1f'))

        sucrose = calculations.priming_sucrose(23, 18, 1.7)
        self.assertEqual('72.2', format(sucrose, '.1f'))

        sucrose = calculations.priming_sucrose(23, 16, 1.5)
        self.assertEqual('48.2', format(sucrose, '.1f'))

    def test_priming_dextrose(self):
        dextrose = calculations.priming_dextrose(23, 20, 2)
        self.assertEqual('115.1', format(dextrose, '.1f'))

        dextrose = calculations.priming_dextrose(23, 18, 2)
        self.assertEqual('109.7', format(dextrose, '.1f'))

        dextrose = calculations.priming_dextrose(23, 16, 2)
        self.assertEqual('103.6', format(dextrose, '.1f'))

        dextrose = calculations.priming_dextrose(23, 12, 2)
        self.assertEqual('89.2', format(dextrose, '.1f'))

        dextrose = calculations.priming_dextrose(23, 10, 2)
        self.assertEqual('81.0', format(dextrose, '.1f'))

        dextrose = calculations.priming_dextrose(23, 20, 2.5)
        self.assertEqual('165.7', format(dextrose, '.1f'))

        dextrose = calculations.priming_dextrose(23, 18, 1.7)
        self.assertEqual('79.3', format(dextrose, '.1f'))

        dextrose = calculations.priming_dextrose(23, 16, 1.5)
        self.assertEqual('53.0', format(dextrose, '.1f'))

    def test_priming_dme(self):
        dme = calculations.priming_dme(23, 20, 2)
        self.assertEqual('154.0', format(dme, '.1f'))

        dme = calculations.priming_dme(23, 18, 2)
        self.assertEqual('146.8', format(dme, '.1f'))

        dme = calculations.priming_dme(23, 16, 2)
        self.assertEqual('138.6', format(dme, '.1f'))

        dme = calculations.priming_dme(23, 12, 2)
        self.assertEqual('119.4', format(dme, '.1f'))

        dme = calculations.priming_dme(23, 10, 2)
        self.assertEqual('108.4', format(dme, '.1f'))

        dme = calculations.priming_dme(23, 20, 2.5)
        self.assertEqual('221.7', format(dme, '.1f'))

        dme = calculations.priming_dme(23, 18, 1.7)
        self.assertEqual('106.2', format(dme, '.1f'))

        dme = calculations.priming_dme(23, 16, 1.5)
        self.assertEqual('70.9', format(dme, '.1f'))

    def test_strike_volume(self):
        strike_volume = calculations.strike_volume(4.5, 2.6)
        self.assertEqual('11.7', format(strike_volume, '.1f'))

    def test_first_runnings(self):
        first_runnings = calculations.first_runnings_volume(11.8, 4.5, 1.04)
        self.assertEqual('7.12', format(first_runnings, '.2f'))

    def test_sparge_volume(self):
        sparge_volume = calculations.sparge_volume(26.5, 6)
        self.assertEqual('20.5', format(sparge_volume, '.1f'))