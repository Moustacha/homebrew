from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test import TestCase, Client

from recipes.models import AllGrainRecipe, Fermentable, Hop, Style, Yeast, RecipeGrain, RecipeHop, BaseRecipe, \
    ExtractRecipe


__author__ = 'sam'


class RecipeTestCase(TestCase):
    fixtures = ['Users', 'SampleDB']

    def setUp(self):
        self.user_name = 'test_user'
        self.user_password = 'test_password'

        self.user_two_name = 'test_two'
        self.user_two_password = 'test_password'

        self.superuser_name = 'superuser'
        self.superuser_password = 'superpassword'
        super().setUp()

    def tearDown(self):
        super().tearDown()

    def test_get_abv(self):
        r = AllGrainRecipe()

        r.original_gravity = 1.051
        r.expected_final_gravity = 1.010
        self.assertEqual('5.37', format(r.alcohol_by_volume(), '.2f'))

        r.original_gravity = 1.072
        r.expected_final_gravity = 1.012
        self.assertEqual('7.86', format(r.alcohol_by_volume(), '.2f'))

        r.original_gravity = 1.038
        r.expected_final_gravity = 1.012
        self.assertEqual('3.41', format(r.alcohol_by_volume(), '.2f'))

    def test_update_view_user_permissions(self):
        recipe = AllGrainRecipe.objects.get(pk=19)
        update_url = reverse('recipe-update', kwargs={'slug': recipe.slug})

        # User owns the recipe; expected result: success
        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(update_url)
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, 'recipe/recipe_update_all_grain.html')
        self.client.logout()

        # User doesn't own the recipe; expected result: fail
        self.client.login(username=self.user_two_name, password=self.user_two_password)
        response = self.client.get(update_url)
        # Redirect to home page
        self.assertRedirects(response, reverse('home'))
        self.client.logout()

        # User is a superuser; expected result: success
        self.client.login(username=self.superuser_name, password=self.superuser_password)
        response = self.client.get(update_url)
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, 'recipe/recipe_update_all_grain.html')
        self.client.logout()

        # User is not logged in; expected result: fail
        response = self.client.get(update_url)
        # Redirect to login page
        self.assertRedirects(response, reverse('account_login') + '?next=' + update_url)

    def test_delete_view_user_permissions(self):
        recipe = AllGrainRecipe.objects.get(pk=19)
        delete_url = reverse('recipe-delete', kwargs={'slug': recipe.slug})

        # User owns the recipe; expected result: success
        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(delete_url)
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, 'recipe/recipe_delete.html')
        self.client.logout()

        # User doesn't own the recipe; expected result: fail
        self.client.login(username=self.user_two_name, password=self.user_two_password)
        response = self.client.get(delete_url)
        # Redirect to home page
        self.assertRedirects(response, reverse('home'))
        self.client.logout()

        # User is a superuser; expected result: success
        self.client.login(username=self.superuser_name, password=self.superuser_password)
        response = self.client.get(delete_url)
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, 'recipe/recipe_delete.html')
        self.client.logout()

        # User is not logged in; expected result: fail
        response = self.client.get(delete_url)
        # Redirect to login page
        self.assertRedirects(response, reverse('account_login') + '?next=' + delete_url)


    def test_clone_allgrainrecipe_for_batch(self):
        original_pk = 19
        recipe = AllGrainRecipe.objects.get(pk=original_pk)
        user = User.objects.get_by_natural_key(self.user_two_name)

        recipe.clone_for_batch(user)
        # Does it have a new pk
        self.assertNotEqual(original_pk, recipe.pk)

        # Make sure the ownership and public fields have been changed
        self.assertEqual(False, recipe.public)
        self.assertEqual(user, recipe.owner)

        # Get the original recipe and compare the relations (recipe grain, etc)
        orig_recipe = AllGrainRecipe.objects.get(pk=original_pk)
        self.assertNotEqual(orig_recipe.pk, recipe.pk)

        # Check the relationship to itself in cloned_from
        self.assertEqual(recipe.cloned_from.pk, orig_recipe.pk)

        orig_fermentables = list(orig_recipe.fermentables.all())
        new_fermentables = list(recipe.fermentables.all())

        self.assertEqual(len(orig_fermentables), len(new_fermentables))

        for i in range(len(orig_fermentables)):
            self.assertEqual(orig_fermentables[i].pk, new_fermentables[i].pk)

        orig_hops = list(orig_recipe.hops.all())
        new_hops = list(recipe.hops.all())

        self.assertEqual(len(orig_hops), len(new_hops))

        for i in range(len(orig_hops)):
            self.assertEqual(orig_hops[i].pk, new_hops[i].pk)

        orig_others = list(orig_recipe.others.all())
        new_others = list(recipe.others.all())

        self.assertEqual(len(orig_others), len(new_others))

        for i in range(len(orig_others)):
            self.assertEqual(orig_others[i].pk, new_others[i].pk)

        self.assertNotEqual(orig_recipe.slug, recipe.slug)

    def test_clone_baserecipe_for_batch(self):
        original_pk = 19
        recipe = BaseRecipe.objects.get(pk=original_pk)
        user = User.objects.get_by_natural_key(self.user_two_name)

        recipe.clone_for_batch(user)
        # Does it have a new pk
        self.assertNotEqual(original_pk, recipe.pk)

        # Make sure the ownership and public fields have been changed
        self.assertEqual(False, recipe.public)
        self.assertEqual(user, recipe.owner)

        # Get the original recipe and compare the relations (recipe grain, etc)
        orig_recipe = BaseRecipe.objects.get(pk=original_pk)
        self.assertNotEqual(orig_recipe.pk, recipe.pk)

        # Check the relationship to itself in cloned_from
        self.assertEqual(recipe.cloned_from.pk, orig_recipe.pk)

        orig_fermentables = list(orig_recipe.fermentables.all())
        new_fermentables = list(recipe.fermentables.all())

        self.assertEqual(len(orig_fermentables), len(new_fermentables))

        for i in range(len(orig_fermentables)):
            self.assertEqual(orig_fermentables[i].pk, new_fermentables[i].pk)

        orig_others = list(orig_recipe.others.all())
        new_others = list(recipe.others.all())

        self.assertEqual(len(orig_others), len(new_others))

        for i in range(len(orig_others)):
            self.assertEqual(orig_others[i].pk, new_others[i].pk)

    def test_clone_extractrecipe_for_batch(self):
        original_pk = 19
        recipe = ExtractRecipe.objects.get(pk=original_pk)
        user = User.objects.get_by_natural_key(self.user_two_name)

        recipe.clone_for_batch(user)
        # Does it have a new pk
        self.assertNotEqual(original_pk, recipe.pk)

        # Make sure the ownership and public fields have been changed
        self.assertEqual(False, recipe.public)
        self.assertEqual(user, recipe.owner)

        # Get the original recipe and compare the relations (recipe grain, etc)
        orig_recipe = ExtractRecipe.objects.get(pk=original_pk)
        self.assertNotEqual(orig_recipe.pk, recipe.pk)

        # Check the relationship to itself in cloned_from
        self.assertEqual(recipe.cloned_from.pk, orig_recipe.pk)

        orig_fermentables = list(orig_recipe.fermentables.all())
        new_fermentables = list(recipe.fermentables.all())

        self.assertEqual(len(orig_fermentables), len(new_fermentables))

        for i in range(len(orig_fermentables)):
            self.assertEqual(orig_fermentables[i].pk, new_fermentables[i].pk)

        orig_hops = list(orig_recipe.hops.all())
        new_hops = list(recipe.hops.all())

        self.assertEqual(len(orig_hops), len(new_hops))

        for i in range(len(orig_hops)):
            self.assertEqual(orig_hops[i].pk, new_hops[i].pk)

        orig_others = list(orig_recipe.others.all())
        new_others = list(recipe.others.all())

        self.assertEqual(len(orig_others), len(new_others))

        for i in range(len(orig_others)):
            self.assertEqual(orig_others[i].pk, new_others[i].pk)

    def create_test_recipe(self):
        owner = User.objects.get_by_natural_key(self.user_name)
        self.pale_malt = Fermentable(type='MALTGR', name='Pale Alt Malt',
                                     extract_potential=80, owner=owner,
                                     lovibond_rating=3.5)
        self.pale_malt.save()

        self.crystal_malt = Fermentable(type='MALTGR', name='Crystal Malt',
                                        extract_potential=76, owner=owner,
                                        lovibond_rating=60)
        self.crystal_malt.save()

        self.fuggle = Hop(name='Fuggle', country_of_origin='UK',
                          alpha_acid_low=4, alpha_acid_high=7,
                          intensity=6, owner=owner)
        self.fuggle.save()

        self.ek_golding = Hop(name='East Kent Golding', country_of_origin='UK',
                              alpha_acid_low=5, alpha_acid_high=8,
                              intensity=6, owner=owner)
        self.ek_golding.save()

        self.recipe = AllGrainRecipe(owner=owner, name='Test Recipe', style=Style.objects.get(pk=1),
                                     target_volume=16, original_gravity=1.085, expected_final_gravity=1.021,
                                     mash_liquor=13.4, sparge_volume=12.7, mash_thickness=2.1,
                                     mash_time=60, mash_temperature=65,
                                     boil_liquor=19.1, boil_time=60, fermentation_temp=17,
                                     conditioning_time=4, conditioning_time_quantifier='w',
                                     conditioning_temp=12, yeast=Yeast.objects.get(pk=64))
        self.recipe.save()
        # Get the recipe from the DB so that all the fields are the right types, e.g. Decimal instead of float
        self.recipe = AllGrainRecipe.objects.get(pk=self.recipe.pk)

        # Add the fermentables
        rg = RecipeGrain(recipe=self.recipe, fermentable=self.pale_malt, quantity=5.69, unit='kg')
        rg.save()
        rg = RecipeGrain(recipe=self.recipe, fermentable=self.crystal_malt, quantity=670, unit='g')
        rg.save()

        # Add the hops
        rh = RecipeHop(recipe=self.recipe, hop=self.fuggle, alpha_acid_rating=5, quantity=76, when_to_add=60, ibu=43.6)
        rh.save()
        rh = RecipeHop(recipe=self.recipe, hop=self.ek_golding, alpha_acid_rating=4.5, quantity=68, when_to_add=30,
                       ibu=27)
        rh.save()
        rh = RecipeHop(recipe=self.recipe, hop=self.ek_golding, alpha_acid_rating=4.5, quantity=148, when_to_add=5,
                       ibu=15.2)
        rh.save()

    def test_starting_volume(self):
        self.create_test_recipe()
        starting_volume = self.recipe.starting_volume()
        self.assertEqual('26.1', format(starting_volume, '.1f'))

    def test_target_gravity_points(self):
        self.create_test_recipe()
        self.recipe.original_gravity = 1.085
        target_gravity_points = self.recipe.get_target_gravity_points()
        self.assertEqual('1360', format(target_gravity_points, '.0f'))

    def test_get_gravity_points(self):
        self.create_test_recipe()
        target_gravity_points = self.recipe.get_gravity_points()
        self.assertEqual('1360', format(target_gravity_points, '.0f'))

    def test_calculate_mash_liquor(self):
        self.create_test_recipe()
        mash_liquor = self.recipe.calculate_mash_liquor()
        self.assertEqual(format(self.recipe.mash_liquor, '.1f'), format(mash_liquor, '.1f'))
        # self.assertEqual(self.recipe.mash_liquor, mash_liquor)

    def test_calculate_sparge_volume(self):
        self.create_test_recipe()
        sparge_volume = self.recipe.calculate_sparge_volume()
        self.assertEqual(str(self.recipe.sparge_volume), format(sparge_volume, '.2f'))
        # self.assertEqual(self.recipe.sparge_volume, sparge_volume)

    def test_calculate_boil_liquor(self):
        self.create_test_recipe()
        boil_liquor = self.recipe.calculate_boil_liquor()
        format_string = '.1f'
        self.assertEqual(format(self.recipe.boil_liquor, format_string), format(boil_liquor, format_string))

    def test_calculate_final_gravity(self):
        self.create_test_recipe()
        final_gravity = self.recipe.calculate_final_gravity()

        format_string = '.3f'
        self.assertEqual(format(self.recipe.expected_final_gravity, format_string),
                         format(final_gravity, format_string))

    def test_recipehop_calculate_ibu(self):
        self.create_test_recipe()

        format_string = '.0f'

        for rh in self.recipe.recipehop_set.all():
            calculated_ibu = rh.calculate_ibu()
            self.assertEqual(format(rh.ibu, format_string), format(calculated_ibu, format_string))

    def test_recipehop_calculate_quantity(self):
        self.create_test_recipe()

        format_string = '.0f'

        for rh in self.recipe.recipehop_set.all():
            calculated_quantity = rh.calculate_quantity()
            self.assertEqual(format(rh.quantity, format_string), format(calculated_quantity, format_string))

    def test_average_boil_gravity(self):
        self.create_test_recipe()
        average_boil_gravity = self.recipe.average_boil_gravity()

        self.assertEqual('1.075', format(average_boil_gravity, '.3f'))

    def test_recipehop_calculate_fields(self):
        owner = User.objects.get_by_natural_key(self.user_name)

        allgrain_recipe = AllGrainRecipe(owner=owner, name='Test Recipe', style=Style.objects.get(pk=1),
                             target_volume=23, original_gravity=1.079, expected_final_gravity=1.022,
                             mash_liquor=16.254, sparge_volume=19.26, mash_time=60, mash_temperature=65,
                             boil_liquor=27, boil_time=75, fermentation_temp=12,
                             conditioning_time=6, conditioning_time_quantifier='w',
                             conditioning_temp=3, yeast=Yeast.objects.get(pk=64))
        allgrain_recipe.save()

        pilsner_malt = Fermentable(type='MALTGR', name='Pilsner',
                                     extract_potential=80.5, owner=owner,
                                     lovibond_rating=1.2)
        pilsner_malt.save()
        carapils_malt = Fermentable(type='MALTGR', name='Carapils',
                                     extract_potential=74, owner=owner,
                                     lovibond_rating=2.44)
        carapils_malt.save()
        biscuit_malt = Fermentable(type='MALTGR', name='Biscuit',
                                     extract_potential=79, owner=owner,
                                     lovibond_rating=19.31)
        biscuit_malt.save()
        lme = Fermentable(type='MALTEX', name='Light Malt Extract',
                                     extract_potential=95, owner=owner,
                                     lovibond_rating=4.3)
        lme.save()

        hallertauer = Hop(name='Hallertauer', country_of_origin='Germany',
                          alpha_acid_low=4, alpha_acid_high=7,
                          intensity=6, owner=owner)
        hallertauer.save()


        rg = RecipeGrain(recipe=allgrain_recipe, fermentable=carapils_malt, quantity=290, unit='g', steeped=True)
        rg.save()
        rg = RecipeGrain(recipe=allgrain_recipe, fermentable=biscuit_malt, quantity=200, unit='g', steeped=True)
        rg.save()
        rg = RecipeGrain(recipe=allgrain_recipe, fermentable=pilsner_malt, quantity=7.25, unit='kg')
        rg.save()

        # Add the hops
        rh = RecipeHop(recipe=allgrain_recipe, hop=hallertauer, alpha_acid_rating=5, quantity=110, when_to_add=60, ibu=48)
        rh.save()
        rh = RecipeHop(recipe=allgrain_recipe, hop=hallertauer, alpha_acid_rating=5, quantity=73, when_to_add=10,
                       ibu=12)
        rh.save()
        # Can't calculate the weight when the IBU is 0
        # rh = RecipeHop(recipe=allgrain_recipe, hop=hallertauer, alpha_acid_rating=5, quantity=110, when_to_add=0,
        #                ibu=0)
        # rh.save()

        recipehops = allgrain_recipe.recipehop_set.all()

        # Store what the expected values are, as they have already been set in create_test_recipe
        expected_ibus = [format(x.ibu, '.0f') for x in recipehops]
        expected_quantity = [format(x.quantity, '.0f') for x in recipehops]

        # Reset the ibu for each hop addition
        for rh in recipehops:
            rh.ibu = 0
            # Calculate the ibu field for each hop addition
            rh.calculate_fields()

        calculated_ibus = [format(x.ibu, '.0f') for x in recipehops]

        self.assertListEqual(expected_ibus, calculated_ibus)

        for rh in recipehops:
            rh.quantity = 0
            rh.calculate_fields()

        calculated_quantity = [format(x.quantity, '.0f') for x in recipehops]

        self.assertListEqual(expected_quantity, calculated_quantity)

        for rh in recipehops:
            rh.calculate_fields(only_empty=False)

        calculated_ibus = [format(x.ibu, '.0f') for x in recipehops]
        calculated_quantity = [format(x.quantity, '.0f') for x in recipehops]
        self.assertListEqual(expected_ibus, calculated_ibus)
        self.assertListEqual(expected_quantity, calculated_quantity)


        ############################################
        # Calculate on a recipe that is from extract

        extract_recipe = ExtractRecipe(owner=owner, name='Test Recipe', style=Style.objects.get(pk=1),
                             target_volume=23, original_gravity=1.079, expected_final_gravity=1.022,
                             steep_liquor=27, steep_time=30, steep_temperature=65,
                             boil_liquor=27, boil_time=75, fermentation_temp=12,
                             conditioning_time=6, conditioning_time_quantifier='w',
                             conditioning_temp=3, yeast=Yeast.objects.get(pk=64))
        extract_recipe.save()

        rg = RecipeGrain(recipe=extract_recipe, fermentable=carapils_malt, quantity=290, unit='g', steeped=True)
        rg.save()
        rg = RecipeGrain(recipe=extract_recipe, fermentable=biscuit_malt, quantity=200, unit='g', steeped=True)
        rg.save()
        rg = RecipeGrain(recipe=extract_recipe, fermentable=lme, quantity=4.6, unit='kg')
        rg.save()

        # Add the hops
        # Half of the hops for the extract recipe. Seems the book i'm using has bad values for something somewhere
        rh = RecipeHop(recipe=extract_recipe, hop=hallertauer, alpha_acid_rating=5, quantity=57, when_to_add=60, ibu=25.4)
        rh.save()
        rh = RecipeHop(recipe=extract_recipe, hop=hallertauer, alpha_acid_rating=5, quantity=36, when_to_add=10,
                       ibu=5.9)
        rh.save()
        # Can't calculate the weight when the IBU is 0
        # rh = RecipeHop(recipe=extract_recipe, hop=hallertauer, alpha_acid_rating=5, quantity=110, when_to_add=0,
        #                ibu=0)
        rh.save()

        recipehops = extract_recipe.recipehop_set.all()

        # Store what the expected values are, as they have already been set in create_test_recipe
        expected_ibus = [format(x.ibu, '.0f') for x in recipehops]
        expected_quantity = [format(x.quantity, '.0f') for x in recipehops]

        # Reset the ibu for each hop addition
        for rh in recipehops:
            rh.ibu = 0
            # Calculate the ibu field for each hop addition
            rh.calculate_fields()

        calculated_ibus = [format(x.ibu, '.0f') for x in recipehops]

        self.assertListEqual(expected_ibus, calculated_ibus)

        for rh in recipehops:
            rh.quantity = 0
            rh.calculate_fields()

        calculated_quantity = [format(x.quantity, '.0f') for x in recipehops]

        self.assertListEqual(expected_quantity, calculated_quantity)

        for rh in recipehops:
            rh.calculate_fields(only_empty=False)

        calculated_ibus = [format(x.ibu, '.0f') for x in recipehops]
        calculated_quantity = [format(x.quantity, '.0f') for x in recipehops]
        self.assertListEqual(expected_ibus, calculated_ibus)
        self.assertListEqual(expected_quantity, calculated_quantity)


    def test_recipe_calculate_fields(self):
        self.create_test_recipe()

        original_gravity = self.recipe.original_gravity
        final_gravity = self.recipe.expected_final_gravity
        mash_liquor = self.recipe.mash_liquor
        sparge_volume = self.recipe.sparge_volume
        boil_liquor = self.recipe.boil_liquor

        self.recipe.calculate_fields(only_empty=True)

        # None of the fields should change value
        self.assertEqual(original_gravity, self.recipe.original_gravity)
        self.assertEqual(final_gravity, self.recipe.expected_final_gravity)
        self.assertEqual(mash_liquor, self.recipe.mash_liquor)
        self.assertEqual(boil_liquor, self.recipe.boil_liquor)

        # Force the boil liquor to be calculated to something different.
        self.recipe.boil_time = 70

        self.recipe.calculate_fields(only_empty=False)
        # The fields should be different
        self.assertNotEqual(original_gravity, self.recipe.original_gravity)
        self.assertNotEqual(final_gravity, self.recipe.expected_final_gravity)
        self.assertNotEqual(sparge_volume, self.recipe.sparge_volume)
        self.assertNotEqual(boil_liquor, self.recipe.boil_liquor)

    def test_recipe_get_ebc(self):
        self.create_test_recipe()
        ebc = self.recipe.get_ebc()
        self.assertEqual('32', format(ebc, '.0f'))

    def test_recipe_get_srm(self):
        self.create_test_recipe()

        self.assertEqual('16', format(self.recipe.get_srm(), '.0f'))

    def test_recipe_html_colour(self):
        self.create_test_recipe()
        self.assertEqual('#B54C00', self.recipe.html_colour())

    def test_recipe_issue_16_custom_recipe_in_search(self):
        self.create_test_recipe()
        user = User.objects.get_by_natural_key(username=self.user_name)

        # Make sure the recipe can be retrieved normally
        r = BaseRecipe.objects.public_or_owner(user).get(pk=self.recipe.pk)
        self.assertIsNotNone(r)

        # Set is as a clone of itself
        r.cloned_from = r
        r.save()

        r2 = BaseRecipe.objects.public_or_owner(user).get(pk=self.recipe.pk)
        self.assertIsNotNone(r2, 'Cloned recipe could not be found')
