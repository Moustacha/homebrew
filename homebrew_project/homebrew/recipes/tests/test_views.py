from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test import TestCase, Client

from recipes.models import AllGrainRecipe, Fermentable, Hop, ExtractRecipe, BaseRecipe, Style


__author__ = 'sam'


class ViewTestCase(TestCase):
    fixtures = ['Users', 'SampleDB']

    def setUp(self):
        self.user_name = 'test_user'
        self.user_password = 'test_password'

        self.user_two_name = 'test_two'
        self.user_two_password = 'test_password'

        self.superuser_name = 'superuser'
        self.superuser_password = 'superpassword'
        super().setUp()

    def tearDown(self):
        super().tearDown()

    def assertResponseOk(self, response, msg=None):
        self.assertEqual(200, response.status_code, msg)

    def test_recipe_list_view(self):
        recipe_list_url = reverse('recipe-list')
        response = self.client.get(recipe_list_url)
        self.assertResponseOk(response)
        context = response.context_data

        # There should be an object called filter that is the form filter
        self.assertIsNotNone(context['filter'])
        # The table should exist too
        self.assertIsNotNone(context['table'])

        # Perform a successful search
        response = self.client.get(recipe_list_url, data={'name': 'mex'})
        self.assertResponseOk(response)
        context = response.context_data
        self.assertIsNotNone(context['table'])
        self.assertEqual(1, len(context['table'].data))

        # Perform an emtpy search
        response = self.client.get(recipe_list_url, data={'name': 'asdf'})
        self.assertResponseOk(response)
        context = response.context_data
        self.assertIsNotNone(context['table'])
        self.assertEqual(0, len(context['table'].data))

        # Perform a search on the style
        response = self.client.get(recipe_list_url, data={'style': 1})
        self.assertResponseOk(response)
        context = response.context_data
        self.assertIsNotNone(context['table'])
        self.assertEqual(4, len(context['table'].data))

        # Make sure the key 'last_page' is set in the session
        session = response.wsgi_request.session
        self.assertIsNotNone(session)
        self.assertIn('list_page', session)
        self.assertEqual(recipe_list_url, session['list_page'])

    def test_recipe_search_view(self):
        recipe_search_url = reverse('recipe-search')
        response = self.client.get(recipe_search_url)
        self.assertResponseOk(response)
        context = response.context_data

        # There should be an object called filter that is the form filter
        self.assertIsNotNone(context['filter'])
        # The table should exist too
        self.assertIsNotNone(context['table'])

        # Search for recipes that have any of the matches
        # So searching for recipes that have pilsner - should match both recipes
        response = self.client.get(recipe_search_url, data={'fermentables_0': 37, 'fermentables_1': 'any'})
        self.assertResponseOk(response)
        context = response.context_data
        self.assertIsNotNone(context['table'])
        self.assertEqual(2, len(context['table'].data))

        # Search for recipes that have pilsner OR carapils
        response = self.client.get(recipe_search_url,
                                   data={'fermentables_0': 20, 'fermentables_0': 37, 'fermentables_1': 'any'})
        self.assertResponseOk(response)
        context = response.context_data
        self.assertIsNotNone(context['table'])
        self.assertEqual(2, len(context['table'].data))

        # Search for recipes that have pilsner AND carapils
        response = self.client.get(recipe_search_url,
                                   data={'fermentables_0': 37, 'fermentables_0': 20, 'fermentables_1': 'all'})
        self.assertResponseOk(response)
        context = response.context_data
        self.assertIsNotNone(context['table'])
        self.assertEqual(1, len(context['table'].data))

    def test_ingredient_list_view(self):
        # Fermentable
        response = self.client.get(reverse('fermentable-list'))
        self.assertResponseOk(response)

        context = response.context_data

        self.assertIn('title', context.keys())
        self.assertIn('create_url', context.keys())

        self.assertEqual(reverse('fermentable-create'), context['create_url'])

        # Hop
        response = self.client.get(reverse('hop-list'))
        self.assertResponseOk(response)

        context = response.context_data

        self.assertIn('title', context.keys())
        self.assertIn('create_url', context.keys())

        self.assertEqual(reverse('hop-create'), context['create_url'])

        # Other
        response = self.client.get(reverse('other-list'))
        self.assertResponseOk(response)

        context = response.context_data

        self.assertIn('title', context.keys())
        self.assertIn('create_url', context.keys())

        self.assertEqual(reverse('other-create'), context['create_url'])

        # Yeast
        response = self.client.get(reverse('yeast-list'))
        self.assertResponseOk(response)

        context = response.context_data

        self.assertIn('title', context.keys())
        self.assertIn('create_url', context.keys())

        self.assertEqual(reverse('yeast-create'), context['create_url'])

    def test_recipe_profile_view(self):
        # Try as an anonymous user
        response = self.client.get(reverse('account_recipe_profile'))
        self.assertRedirects(response, reverse('account_login') + '?next=' + reverse('account_recipe_profile'))

        # Login as a user
        self.client.login(username=self.user_name, password=self.user_password)

        response = self.client.get(reverse('account_recipe_profile'))
        self.assertResponseOk(response)

    def test_recipe_detail_view(self):
        """
        Test that BaseRecipe, ExtractRecipe and AllGrainRecipe get_absolute_url works
        :return:
        """
        recipes = [AllGrainRecipe.objects.get(pk=19), ExtractRecipe.objects.get(pk=21), BaseRecipe.objects.get(pk=22)]

        for r in recipes:
            self.assertResponseOk(self.client.get(r.get_absolute_url()), '{}'.format(r.__class__))

    def test_recipe_update_view(self):
        """
        Test that BaseRecipe, ExtractRecipe and AllGrainRecipe get_update_url works
        :return:
        """

        recipes = [AllGrainRecipe.objects.get(pk=19), ExtractRecipe.objects.get(pk=21), BaseRecipe.objects.get(pk=22)]

        for r in recipes:
            response = self.client.get(r.get_update_url())
            self.assertRedirects(response, reverse('account_login')+'?next='+r.get_update_url())

        self.client.login(username=self.user_name, password=self.user_password)
        for r in recipes:
            self.assertResponseOk(self.client.get(r.get_update_url()), '{}'.format(r.__class__))

    def test_recipe_delete_view(self):
        """
        Test that BaseRecipe, ExtractRecipe and AllGrainRecipe get_delete_url works
        :return:
        """
        recipes = [AllGrainRecipe.objects.get(pk=19), ExtractRecipe.objects.get(pk=21), BaseRecipe.objects.get(pk=22)]

        for r in recipes:
            response = self.client.get(r.get_delete_url())
            self.assertRedirects(response, reverse('account_login')+'?next='+r.get_delete_url())

        self.client.login(username=self.user_name, password=self.user_password)
        for r in recipes:
            self.assertResponseOk(self.client.get(r.get_delete_url()), '{}'.format(r.__class__))

    def test_create_basic_recipe(self):
        create_url = reverse('recipe-create-basic')
        # Test that you need to be logged in first
        response = self.client.get(create_url)
        self.assertRedirects(response, reverse('account_login')+'?next='+create_url)

        # Login
        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(create_url)
        self.assertResponseOk(response)
        # Create recipe
        post_data = {
            'name': 'test base',
            'public': 'on',
            'style': '1',
            'target_volume': '23',
            'description': '',
            'original_gravity': '1.081',
            'expected_final_gravity': '1.005',
            'owner': '1',
            'recipegrain_set-TOTAL_FORMS': '2',
            'recipegrain_set-INITIAL_FORMS': '0',
            'recipegrain_set-MIN_NUM_FORMS': '0',
            'recipegrain_set-MAX_NUM_FORMS': '1000',
            'recipegrain_set-0-recipe': '',
            'recipegrain_set-0-fermentable': '37',
            'recipegrain_set-0-quantity': '4',
            'recipegrain_set-0-unit': 'kg',
            'recipegrain_set-0-id': '',
            'recipegrain_set-0-DELETE': '',
            'recipegrain_set-1-recipe': '',
            'recipegrain_set-1-fermentable': '',
            'recipegrain_set-1-quantity': '',
            'recipegrain_set-1-unit': '',
            'recipegrain_set-1-id': '',
            'recipegrain_set-1-DELETE': '',
            'recipeother_set-TOTAL_FORMS': '2',
            'recipeother_set-INITIAL_FORMS': '0',
            'recipeother_set-MIN_NUM_FORMS': '0',
            'recipeother_set-MAX_NUM_FORMS': '1000',
            'recipeother_set-0-recipe': '',
            'recipeother_set-0-other': '1',
            'recipeother_set-0-quantity': '1',
            'recipeother_set-0-unit': 'tsp',
            'recipeother_set-0-when_to_add': '15 minutes from end',
            'recipeother_set-0-id': '',
            'recipeother_set-0-DELETE': '',
            'recipeother_set-1-recipe': '',
            'recipeother_set-1-other': '',
            'recipeother_set-1-quantity': '',
            'recipeother_set-1-unit': '',
            'recipeother_set-1-when_to_add': '',
            'recipeother_set-1-id': '',
            'recipeother_set-1-DELETE': '',
            'yeast': '64',
            'fermentation_temp': '18',
            'conditioning_time': '2',
            'conditioning_time_quantifier': 'w',
            'conditioning_temp': '12',
            'submit': 'Save'
        }
        response = self.client.post(create_url, data=post_data)
        self.assertTemplateNotUsed(response, 'recipe/recipe_create.html', 'Recipe wasn''t created successfully')

        # Make sure everything is there and correct...
        # Recipe
        recipe = BaseRecipe.objects.all().order_by('-id').first()
        self.assertEqual('test base', recipe.name)

        self.assertRedirects(response, recipe.get_absolute_url())

        self.assertEqual(True, recipe.public)
        self.assertEqual(1, recipe.style.pk)
        self.assertEqual(23, recipe.target_volume)
        self.assertEqual('1.081', str(recipe.original_gravity))
        self.assertEqual('1.005', str(recipe.expected_final_gravity))
        self.assertEqual(64, recipe.yeast.pk)
        self.assertEqual(18, recipe.fermentation_temp)
        self.assertEqual(2, recipe.conditioning_time)
        self.assertEqual('w', recipe.conditioning_time_quantifier)
        self.assertEqual(12, recipe.conditioning_temp)

        # Fermentables
        fermentables = recipe.recipegrain_set.all()
        self.assertEqual(1, len(fermentables))
        rg = fermentables[0]
        self.assertEqual(37, rg.fermentable.pk)
        self.assertEqual(4, rg.quantity)
        self.assertEqual('kg', rg.unit)

        # Others
        others = recipe.recipeother_set.all()
        self.assertEqual(1, len(others))
        ro = others[0]
        self.assertEqual(1, ro.other.pk)
        self.assertEqual(1, ro.quantity)
        self.assertEqual('tsp', ro.unit)

    def test_create_extract_recipe(self):
        create_url = reverse('recipe-create-extract')
        # Test that you need to be logged in first
        response = self.client.get(create_url)
        self.assertRedirects(response, reverse('account_login')+'?next='+create_url)

        # Login
        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(create_url)
        self.assertResponseOk(response)
        # Create recipe
        post_data = {
            'name': 'test extract',
            'public': 'on',
            'style': '1',
            'target_volume': '23',
            'description': '',
            'original_gravity': '1.081',
            'expected_final_gravity': '1.005',
            'owner': '1',
            'steep_liquor': '0',
            'steep_time': '',
            'steep_temperature': '',
            'recipegrain_set-TOTAL_FORMS': '2',
            'recipegrain_set-INITIAL_FORMS': '0',
            'recipegrain_set-MIN_NUM_FORMS': '0',
            'recipegrain_set-MAX_NUM_FORMS': '1000',
            'recipegrain_set-0-recipe': '',
            'recipegrain_set-0-fermentable': '37',
            'recipegrain_set-0-quantity': '4',
            'recipegrain_set-0-unit': 'kg',
            'recipegrain_set-0-id': '',
            'recipegrain_set-0-DELETE': '',
            'recipegrain_set-1-recipe': '',
            'recipegrain_set-1-fermentable': '',
            'recipegrain_set-1-quantity': '',
            'recipegrain_set-1-unit': '',
            'recipegrain_set-1-id': '',
            'recipegrain_set-1-DELETE': '',
            'boil_liquor': '27',
            'boil_time': '65',
            'recipehop_set-TOTAL_FORMS': '2',
            'recipehop_set-INITIAL_FORMS': '0',
            'recipehop_set-MIN_NUM_FORMS': '0',
            'recipehop_set-MAX_NUM_FORMS': '1000',
            'recipehop_set-0-recipe': '',
            'recipehop_set-0-hop': '35',
            'recipehop_set-0-alpha_acid_rating': '8',
            'recipehop_set-0-quantity': '0',
            'recipehop_set-0-ibu': '50',
            'recipehop_set-0-when_to_add': '65',
            'recipehop_set-0-id': '',
            'recipehop_set-0-DELETE': '',
            'recipehop_set-1-recipe': '',
            'recipehop_set-1-hop': '',
            'recipehop_set-1-alpha_acid_rating': '',
            'recipehop_set-1-quantity': '0',
            'recipehop_set-1-ibu': '0',
            'recipehop_set-1-when_to_add': '',
            'recipehop_set-1-id': '',
            'recipehop_set-1-DELETE': '',
            'recipeother_set-TOTAL_FORMS': '2',
            'recipeother_set-INITIAL_FORMS': '0',
            'recipeother_set-MIN_NUM_FORMS': '0',
            'recipeother_set-MAX_NUM_FORMS': '1000',
            'recipeother_set-0-recipe': '',
            'recipeother_set-0-other': '1',
            'recipeother_set-0-quantity': '1',
            'recipeother_set-0-unit': 'tsp',
            'recipeother_set-0-when_to_add': '15 minutes from end',
            'recipeother_set-0-id': '',
            'recipeother_set-0-DELETE': '',
            'recipeother_set-1-recipe': '',
            'recipeother_set-1-other': '',
            'recipeother_set-1-quantity': '',
            'recipeother_set-1-unit': '',
            'recipeother_set-1-when_to_add': '',
            'recipeother_set-1-id': '',
            'recipeother_set-1-DELETE': '',
            'yeast': '64',
            'fermentation_temp': '18',
            'conditioning_time': '2',
            'conditioning_time_quantifier': 'w',
            'conditioning_temp': '12',
            'submit': 'Save'
        }
        response = self.client.post(create_url, data=post_data)
        self.assertTemplateNotUsed(response, 'recipe/recipe_create.html', 'Recipe wasn''t created successfully')

        # Make sure everything is there and correct...
        # Recipe
        recipe = ExtractRecipe.objects.all().order_by('-id').first()
        self.assertEqual('test extract', recipe.name)

        self.assertRedirects(response, recipe.get_absolute_url())

        self.assertEqual(True, recipe.public)
        self.assertEqual(1, recipe.style.pk)
        self.assertEqual(23, recipe.target_volume)
        self.assertEqual('1.081', str(recipe.original_gravity))
        self.assertEqual('1.005', str(recipe.expected_final_gravity))
        self.assertEqual(0, recipe.steep_liquor)
        self.assertEqual(27, recipe.boil_liquor)
        self.assertEqual(65, recipe.boil_time)
        self.assertEqual(64, recipe.yeast.pk)
        self.assertEqual(18, recipe.fermentation_temp)
        self.assertEqual(2, recipe.conditioning_time)
        self.assertEqual('w', recipe.conditioning_time_quantifier)
        self.assertEqual(12, recipe.conditioning_temp)

        # Fermentables
        fermentables = recipe.recipegrain_set.all()
        self.assertEqual(1, len(fermentables))
        rg = fermentables[0]
        self.assertEqual(37, rg.fermentable.pk)
        self.assertEqual(4, rg.quantity)
        self.assertEqual('kg', rg.unit)


        # Hops
        hops = recipe.recipehop_set.all()
        self.assertEqual(1, len(hops))
        rh = hops[0]
        self.assertEqual(35, rh.hop.pk)
        self.assertEqual(8, rh.alpha_acid_rating)
        self.assertNotEqual(0, rh.quantity)
        self.assertEqual(50, rh.ibu)
        self.assertEqual(65, rh.when_to_add)

        # Others
        others = recipe.recipeother_set.all()
        self.assertEqual(1, len(others))
        ro = others[0]
        self.assertEqual(1, ro.other.pk)
        self.assertEqual(1, ro.quantity)
        self.assertEqual('tsp', ro.unit)

    def test_create_all_grain_recipe(self):
        create_url = reverse('recipe-create-all_grain')
        # Test that you need to be logged in first
        response = self.client.get(create_url)
        self.assertRedirects(response, reverse('account_login')+'?next='+create_url)

        # Login
        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(create_url)
        self.assertResponseOk(response)
        # Create recipe
        post_data = {
            'name': 'test all grain',
            'public': 'on',
            'style': '1',
            'target_volume': '23',
            'description': '',
            'original_gravity': '1.081',
            'expected_final_gravity': '1.005',
            'owner': '1',
            'mash_liquor': '17',
            'mash_thickness': '4.25',
            'sparge_volume': '0',
            'mash_time': '90',
            'mash_temperature': '65',
            'recipegrain_set-TOTAL_FORMS': '2',
            'recipegrain_set-INITIAL_FORMS': '0',
            'recipegrain_set-MIN_NUM_FORMS': '0',
            'recipegrain_set-MAX_NUM_FORMS': '1000',
            'recipegrain_set-0-recipe': '',
            'recipegrain_set-0-fermentable': '37',
            'recipegrain_set-0-quantity': '4',
            'recipegrain_set-0-unit': 'kg',
            'recipegrain_set-0-id': '',
            'recipegrain_set-0-DELETE': '',
            'recipegrain_set-1-recipe': '',
            'recipegrain_set-1-fermentable': '',
            'recipegrain_set-1-quantity': '',
            'recipegrain_set-1-unit': '',
            'recipegrain_set-1-id': '',
            'recipegrain_set-1-DELETE': '',
            'boil_liquor': '27',
            'boil_time': '65',
            'recipehop_set-TOTAL_FORMS': '2',
            'recipehop_set-INITIAL_FORMS': '0',
            'recipehop_set-MIN_NUM_FORMS': '0',
            'recipehop_set-MAX_NUM_FORMS': '1000',
            'recipehop_set-0-recipe': '',
            'recipehop_set-0-hop': '35',
            'recipehop_set-0-alpha_acid_rating': '8',
            'recipehop_set-0-quantity': '0',
            'recipehop_set-0-ibu': '50',
            'recipehop_set-0-when_to_add': '65',
            'recipehop_set-0-id': '',
            'recipehop_set-0-DELETE': '',
            'recipehop_set-1-recipe': '',
            'recipehop_set-1-hop': '',
            'recipehop_set-1-alpha_acid_rating': '',
            'recipehop_set-1-quantity': '0',
            'recipehop_set-1-ibu': '0',
            'recipehop_set-1-when_to_add': '',
            'recipehop_set-1-id': '',
            'recipehop_set-1-DELETE': '',
            'recipeother_set-TOTAL_FORMS': '2',
            'recipeother_set-INITIAL_FORMS': '0',
            'recipeother_set-MIN_NUM_FORMS': '0',
            'recipeother_set-MAX_NUM_FORMS': '1000',
            'recipeother_set-0-recipe': '',
            'recipeother_set-0-other': '1',
            'recipeother_set-0-quantity': '1',
            'recipeother_set-0-unit': 'tsp',
            'recipeother_set-0-when_to_add': '15 minutes from end',
            'recipeother_set-0-id': '',
            'recipeother_set-0-DELETE': '',
            'recipeother_set-1-recipe': '',
            'recipeother_set-1-other': '',
            'recipeother_set-1-quantity': '',
            'recipeother_set-1-unit': '',
            'recipeother_set-1-when_to_add': '',
            'recipeother_set-1-id': '',
            'recipeother_set-1-DELETE': '',
            'yeast': '64',
            'fermentation_temp': '18',
            'conditioning_time': '2',
            'conditioning_time_quantifier': 'w',
            'conditioning_temp': '12',
            'submit': 'Save'
        }
        response = self.client.post(create_url, data=post_data)
        self.assertTemplateNotUsed(response, 'recipe/recipe_create.html', 'Recipe wasn''t created successfully')

        # Make sure everything is there and correct...
        # Recipe
        recipe = AllGrainRecipe.objects.all().order_by('-id').first()
        self.assertEqual('test all grain', recipe.name)

        self.assertRedirects(response, recipe.get_absolute_url())

        self.assertEqual(True, recipe.public)
        self.assertEqual(1, recipe.style.pk)
        self.assertEqual(23, recipe.target_volume)
        self.assertEqual('1.081', str(recipe.original_gravity))
        self.assertEqual('1.005', str(recipe.expected_final_gravity))
        self.assertEqual(17, recipe.mash_liquor)
        self.assertEqual(4.25, recipe.mash_thickness)
        self.assertEqual(90, recipe.mash_time)
        self.assertEqual(65, recipe.mash_temperature)
        self.assertEqual(27, recipe.boil_liquor)
        self.assertEqual(65, recipe.boil_time)
        self.assertEqual(64, recipe.yeast.pk)
        self.assertEqual(18, recipe.fermentation_temp)
        self.assertEqual(2, recipe.conditioning_time)
        self.assertEqual('w', recipe.conditioning_time_quantifier)
        self.assertEqual(12, recipe.conditioning_temp)

        # Fermentables
        fermentables = recipe.recipegrain_set.all()
        self.assertEqual(1, len(fermentables))
        rg = fermentables[0]
        self.assertEqual(37, rg.fermentable.pk)
        self.assertEqual(4, rg.quantity)
        self.assertEqual('kg', rg.unit)


        # Hops
        hops = recipe.recipehop_set.all()
        self.assertEqual(1, len(hops))
        rh = hops[0]
        self.assertEqual(35, rh.hop.pk)
        self.assertEqual(8, rh.alpha_acid_rating)
        self.assertNotEqual(0, rh.quantity)
        self.assertEqual(50, rh.ibu)
        self.assertEqual(65, rh.when_to_add)

        # Others
        others = recipe.recipeother_set.all()
        self.assertEqual(1, len(others))
        ro = others[0]
        self.assertEqual(1, ro.other.pk)
        self.assertEqual(1, ro.quantity)
        self.assertEqual('tsp', ro.unit)

        # Encountered an error when the IBU is None. It tries to save with NULL instead of 0
        post_data = {
            'name': 'test all grain second',
            'public': 'on',
            'style': '1',
            'target_volume': '23',
            'description': '',
            'original_gravity': '1.081',
            'expected_final_gravity': '1.005',
            'owner': '1',
            'mash_liquor': '17',
            'mash_thickness': '4.25',
            'sparge_volume': '0',
            'mash_time': '90',
            'mash_temperature': '65',
            'recipegrain_set-TOTAL_FORMS': '2',
            'recipegrain_set-INITIAL_FORMS': '0',
            'recipegrain_set-MIN_NUM_FORMS': '0',
            'recipegrain_set-MAX_NUM_FORMS': '1000',
            'recipegrain_set-0-recipe': '',
            'recipegrain_set-0-fermentable': '37',
            'recipegrain_set-0-quantity': '4',
            'recipegrain_set-0-unit': 'kg',
            'recipegrain_set-0-id': '',
            'recipegrain_set-0-DELETE': '',
            'recipegrain_set-1-recipe': '',
            'recipegrain_set-1-fermentable': '',
            'recipegrain_set-1-quantity': '',
            'recipegrain_set-1-unit': '',
            'recipegrain_set-1-id': '',
            'recipegrain_set-1-DELETE': '',
            'boil_liquor': '27',
            'boil_time': '65',
            'recipehop_set-TOTAL_FORMS': '2',
            'recipehop_set-INITIAL_FORMS': '0',
            'recipehop_set-MIN_NUM_FORMS': '0',
            'recipehop_set-MAX_NUM_FORMS': '1000',
            'recipehop_set-0-recipe': '',
            'recipehop_set-0-hop': '35',
            'recipehop_set-0-alpha_acid_rating': '8',
            'recipehop_set-0-quantity': '0',
            'recipehop_set-0-ibu': '50',
            'recipehop_set-0-when_to_add': '65',
            'recipehop_set-0-id': '',
            'recipehop_set-0-DELETE': '',
            'recipehop_set-1-recipe': '',
            'recipehop_set-1-hop': '35',
            'recipehop_set-1-alpha_acid_rating': '8',
            'recipehop_set-1-quantity': '15',
            'recipehop_set-1-ibu': '',
            'recipehop_set-1-when_to_add': '15',
            'recipehop_set-1-id': '',
            'recipehop_set-1-DELETE': '',
            'recipeother_set-TOTAL_FORMS': '2',
            'recipeother_set-INITIAL_FORMS': '0',
            'recipeother_set-MIN_NUM_FORMS': '0',
            'recipeother_set-MAX_NUM_FORMS': '1000',
            'recipeother_set-0-recipe': '',
            'recipeother_set-0-other': '1',
            'recipeother_set-0-quantity': '1',
            'recipeother_set-0-unit': 'tsp',
            'recipeother_set-0-when_to_add': '15 minutes from end',
            'recipeother_set-0-id': '',
            'recipeother_set-0-DELETE': '',
            'recipeother_set-1-recipe': '',
            'recipeother_set-1-other': '',
            'recipeother_set-1-quantity': '',
            'recipeother_set-1-unit': '',
            'recipeother_set-1-when_to_add': '',
            'recipeother_set-1-id': '',
            'recipeother_set-1-DELETE': '',
            'yeast': '64',
            'fermentation_temp': '18',
            'conditioning_time': '2',
            'conditioning_time_quantifier': 'w',
            'conditioning_temp': '12',
            'submit': 'Save'
        }
        response = self.client.post(create_url, data=post_data)
        self.assertTemplateNotUsed(response, 'recipe/recipe_create.html', 'Recipe wasn\'t created successfully')

        # Make sure everything is there and correct...
        # Recipe
        recipe = AllGrainRecipe.objects.get(name='test all grain second')
        self.assertRedirects(response, recipe.get_absolute_url())

        self.assertEqual(2, recipe.recipehop_set.count())

        # Similar to IBU test, but with quantity
        post_data = {
            'name': 'test all grain third',
            'public': 'on',
            'style': '1',
            'target_volume': '23',
            'description': '',
            'original_gravity': '1.081',
            'expected_final_gravity': '1.005',
            'owner': '1',
            'mash_liquor': '17',
            'mash_thickness': '4.25',
            'sparge_volume': '0',
            'mash_time': '90',
            'mash_temperature': '65',
            'recipegrain_set-TOTAL_FORMS': '2',
            'recipegrain_set-INITIAL_FORMS': '0',
            'recipegrain_set-MIN_NUM_FORMS': '0',
            'recipegrain_set-MAX_NUM_FORMS': '1000',
            'recipegrain_set-0-recipe': '',
            'recipegrain_set-0-fermentable': '37',
            'recipegrain_set-0-quantity': '4',
            'recipegrain_set-0-unit': 'kg',
            'recipegrain_set-0-id': '',
            'recipegrain_set-0-DELETE': '',
            'recipegrain_set-1-recipe': '',
            'recipegrain_set-1-fermentable': '',
            'recipegrain_set-1-quantity': '',
            'recipegrain_set-1-unit': '',
            'recipegrain_set-1-id': '',
            'recipegrain_set-1-DELETE': '',
            'boil_liquor': '27',
            'boil_time': '65',
            'recipehop_set-TOTAL_FORMS': '2',
            'recipehop_set-INITIAL_FORMS': '0',
            'recipehop_set-MIN_NUM_FORMS': '0',
            'recipehop_set-MAX_NUM_FORMS': '1000',
            'recipehop_set-0-recipe': '',
            'recipehop_set-0-hop': '35',
            'recipehop_set-0-alpha_acid_rating': '8',
            'recipehop_set-0-quantity': '0',
            'recipehop_set-0-ibu': '50',
            'recipehop_set-0-when_to_add': '65',
            'recipehop_set-0-id': '',
            'recipehop_set-0-DELETE': '',
            'recipehop_set-1-recipe': '',
            'recipehop_set-1-hop': '35',
            'recipehop_set-1-alpha_acid_rating': '8',
            'recipehop_set-1-quantity': '',
            'recipehop_set-1-ibu': '20',
            'recipehop_set-1-when_to_add': '15',
            'recipehop_set-1-id': '',
            'recipehop_set-1-DELETE': '',
            'recipeother_set-TOTAL_FORMS': '2',
            'recipeother_set-INITIAL_FORMS': '0',
            'recipeother_set-MIN_NUM_FORMS': '0',
            'recipeother_set-MAX_NUM_FORMS': '1000',
            'recipeother_set-0-recipe': '',
            'recipeother_set-0-other': '1',
            'recipeother_set-0-quantity': '1',
            'recipeother_set-0-unit': 'tsp',
            'recipeother_set-0-when_to_add': '15 minutes from end',
            'recipeother_set-0-id': '',
            'recipeother_set-0-DELETE': '',
            'recipeother_set-1-recipe': '',
            'recipeother_set-1-other': '',
            'recipeother_set-1-quantity': '',
            'recipeother_set-1-unit': '',
            'recipeother_set-1-when_to_add': '',
            'recipeother_set-1-id': '',
            'recipeother_set-1-DELETE': '',
            'yeast': '64',
            'fermentation_temp': '18',
            'conditioning_time': '2',
            'conditioning_time_quantifier': 'w',
            'conditioning_temp': '12',
            'submit': 'Save'
        }
        response = self.client.post(create_url, data=post_data)
        self.assertTemplateNotUsed(response, 'recipe/recipe_create.html', 'Recipe wasn\'t created successfully')

        # Make sure everything is there and correct...
        # Recipe
        recipe = AllGrainRecipe.objects.get(name='test all grain third')
        self.assertRedirects(response, recipe.get_absolute_url())

        self.assertEqual(2, recipe.recipehop_set.count())

    def test_update_basic_recipe(self):
        recipe = BaseRecipe.objects.get(pk=22)
        update_url = recipe.get_update_url()
        # Test that you need to be logged in first
        response = self.client.get(update_url)
        self.assertRedirects(response, reverse('account_login')+'?next='+update_url)

        # Login
        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(update_url)
        self.assertResponseOk(response)
        # Create recipe
        post_data = {
            'name': 'test base',
            'public': 'on',
            'style': '1',
            'target_volume': '23',
            'description': '',
            'original_gravity': '1.081',
            'expected_final_gravity': '1.005',
            'owner': '1',
            'recipegrain_set-TOTAL_FORMS': '2',
            'recipegrain_set-INITIAL_FORMS': '1',
            'recipegrain_set-MIN_NUM_FORMS': '0',
            'recipegrain_set-MAX_NUM_FORMS': '1000',
            'recipegrain_set-0-recipe': '22',
            'recipegrain_set-0-fermentable': '37',
            'recipegrain_set-0-quantity': '4',
            'recipegrain_set-0-unit': 'kg',
            'recipegrain_set-0-id': '33',
            'recipegrain_set-0-DELETE': '',
            'recipegrain_set-1-recipe': '22',
            'recipegrain_set-1-fermentable': '',
            'recipegrain_set-1-quantity': '',
            'recipegrain_set-1-unit': '',
            'recipegrain_set-1-id': '',
            'recipegrain_set-1-DELETE': '',
            'recipeother_set-TOTAL_FORMS': '2',
            'recipeother_set-INITIAL_FORMS': '0',
            'recipeother_set-MIN_NUM_FORMS': '0',
            'recipeother_set-MAX_NUM_FORMS': '1000',
            'recipeother_set-0-recipe': '22',
            'recipeother_set-0-other': '1',
            'recipeother_set-0-quantity': '1',
            'recipeother_set-0-unit': 'tsp',
            'recipeother_set-0-when_to_add': '15 minutes from end',
            'recipeother_set-0-id': '',
            'recipeother_set-0-DELETE': '',
            'recipeother_set-1-recipe': '22',
            'recipeother_set-1-other': '',
            'recipeother_set-1-quantity': '',
            'recipeother_set-1-unit': '',
            'recipeother_set-1-when_to_add': '',
            'recipeother_set-1-id': '',
            'recipeother_set-1-DELETE': '',
            'yeast': '64',
            'fermentation_temp': '18',
            'conditioning_time': '2',
            'conditioning_time_quantifier': 'w',
            'conditioning_temp': '12',
            'submit': 'Save'
        }
        response = self.client.post(update_url, data=post_data)
        self.assertTemplateNotUsed(response, 'recipe/recipe_update.html', 'Recipe wasn''t updated successfully')

        recipe = BaseRecipe.objects.get(pk=22)
        self.assertRedirects(response, recipe.get_absolute_url())

        # Make sure everything is there and correct...
        # Recipe
        self.assertEqual('test base', recipe.name)

        self.assertRedirects(response, recipe.get_absolute_url())

        self.assertEqual(True, recipe.public)
        self.assertEqual(1, recipe.style.pk)
        self.assertEqual(23, recipe.target_volume)
        self.assertEqual('1.081', str(recipe.original_gravity))
        self.assertEqual('1.005', str(recipe.expected_final_gravity))
        self.assertEqual(64, recipe.yeast.pk)
        self.assertEqual(18, recipe.fermentation_temp)
        self.assertEqual(2, recipe.conditioning_time)
        self.assertEqual('w', recipe.conditioning_time_quantifier)
        self.assertEqual(12, recipe.conditioning_temp)

        # Fermentables
        fermentables = recipe.recipegrain_set.all()
        self.assertEqual(1, len(fermentables))
        rg = fermentables[0]
        self.assertEqual(37, rg.fermentable.pk)
        self.assertEqual(4, rg.quantity)
        self.assertEqual('kg', rg.unit)

        # Others
        others = recipe.recipeother_set.all()
        self.assertEqual(1, len(others))
        ro = others[0]
        self.assertEqual(1, ro.other.pk)
        self.assertEqual(1, ro.quantity)
        self.assertEqual('tsp', ro.unit)

    def test_update_extract_recipe(self):
        recipe = ExtractRecipe.objects.get(pk=21)
        update_url = recipe.get_update_url()
        # Test that you need to be logged in first
        response = self.client.get(update_url)
        self.assertRedirects(response, reverse('account_login')+'?next='+update_url)

        # Login
        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(update_url)
        self.assertResponseOk(response)
        # Create recipe
        post_data = {
            'name': 'test extract',
            'public': 'on',
            'style': '1',
            'target_volume': '23',
            'description': '',
            'original_gravity': '1.081',
            'expected_final_gravity': '1.005',
            'owner': '1',
            'steep_liquor': '0',
            'steep_time': '',
            'steep_temperature': '',
            'recipegrain_set-TOTAL_FORMS': '2',
            'recipegrain_set-INITIAL_FORMS': '2',
            'recipegrain_set-MIN_NUM_FORMS': '0',
            'recipegrain_set-MAX_NUM_FORMS': '1000',
            'recipegrain_set-0-recipe': '21',
            'recipegrain_set-0-fermentable': '37',
            'recipegrain_set-0-quantity': '4',
            'recipegrain_set-0-unit': 'kg',
            'recipegrain_set-0-id': '31',
            'recipegrain_set-0-DELETE': '',
            'recipegrain_set-1-recipe': '21',
            'recipegrain_set-1-fermentable': '',
            'recipegrain_set-1-quantity': '',
            'recipegrain_set-1-unit': '',
            'recipegrain_set-1-id': '32',
            'recipegrain_set-1-DELETE': 'on',
            'boil_liquor': '27',
            'boil_time': '65',
            'recipehop_set-TOTAL_FORMS': '2',
            'recipehop_set-INITIAL_FORMS': '1',
            'recipehop_set-MIN_NUM_FORMS': '0',
            'recipehop_set-MAX_NUM_FORMS': '1000',
            'recipehop_set-0-recipe': '21',
            'recipehop_set-0-hop': '35',
            'recipehop_set-0-alpha_acid_rating': '8',
            'recipehop_set-0-quantity': '0',
            'recipehop_set-0-ibu': '50',
            'recipehop_set-0-when_to_add': '65',
            'recipehop_set-0-id': '31',
            'recipehop_set-0-DELETE': '',
            'recipehop_set-1-recipe': '21',
            'recipehop_set-1-hop': '',
            'recipehop_set-1-alpha_acid_rating': '',
            'recipehop_set-1-quantity': '0',
            'recipehop_set-1-ibu': '0',
            'recipehop_set-1-when_to_add': '',
            'recipehop_set-1-id': '',
            'recipehop_set-1-DELETE': '',
            'recipeother_set-TOTAL_FORMS': '2',
            'recipeother_set-INITIAL_FORMS': '0',
            'recipeother_set-MIN_NUM_FORMS': '0',
            'recipeother_set-MAX_NUM_FORMS': '1000',
            'recipeother_set-0-recipe': '21',
            'recipeother_set-0-other': '1',
            'recipeother_set-0-quantity': '1',
            'recipeother_set-0-unit': 'tsp',
            'recipeother_set-0-when_to_add': '15 minutes from end',
            'recipeother_set-0-id': '',
            'recipeother_set-0-DELETE': '',
            'recipeother_set-1-recipe': '21',
            'recipeother_set-1-other': '',
            'recipeother_set-1-quantity': '',
            'recipeother_set-1-unit': '',
            'recipeother_set-1-when_to_add': '',
            'recipeother_set-1-id': '',
            'recipeother_set-1-DELETE': '',
            'yeast': '64',
            'fermentation_temp': '18',
            'conditioning_time': '2',
            'conditioning_time_quantifier': 'w',
            'conditioning_temp': '12',
            'submit': 'Save'
        }
        response = self.client.post(update_url, data=post_data)
        self.assertTemplateNotUsed(response, 'recipe/recipe_update.html', 'Recipe wasn''t updated successfully')
        recipe = ExtractRecipe.objects.get(pk=21)
        self.assertRedirects(response, recipe.get_absolute_url())

        # Make sure everything is there and correct...
        # Recipe

        self.assertEqual('test extract', recipe.name)

        self.assertRedirects(response, recipe.get_absolute_url())

        self.assertEqual(True, recipe.public)
        self.assertEqual(1, recipe.style.pk)
        self.assertEqual(23, recipe.target_volume)
        self.assertEqual('1.081', str(recipe.original_gravity))
        self.assertEqual('1.005', str(recipe.expected_final_gravity))
        self.assertEqual(0, recipe.steep_liquor)
        self.assertEqual(27, recipe.boil_liquor)
        self.assertEqual(65, recipe.boil_time)
        self.assertEqual(64, recipe.yeast.pk)
        self.assertEqual(18, recipe.fermentation_temp)
        self.assertEqual(2, recipe.conditioning_time)
        self.assertEqual('w', recipe.conditioning_time_quantifier)
        self.assertEqual(12, recipe.conditioning_temp)

        # Fermentables
        fermentables = recipe.recipegrain_set.all()
        self.assertEqual(1, len(fermentables))
        rg = fermentables[0]
        self.assertEqual(37, rg.fermentable.pk)
        self.assertEqual(4, rg.quantity)
        self.assertEqual('kg', rg.unit)


        # Hops
        hops = recipe.recipehop_set.all()
        self.assertEqual(1, len(hops))
        rh = hops[0]
        self.assertEqual(35, rh.hop.pk)
        self.assertEqual(8, rh.alpha_acid_rating)
        self.assertNotEqual(0, rh.quantity)
        self.assertEqual(50, rh.ibu)
        self.assertEqual(65, rh.when_to_add)

        # Others
        others = recipe.recipeother_set.all()
        self.assertEqual(1, len(others))
        ro = others[0]
        self.assertEqual(1, ro.other.pk)
        self.assertEqual(1, ro.quantity)
        self.assertEqual('tsp', ro.unit)

        # Test steep_liquor is updated if a value other than 0 is set
        # or any of the fermentables require steeping

        # Steep liquor is smaller
        post_data['steep_liquor'] = 12

        response = self.client.post(update_url, data=post_data)
        self.assertTemplateNotUsed(response, 'recipe/recipe_update.html', 'Recipe wasn''t updated successfully')
        recipe = ExtractRecipe.objects.get(pk=21)
        self.assertRedirects(response, recipe.get_absolute_url())

        # Boil liquor is larger, so steep liquor should be the same
        self.assertEqual(recipe.boil_liquor, recipe.steep_liquor)
        self.assertEqual(27, recipe.steep_liquor)

        # Steep liquor is larger
        post_data['steep_liquor'] = 30

        response = self.client.post(update_url, data=post_data)
        self.assertTemplateNotUsed(response, 'recipe/recipe_update.html', 'Recipe wasn''t updated successfully')
        recipe = ExtractRecipe.objects.get(pk=21)
        self.assertRedirects(response, recipe.get_absolute_url())

        # Boil liquor is larger, so steep liquor should be the same
        self.assertEqual(recipe.boil_liquor, recipe.steep_liquor)
        self.assertEqual(30, recipe.steep_liquor)

        # Steep liquor is null
        post_data['steep_liquor'] = ''

        response = self.client.post(update_url, data=post_data)
        self.assertTemplateNotUsed(response, 'recipe/recipe_update.html', 'Recipe wasn''t updated successfully')
        recipe = ExtractRecipe.objects.get(pk=21)
        self.assertRedirects(response, recipe.get_absolute_url())

        # Steep liquor should be None
        self.assertIsNone(recipe.steep_liquor)
        self.assertEqual(27, recipe.boil_liquor)

        # Steep liquor is null, but a fermentable requires steeping
        post_data['steep_liquor'] = ''
        post_data.update({
            'recipegrain_set-INITIAL_FORMS': '1',
            'recipegrain_set-1-recipe': '21',
            'recipegrain_set-1-fermentable': '37',
            'recipegrain_set-1-quantity': '200',
            'recipegrain_set-1-unit': 'g',
            'recipegrain_set-1-steeped': 'on',
            'recipegrain_set-1-id': '',
            'recipegrain_set-1-DELETE': '',
        })

        response = self.client.post(update_url, data=post_data)
        self.assertTemplateNotUsed(response, 'recipe/recipe_update.html', 'Recipe wasn''t updated successfully')
        recipe = ExtractRecipe.objects.get(pk=21)
        self.assertRedirects(response, recipe.get_absolute_url())

        # Boil liquor is larger, so steep liquor should be the same
        self.assertEqual(2, recipe.recipegrain_set.count())
        self.assertIsNotNone(recipe.steep_liquor)
        self.assertEqual(27, recipe.boil_liquor)
        self.assertEqual(27, recipe.steep_liquor)

    def test_update_all_grain_recipe(self):
        recipe = AllGrainRecipe.objects.get(pk=19)
        update_url = recipe.get_update_url()
        # Test that you need to be logged in first
        response = self.client.get(update_url)
        self.assertRedirects(response, reverse('account_login')+'?next='+update_url)

        # Login
        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(update_url)
        self.assertResponseOk(response)
        # Create recipe
        post_data = {
            'name': 'test all grain',
            'public': 'on',
            'style': '1',
            'target_volume': '23',
            'description': '',
            'original_gravity': '1.081',
            'expected_final_gravity': '1.005',
            'owner': '1',
            'mash_liquor': '17',
            'mash_thickness': '4.25',
            'sparge_volume': '0',
            'mash_time': '90',
            'mash_temperature': '65',
            'recipegrain_set-TOTAL_FORMS': '3',
            'recipegrain_set-INITIAL_FORMS': '3',
            'recipegrain_set-MIN_NUM_FORMS': '0',
            'recipegrain_set-MAX_NUM_FORMS': '1000',
            'recipegrain_set-0-recipe': '19',
            'recipegrain_set-0-fermentable': '37',
            'recipegrain_set-0-quantity': '4',
            'recipegrain_set-0-unit': 'kg',
            'recipegrain_set-0-id': '23',
            'recipegrain_set-0-DELETE': '',
            'recipegrain_set-1-recipe': '19',
            'recipegrain_set-1-fermentable': '',
            'recipegrain_set-1-quantity': '',
            'recipegrain_set-1-unit': '',
            'recipegrain_set-1-id': '24',
            'recipegrain_set-1-DELETE': 'on',
            'recipegrain_set-2-recipe': '19',
            'recipegrain_set-2-fermentable': '',
            'recipegrain_set-2-quantity': '',
            'recipegrain_set-2-unit': '',
            'recipegrain_set-2-id': '25',
            'recipegrain_set-2-DELETE': 'on',
            'boil_liquor': '27',
            'boil_time': '65',
            'recipehop_set-TOTAL_FORMS': '3',
            'recipehop_set-INITIAL_FORMS': '3',
            'recipehop_set-MIN_NUM_FORMS': '0',
            'recipehop_set-MAX_NUM_FORMS': '1000',
            'recipehop_set-0-recipe': '19',
            'recipehop_set-0-hop': '35',
            'recipehop_set-0-alpha_acid_rating': '8',
            'recipehop_set-0-quantity': '0',
            'recipehop_set-0-ibu': '50',
            'recipehop_set-0-when_to_add': '65',
            'recipehop_set-0-id': '23',
            'recipehop_set-0-DELETE': '',
            'recipehop_set-1-recipe': '19',
            'recipehop_set-1-hop': '',
            'recipehop_set-1-alpha_acid_rating': '',
            'recipehop_set-1-quantity': '0',
            'recipehop_set-1-ibu': '0',
            'recipehop_set-1-when_to_add': '',
            'recipehop_set-1-id': '24',
            'recipehop_set-1-DELETE': 'on',
            'recipehop_set-2-recipe': '19',
            'recipehop_set-2-hop': '',
            'recipehop_set-2-alpha_acid_rating': '',
            'recipehop_set-2-quantity': '0',
            'recipehop_set-2-ibu': '0',
            'recipehop_set-2-when_to_add': '',
            'recipehop_set-2-id': '25',
            'recipehop_set-2-DELETE': 'on',
            'recipeother_set-TOTAL_FORMS': '1',
            'recipeother_set-INITIAL_FORMS': '1',
            'recipeother_set-MIN_NUM_FORMS': '0',
            'recipeother_set-MAX_NUM_FORMS': '1000',
            'recipeother_set-0-recipe': '19',
            'recipeother_set-0-other': '1',
            'recipeother_set-0-quantity': '1',
            'recipeother_set-0-unit': 'tsp',
            'recipeother_set-0-when_to_add': '15 minutes from end',
            'recipeother_set-0-id': '6',
            'recipeother_set-0-DELETE': '',
            'yeast': '64',
            'fermentation_temp': '18',
            'conditioning_time': '2',
            'conditioning_time_quantifier': 'w',
            'conditioning_temp': '12',
            'submit': 'Save'
        }
        response = self.client.post(update_url, data=post_data)
        self.assertTemplateNotUsed(response, 'recipe/recipe_update.html', 'Recipe wasn\'t updated successfully')

        recipe = AllGrainRecipe.objects.get(pk=19)
        self.assertRedirects(response, recipe.get_absolute_url())

        # Make sure everything is there and correct...
        # Recipe

        self.assertEqual('test all grain', recipe.name)

        self.assertRedirects(response, recipe.get_absolute_url())

        self.assertEqual(True, recipe.public)
        self.assertEqual(1, recipe.style.pk)
        self.assertEqual(23, recipe.target_volume)
        self.assertEqual('1.081', str(recipe.original_gravity))
        self.assertEqual('1.005', str(recipe.expected_final_gravity))
        self.assertEqual(17, recipe.mash_liquor)
        self.assertEqual(4.25, recipe.mash_thickness)
        self.assertEqual(90, recipe.mash_time)
        self.assertEqual(65, recipe.mash_temperature)
        self.assertEqual(27, recipe.boil_liquor)
        self.assertEqual(65, recipe.boil_time)
        self.assertEqual(64, recipe.yeast.pk)
        self.assertEqual(18, recipe.fermentation_temp)
        self.assertEqual(2, recipe.conditioning_time)
        self.assertEqual('w', recipe.conditioning_time_quantifier)
        self.assertEqual(12, recipe.conditioning_temp)

        # Fermentables
        fermentables = recipe.recipegrain_set.all()
        self.assertEqual(1, len(fermentables))
        rg = fermentables[0]
        self.assertEqual(37, rg.fermentable.pk)
        self.assertEqual(4, rg.quantity)
        self.assertEqual('kg', rg.unit)


        # Hops
        hops = recipe.recipehop_set.all()
        self.assertEqual(1, len(hops))
        rh = hops[0]
        self.assertEqual(35, rh.hop.pk)
        self.assertEqual(8, rh.alpha_acid_rating)
        self.assertNotEqual(0, rh.quantity)
        self.assertEqual(50, rh.ibu)
        self.assertEqual(65, rh.when_to_add)

        # Others
        others = recipe.recipeother_set.all()
        self.assertEqual(1, len(others))
        ro = others[0]
        self.assertEqual(1, ro.other.pk)
        self.assertEqual(1, ro.quantity)
        self.assertEqual('tsp', ro.unit)

    def test_update_basic_recipe_calculate_fields(self):
        recipe = BaseRecipe.objects.get(pk=22)
        update_url = recipe.get_update_url()
        # Test that you need to be logged in first
        response = self.client.get(update_url)
        self.assertRedirects(response, reverse('account_login')+'?next='+update_url)

        # Login
        self.client.login(username=self.user_name, password=self.user_password)
        response = self.client.get(update_url)
        self.assertResponseOk(response)
        # Create recipe
        post_data = {
            'name': 'test base',
            'public': 'on',
            'style': '1',
            'target_volume': '23',
            'description': '',
            'original_gravity': '0',
            'expected_final_gravity': '0',
            'owner': '1',
            'recipegrain_set-TOTAL_FORMS': '2',
            'recipegrain_set-INITIAL_FORMS': '1',
            'recipegrain_set-MIN_NUM_FORMS': '0',
            'recipegrain_set-MAX_NUM_FORMS': '1000',
            'recipegrain_set-0-recipe': '22',
            'recipegrain_set-0-fermentable': '37',
            'recipegrain_set-0-quantity': '4',
            'recipegrain_set-0-unit': 'kg',
            'recipegrain_set-0-id': '33',
            'recipegrain_set-0-DELETE': '',
            'recipegrain_set-1-recipe': '22',
            'recipegrain_set-1-fermentable': '37',
            'recipegrain_set-1-quantity': '3',
            'recipegrain_set-1-unit': 'kg',
            'recipegrain_set-1-id': '',
            'recipegrain_set-1-DELETE': '',
            'recipeother_set-TOTAL_FORMS': '2',
            'recipeother_set-INITIAL_FORMS': '0',
            'recipeother_set-MIN_NUM_FORMS': '0',
            'recipeother_set-MAX_NUM_FORMS': '1000',
            'recipeother_set-0-recipe': '22',
            'recipeother_set-0-other': '1',
            'recipeother_set-0-quantity': '1',
            'recipeother_set-0-unit': 'tsp',
            'recipeother_set-0-when_to_add': '15 minutes from end',
            'recipeother_set-0-id': '',
            'recipeother_set-0-DELETE': '',
            'recipeother_set-1-recipe': '22',
            'recipeother_set-1-other': '',
            'recipeother_set-1-quantity': '',
            'recipeother_set-1-unit': '',
            'recipeother_set-1-when_to_add': '',
            'recipeother_set-1-id': '',
            'recipeother_set-1-DELETE': '',
            'yeast': '64',
            'fermentation_temp': '18',
            'conditioning_time': '2',
            'conditioning_time_quantifier': 'w',
            'conditioning_temp': '12',
            'submit': 'Save'
        }
        response = self.client.post(update_url, data=post_data)
        self.assertTemplateNotUsed(response, 'recipe/recipe_update.html', 'Recipe wasn''t updated successfully')

        recipe = BaseRecipe.objects.get(pk=22)
        self.assertRedirects(response, recipe.get_absolute_url())

        # Make sure everything is there and correct...
        # Recipe
        self.assertEqual('test base', recipe.name)

        self.assertRedirects(response, recipe.get_absolute_url())

        self.assertEqual(True, recipe.public)
        self.assertEqual(1, recipe.style.pk)
        self.assertEqual(23, recipe.target_volume)
        self.assertNotEqual('1.046', str(recipe.original_gravity))
        self.assertNotEqual('1.012', str(recipe.expected_final_gravity))
        self.assertNotEqual('0', str(recipe.original_gravity), "Original gravity didn't calculate")
        self.assertNotEqual('0', str(recipe.expected_final_gravity), "Final gravity didn't calculate")
        # Fermentables has increased significantly
        self.assertTrue(recipe.original_gravity > 1.046)
        self.assertEqual(64, recipe.yeast.pk)
        self.assertEqual(18, recipe.fermentation_temp)
        self.assertEqual(2, recipe.conditioning_time)
        self.assertEqual('w', recipe.conditioning_time_quantifier)
        self.assertEqual(12, recipe.conditioning_temp)

        # Fermentables
        fermentables = recipe.recipegrain_set.all()
        self.assertEqual(2, len(fermentables))
        rg = fermentables[0]
        self.assertEqual(37, rg.fermentable.pk)
        self.assertEqual(4, rg.quantity)
        self.assertEqual('kg', rg.unit)

        # Others
        others = recipe.recipeother_set.all()
        self.assertEqual(1, len(others))
        ro = others[0]
        self.assertEqual(1, ro.other.pk)
        self.assertEqual(1, ro.quantity)
        self.assertEqual('tsp', ro.unit)