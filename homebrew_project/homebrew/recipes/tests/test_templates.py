from datetime import datetime
from bs4 import BeautifulSoup
from django.core.urlresolvers import reverse
from django.template.defaultfilters import floatformat
from brews.models import Brew, TemperatureReading

from recipes.models import AllGrainRecipe, ExtractRecipe, BaseRecipe


__author__ = 'sam'

from django.test import TestCase


class RecipeTemplateTestCase(TestCase):
    fixtures = ['Users', 'SampleDB']

    def setUp(self):
        super().setUp()

        self.user_name = 'test_user'
        self.user_password = 'test_password'

        self.user_two_name = 'test_two'
        self.user_two_password = 'test_password'

        self.superuser_name = 'superuser'
        self.superuser_password = 'superpassword'

    def test_allgrainrecipe_detail_template(self):
        recipe = AllGrainRecipe.objects.get(pk=19)

        self.client.login(username=self.user_name, password=self.user_password)

        # Go to the recipe
        response = self.client.get(recipe.get_absolute_url())
        soup = BeautifulSoup(response.content.decode('utf-8'), "html.parser")

        # All the panels are displayed
        general = soup.select('#general')
        mash = soup.select('#mash')
        boil = soup.select('#boil')
        fermentation = soup.select('#fermentation')
        other_additions = soup.select('#other_additions')

        self.assertTrue(len(general) == 1)
        self.assertTrue(len(mash) == 1)
        self.assertTrue(len(boil) == 1)
        self.assertTrue(len(fermentation) == 1)
        self.assertTrue(len(other_additions) == 1)

        # Make sure the values are being displayed
        name = soup.select('#name')[0]
        self.assertEqual(recipe.name, name.string)

        style = soup.select('#style')[0]
        self.assertEqual(recipe.style.name, style.string)

        description = soup.select('#description')[0]
        self.assertEqual(recipe.description, str(description.string).strip())

        target_volume = soup.select('#target_volume')[0]
        self.assertEqual(str(recipe.target_volume)+' L', target_volume.string)

        original_gravity = soup.select('#original_gravity')[0]
        self.assertEqual(str(recipe.original_gravity), original_gravity.string)

        expected_final_gravity = soup.select('#expected_final_gravity')[0]
        self.assertEqual(str(recipe.expected_final_gravity), expected_final_gravity.string)

        estimated_abv = soup.select('#alcohol_by_volume')[0]
        self.assertEqual(format(recipe.alcohol_by_volume(), '.2f')+'%', estimated_abv.string)

        bitterness_rating = soup.select('#bitterness_rating')[0]
        self.assertEqual(str(recipe.get_bitterness_rating()), bitterness_rating.string)

        mash_liquor = soup.select('#mash_liquor')[0]
        self.assertEqual('11.5 L', mash_liquor.string)

        mash_thickness = soup.select('#mash_thickness')[0]
        self.assertEqual(str(recipe.mash_thickness)+' L/kg', mash_thickness.string)

        sparge_volume = soup.select('#sparge_volume')[0]
        self.assertEqual('0 L', sparge_volume.string)

        mash_time = soup.select('#mash_time')[0]
        self.assertEqual(str(recipe.mash_time)+' mins', mash_time.string)

        mash_temp = soup.select('#mash_temperature')[0]
        self.assertEqual(str(recipe.mash_temperature)+'°C', mash_temp.string)

        # Grain
        for i, grain in enumerate(list(recipe.get_recipegrain_ordered())):
            grain_elem = soup.select('#{}_grain a'.format(i+1))[0]
            self.assertEqual(grain.fermentable.name, grain_elem.string)

            grain_quantity = soup.select('#{}_grain_quantity'.format(i+1))[0]
            self.assertEqual('{} {}'.format(grain.quantity, grain.unit), grain_quantity.string)

        boil_liquor = soup.select('#boil_liquor')[0]
        self.assertEqual('27 L', boil_liquor.string)

        boil_time = soup.select('#boil_time')[0]
        self.assertEqual(str(recipe.boil_time)+' mins', boil_time.string)

        # Hops
        for i, hop in enumerate(list(recipe.get_recipehop_ordered())):
            hop_elem = soup.select('#{}_hop a'.format(i+1))[0]
            self.assertEqual('{} {}%'.format(hop.hop.name, hop.alpha_acid_rating), hop_elem.string)

            hop_quantity = soup.select('#{}_hop_quantity'.format(i+1))[0]
            self.assertEqual('{:.0f} g'.format(hop.quantity, grain.unit), hop_quantity.string)

            hop_ibu = soup.select('#{}_hop_ibu'.format(i+1))[0]
            self.assertEqual(format(hop.ibu, '.1f'), hop_ibu.string)

            wta = soup.select('#{}_hop_wta'.format(i+1))[0]
            self.assertEqual(hop.display_when_to_add(), wta.string)

        # Other additions
        for i, other in enumerate(list(recipe.get_recipeother_ordered())):
            other_elem = soup.select('#{}_other a'.format(i+1))[0]
            self.assertEqual(str(other.other), other_elem.string)

            other_quantity = soup.select('#{}_other_quantity'.format(i+1))[0]
            self.assertEqual('{} {}'.format(other.quantity, other.unit), other_quantity.string)

            other_wta = soup.select('#{}_other_wta'.format(i+1))[0]
            self.assertEqual(other.when_to_add, other_wta.string)

        yeast = soup.select('#yeast a')[0]
        self.assertEqual(str(recipe.yeast), yeast.string)

        fermentation_temp = soup.select('#fermentation_temp')[0]
        self.assertEqual(str(recipe.fermentation_temp)+'°C', fermentation_temp.string)

        conditioning = soup.select('#conditioning_time')[0]
        self.assertEqual('{0} {1}@{2}°C'.format(recipe.conditioning_time, recipe.get_conditioning_time_quantifier_display(),
                                             recipe.conditioning_temp), conditioning.string)

        # Because we're logged in and the owner, the update, delete and brew it buttons should be visible too
        back = soup.select('#back')
        update = soup.select('#update')
        delete = soup.select('#delete')
        brew_it = soup.select('#brew_it')

        self.assertTrue(len(back) == 1)
        self.assertTrue(len(update) == 1)
        self.assertTrue(len(delete) == 1)
        self.assertTrue(len(brew_it) == 1)

    def test_extractrecipe_detail_template(self):
        recipe = ExtractRecipe.objects.get(pk=21)

        self.client.login(username=self.user_name, password=self.user_password)

        # Go to the recipe
        response = self.client.get(recipe.get_absolute_url())
        soup = BeautifulSoup(response.content.decode('utf-8'), "html.parser")

        # All the panels are displayed
        general = soup.select('#general')
        mash = soup.select('#mash')
        fermentables = soup.select('#fermentables')
        boil = soup.select('#boil')
        fermentation = soup.select('#fermentation')
        other_additions = soup.select('#other_additions')

        self.assertTrue(len(general) == 1)
        self.assertTrue(len(fermentables) == 1)
        self.assertTrue(len(mash) == 0)
        self.assertTrue(len(boil) == 1)
        self.assertTrue(len(fermentation) == 1)
        self.assertTrue(len(other_additions) == 1)

        # Make sure the values are being displayed
        name = soup.select('#name')[0]
        self.assertEqual(recipe.name, name.string)

        style = soup.select('#style')[0]
        self.assertEqual(recipe.style.name, style.string)

        description = soup.select('#description')[0]
        self.assertEqual(recipe.description, str(description.string).strip())

        target_volume = soup.select('#target_volume')[0]
        self.assertEqual(str(recipe.target_volume)+' L', target_volume.string)

        original_gravity = soup.select('#original_gravity')[0]
        self.assertEqual(str(recipe.original_gravity), original_gravity.string)

        expected_final_gravity = soup.select('#expected_final_gravity')[0]
        self.assertEqual(str(recipe.expected_final_gravity), expected_final_gravity.string)

        estimated_abv = soup.select('#alcohol_by_volume')[0]
        self.assertEqual(format(recipe.alcohol_by_volume(), '.2f')+'%', estimated_abv.string)

        bitterness_rating = soup.select('#bitterness_rating')[0]
        self.assertEqual(str(recipe.get_bitterness_rating()), bitterness_rating.string)

        steep_liquor = soup.select('#steep_liquor')[0]
        self.assertEqual('0 L', steep_liquor.string)

        steep_time = soup.select('#steep_time')[0]
        self.assertEqual(str(recipe.steep_time)+' mins', steep_time.string)

        steep_temperature = soup.select('#steep_temperature')[0]
        self.assertEqual(str(recipe.steep_temperature)+'°C', steep_temperature.string)

        mash_liquor = soup.select('#mash_liquor')
        self.assertTrue(len(mash_liquor) == 0)

        mash_time = soup.select('#mash_time')
        self.assertTrue(len(mash_time) == 0)

        mash_temp = soup.select('#mash_temperature')
        self.assertTrue(len(mash_temp) == 0)

        # Grain
        for i, grain in enumerate(list(recipe.get_recipegrain_ordered())):
            grain_elem = soup.select('#{}_grain a'.format(i+1))[0]
            self.assertEqual(grain.fermentable.name, grain_elem.string)

            grain_quantity = soup.select('#{}_grain_quantity'.format(i+1))[0]
            self.assertEqual('{} {}'.format(grain.quantity, grain.unit), grain_quantity.string)

            grain_steeped = soup.select('#{}_grain_steep .glyphicon'.format(i+1))
            if grain.steeped:
                self.assertTrue(len(grain_steeped) == 1)
            else:
                self.assertTrue(len(grain_steeped) == 0)

        boil_liquor = soup.select('#boil_liquor')[0]
        self.assertEqual('27 L', boil_liquor.string)

        boil_time = soup.select('#boil_time')[0]
        self.assertEqual(str(recipe.boil_time)+' mins', boil_time.string)

        # Hops
        for i, hop in enumerate(list(recipe.get_recipehop_ordered())):
            hop_elem = soup.select('#{}_hop a'.format(i+1))[0]
            self.assertEqual('{} {}%'.format(hop.hop.name, hop.alpha_acid_rating), hop_elem.string)

            hop_quantity = soup.select('#{}_hop_quantity'.format(i+1))[0]
            self.assertEqual('{:.0f} g'.format(hop.quantity, grain.unit), hop_quantity.string)

            hop_ibu = soup.select('#{}_hop_ibu'.format(i+1))[0]
            self.assertEqual(format(hop.ibu, '.1f'), hop_ibu.string)

            wta = soup.select('#{}_hop_wta'.format(i+1))[0]
            self.assertEqual(hop.display_when_to_add(), wta.string)

        # Other additions
        for i, other in enumerate(list(recipe.get_recipeother_ordered())):
            other_elem = soup.select('#{}_other a'.format(i+1))[0]
            self.assertEqual(str(other.other), other_elem.string)

            other_quantity = soup.select('#{}_other_quantity'.format(i+1))[0]
            self.assertEqual('{} {}'.format(other.quantity, other.unit), other_quantity.string)

            other_wta = soup.select('#{}_other_wta'.format(i+1))[0]
            self.assertEqual(other.when_to_add, other_wta.string)

        yeast = soup.select('#yeast a')[0]
        self.assertEqual(str(recipe.yeast), yeast.string)

        fermentation_temp = soup.select('#fermentation_temp')[0]
        self.assertEqual(str(recipe.fermentation_temp)+'°C', fermentation_temp.string)

        conditioning = soup.select('#conditioning_time')[0]
        self.assertEqual('{0} {1}@{2}°C'.format(recipe.conditioning_time, recipe.get_conditioning_time_quantifier_display(),
                                             recipe.conditioning_temp), conditioning.string)

        # Because we're logged in and the owner, the update, delete and brew it buttons should be visible too
        back = soup.select('#back')
        update = soup.select('#update')
        delete = soup.select('#delete')
        brew_it = soup.select('#brew_it')

        self.assertTrue(len(back) == 1)
        self.assertTrue(len(update) == 1)
        self.assertTrue(len(delete) == 1)
        self.assertTrue(len(brew_it) == 1)

    def test_baserecipe_detail_template(self):
        recipe = BaseRecipe.objects.get(pk=22)

        self.client.login(username=self.user_name, password=self.user_password)

        # Go to the recipe
        response = self.client.get(recipe.get_absolute_url())
        soup = BeautifulSoup(response.content.decode('utf-8'), "html.parser")

        # All the panels are displayed
        general = soup.select('#general')
        mash = soup.select('#mash')
        fermentables = soup.select('#fermentables')
        boil = soup.select('#boil')
        fermentation = soup.select('#fermentation')
        other_additions = soup.select('#other_additions')

        self.assertTrue(len(general) == 1)
        self.assertTrue(len(fermentables) == 1)
        self.assertTrue(len(mash) == 0)
        self.assertTrue(len(boil) == 0)
        self.assertTrue(len(fermentation) == 1)
        self.assertTrue(len(other_additions) == 1)

        # Make sure the values are being displayed
        name = soup.select('#name')[0]
        self.assertEqual(recipe.name, name.string)

        style = soup.select('#style')[0]
        self.assertEqual(recipe.style.name, style.string)

        description = soup.select('#description')[0]
        self.assertEqual(recipe.description, str(description.string).strip())

        target_volume = soup.select('#target_volume')[0]
        self.assertEqual(str(recipe.target_volume)+' L', target_volume.string)

        original_gravity = soup.select('#original_gravity')[0]
        self.assertEqual(str(recipe.original_gravity), original_gravity.string)

        expected_final_gravity = soup.select('#expected_final_gravity')[0]
        self.assertEqual(str(recipe.expected_final_gravity), expected_final_gravity.string)

        estimated_abv = soup.select('#alcohol_by_volume')[0]
        self.assertEqual(format(recipe.alcohol_by_volume(), '.2f')+'%', estimated_abv.string)

        bitterness_rating = soup.select('#bitterness_rating')
        self.assertTrue(len(bitterness_rating) == 0)

        steep_liquor = soup.select('#steep_liquor')
        self.assertTrue(len(steep_liquor) == 0)

        steep_time = soup.select('#steep_time')
        self.assertTrue(len(steep_time) == 0)

        steep_temperature = soup.select('#steep_temperature')
        self.assertTrue(len(steep_temperature) == 0)

        mash_liquor = soup.select('#mash_liquor')
        self.assertTrue(len(mash_liquor) == 0)

        mash_time = soup.select('#mash_time')
        self.assertTrue(len(mash_time) == 0)

        mash_temp = soup.select('#mash_temperature')
        self.assertTrue(len(mash_temp) == 0)

        # Grain
        for i, grain in enumerate(list(recipe.get_recipegrain_ordered())):
            grain_elem = soup.select('#{}_grain a'.format(i+1))[0]
            self.assertEqual(grain.fermentable.name, grain_elem.string)

            grain_quantity = soup.select('#{}_grain_quantity'.format(i+1))[0]
            self.assertEqual('{} {}'.format(grain.quantity, grain.unit), grain_quantity.string)

            grain_steeped = soup.select('#{}_grain_steep'.format(i+1))
            self.assertTrue(len(grain_steeped) == 0)

        boil_liquor = soup.select('#boil_liquor')
        self.assertTrue(len(boil_liquor) == 0)

        boil_time = soup.select('#boil_time')
        self.assertTrue(len(boil_time) == 0)


        # Other additions
        for i, other in enumerate(list(recipe.get_recipeother_ordered())):
            other_elem = soup.select('#{}_other a'.format(i+1))[0]
            self.assertEqual(str(other.other), other_elem.string)

            other_quantity = soup.select('#{}_other_quantity'.format(i+1))[0]
            self.assertEqual('{} {}'.format(other.quantity, other.unit), other_quantity.string)

            other_wta = soup.select('#{}_other_wta'.format(i+1))[0]
            self.assertEqual(other.when_to_add, other_wta.string)

        yeast = soup.select('#yeast a')[0]
        self.assertEqual(str(recipe.yeast), yeast.string)

        fermentation_temp = soup.select('#fermentation_temp')[0]
        self.assertEqual(str(recipe.fermentation_temp)+'°C', fermentation_temp.string)

        conditioning = soup.select('#conditioning_time')[0]
        self.assertEqual('{0} {1}@{2}°C'.format(recipe.conditioning_time, recipe.get_conditioning_time_quantifier_display(),
                                             recipe.conditioning_temp), conditioning.string)

        # Because we're logged in and the owner, the update, delete and brew it buttons should be visible too
        back = soup.select('#back')
        update = soup.select('#update')
        delete = soup.select('#delete')
        brew_it = soup.select('#brew_it')

        self.assertTrue(len(back) == 1)
        self.assertTrue(len(update) == 1)
        self.assertTrue(len(delete) == 1)
        self.assertTrue(len(brew_it) == 1)

    def test_allgrainrecipe_brew_detail_template(self):
        recipe = AllGrainRecipe.objects.get(pk=19)
        brew = Brew(recipe=recipe, owner=recipe.owner, brew_date=datetime.now())
        brew.save()

        self.client.login(username=self.user_name, password=self.user_password)

        # Go to the recipe
        response = self.client.get(brew.get_absolute_url())
        soup = BeautifulSoup(response.content.decode('utf-8'), "html.parser")

        # All the panels are displayed
        brew_details = soup.select('#brew_details')
        general = soup.select('#general')
        mash = soup.select('#mash')
        boil = soup.select('#boil')
        fermentation = soup.select('#fermentation')
        other_additions = soup.select('#other_additions')

        self.assertTrue(len(brew_details) == 1)
        self.assertTrue(len(general) == 1)
        self.assertTrue(len(mash) == 1)
        self.assertTrue(len(boil) == 1)
        self.assertTrue(len(fermentation) == 1)
        self.assertTrue(len(other_additions) == 1)

        # Make sure the values are being displayed
        name = soup.select('#name')[0]
        self.assertEqual(recipe.name, name.string)

        style = soup.select('#style')[0]
        self.assertEqual(recipe.style.name, style.string)

        description = soup.select('#description')[0]
        self.assertEqual(recipe.description, str(description.string).strip())

        target_volume = soup.select('#target_volume')[0]
        self.assertEqual(str(recipe.target_volume)+' L', target_volume.string)

        original_gravity = soup.select('#original_gravity')[0]
        self.assertEqual(str(recipe.original_gravity), original_gravity.string)

        expected_final_gravity = soup.select('#expected_final_gravity')[0]
        self.assertEqual(str(recipe.expected_final_gravity), expected_final_gravity.string)

        estimated_abv = soup.select('#alcohol_by_volume')[0]
        self.assertEqual(format(recipe.alcohol_by_volume(), '.2f')+'%', estimated_abv.string)

        bitterness_rating = soup.select('#bitterness_rating')[0]
        self.assertEqual(str(recipe.get_bitterness_rating()), bitterness_rating.string)

        mash_liquor = soup.select('#mash_liquor')[0]
        self.assertEqual(format(recipe.mash_liquor, '.1f')+' L', mash_liquor.string)

        mash_thickness = soup.select('#mash_thickness')[0]
        self.assertEqual(str(recipe.mash_thickness)+' L/kg', mash_thickness.string)

        sparge_volume = soup.select('#sparge_volume')[0]
        self.assertEqual('0 L', sparge_volume.string)

        mash_time = soup.select('#mash_time')[0]
        self.assertEqual(str(recipe.mash_time)+' mins', mash_time.string)

        mash_temp = soup.select('#mash_temperature')[0]
        self.assertEqual(str(recipe.mash_temperature)+'°C', mash_temp.string)

        # Grain
        for i, grain in enumerate(list(recipe.get_recipegrain_ordered())):
            grain_elem = soup.select('#{}_grain a'.format(i+1))[0]
            self.assertEqual(grain.fermentable.name, grain_elem.string)

            grain_quantity = soup.select('#{}_grain_quantity'.format(i+1))[0]
            self.assertEqual('{} {}'.format(grain.quantity, grain.unit), grain_quantity.string)

        boil_liquor = soup.select('#boil_liquor')[0]
        self.assertEqual('27 L', boil_liquor.string)

        boil_time = soup.select('#boil_time')[0]
        self.assertEqual(str(recipe.boil_time)+' mins', boil_time.string)

        # Hops
        for i, hop in enumerate(list(recipe.get_recipehop_ordered())):
            hop_elem = soup.select('#{}_hop a'.format(i+1))[0]
            self.assertEqual('{} {}%'.format(hop.hop.name, hop.alpha_acid_rating), hop_elem.string)

            hop_quantity = soup.select('#{}_hop_quantity'.format(i+1))[0]
            self.assertEqual('{:.0f} g'.format(hop.quantity, grain.unit), hop_quantity.string)

            hop_ibu = soup.select('#{}_hop_ibu'.format(i+1))[0]
            self.assertEqual(format(hop.ibu, '.1f'), hop_ibu.string)

            wta = soup.select('#{}_hop_wta'.format(i+1))[0]
            self.assertEqual(hop.display_when_to_add(), wta.string)

        # Other additions
        for i, other in enumerate(list(recipe.get_recipeother_ordered())):
            other_elem = soup.select('#{}_other a'.format(i+1))[0]
            self.assertEqual(str(other.other), other_elem.string)

            other_quantity = soup.select('#{}_other_quantity'.format(i+1))[0]
            self.assertEqual('{} {}'.format(other.quantity, other.unit), other_quantity.string)

            other_wta = soup.select('#{}_other_wta'.format(i+1))[0]
            self.assertEqual(other.when_to_add, other_wta.string)

        yeast = soup.select('#yeast a')[0]
        self.assertEqual(str(recipe.yeast), yeast.string)

        fermentation_temp = soup.select('#fermentation_temp')[0]
        self.assertEqual(str(recipe.fermentation_temp)+'°C', fermentation_temp.string)

        conditioning = soup.select('#conditioning_time')[0]
        self.assertEqual('{0} {1}@{2}°C'.format(recipe.conditioning_time, recipe.get_conditioning_time_quantifier_display(),
                                             recipe.conditioning_temp), conditioning.string)

        # Because we're logged in and the owner, the update, delete and customise buttons should be visible too
        back = soup.select('#back')
        update = soup.select('#update')
        delete = soup.select('#delete')
        customise = soup.select('#customise')

        self.assertTrue(len(back) == 4) # 4 tabs, 4 back buttons
        self.assertTrue(len(update) == 1)
        self.assertTrue(len(delete) == 1)
        self.assertTrue(len(customise) == 1)

        # Make sure the tabs are displayed
        overview = soup.select('#overview')
        gravity_readings = soup.select('#gravity_readings')
        temperature_readings = soup.select('#temp_readings')
        notes = soup.select('#notes')

        self.assertTrue(len(overview) == 1)
        self.assertTrue(len(gravity_readings) == 1)
        self.assertTrue(len(temperature_readings) == 1)
        self.assertTrue(len(notes) == 1)

        # Add a temperature reading to make the priming panel display
        tr = TemperatureReading(batch=brew, time=datetime.now(), value=18)
        tr.save()

        response = self.client.get(brew.get_absolute_url())
        soup = BeautifulSoup(response.content.decode('utf-8'), "html.parser")

        priming = soup.select('#priming')
        self.assertTrue(len(priming) == 1)



    def test_extractrecipe_brew_detail_template(self):
        recipe = ExtractRecipe.objects.get(pk=21)
        brew = Brew(recipe=recipe, owner=recipe.owner, brew_date=datetime.now())
        brew.save()

        self.client.login(username=self.user_name, password=self.user_password)

        # Go to the recipe
        response = self.client.get(brew.get_absolute_url())
        soup = BeautifulSoup(response.content.decode('utf-8'), "html.parser")

        # All the panels are displayed
        brew_details = soup.select('#brew_details')
        general = soup.select('#general')
        mash = soup.select('#mash')
        fermentables = soup.select('#fermentables')
        boil = soup.select('#boil')
        fermentation = soup.select('#fermentation')
        other_additions = soup.select('#other_additions')

        self.assertTrue(len(general) == 1)
        self.assertTrue(len(fermentables) == 1)
        self.assertTrue(len(mash) == 0)
        self.assertTrue(len(boil) == 1)
        self.assertTrue(len(fermentation) == 1)
        self.assertTrue(len(other_additions) == 1)

        # Make sure the values are being displayed
        name = soup.select('#name')[0]
        self.assertEqual(recipe.name, name.string)

        style = soup.select('#style')[0]
        self.assertEqual(recipe.style.name, style.string)

        description = soup.select('#description')[0]
        self.assertEqual(recipe.description, str(description.string).strip())

        original_gravity = soup.select('#original_gravity')[0]
        self.assertEqual(str(recipe.original_gravity), original_gravity.string)

        expected_final_gravity = soup.select('#expected_final_gravity')[0]
        self.assertEqual(str(recipe.expected_final_gravity), expected_final_gravity.string)

        estimated_abv = soup.select('#alcohol_by_volume')[0]
        self.assertEqual(format(recipe.alcohol_by_volume(), '.2f')+'%', estimated_abv.string)

        bitterness_rating = soup.select('#bitterness_rating')[0]
        self.assertEqual(str(recipe.get_bitterness_rating()), bitterness_rating.string)

        steep_liquor = soup.select('#steep_liquor')[0]
        self.assertEqual('0 L', steep_liquor.string)

        steep_time = soup.select('#steep_time')[0]
        self.assertEqual(str(recipe.steep_time)+' mins', steep_time.string)

        steep_temperature = soup.select('#steep_temperature')[0]
        self.assertEqual(str(recipe.steep_temperature)+'°C', steep_temperature.string)

        mash_liquor = soup.select('#mash_liquor')
        self.assertTrue(len(mash_liquor) == 0)

        mash_time = soup.select('#mash_time')
        self.assertTrue(len(mash_time) == 0)

        mash_temp = soup.select('#mash_temperature')
        self.assertTrue(len(mash_temp) == 0)

        # Grain
        for i, grain in enumerate(list(recipe.get_recipegrain_ordered())):
            grain_elem = soup.select('#{}_grain a'.format(i+1))[0]
            self.assertEqual(grain.fermentable.name, grain_elem.string)

            grain_quantity = soup.select('#{}_grain_quantity'.format(i+1))[0]
            self.assertEqual('{} {}'.format(grain.quantity, grain.unit), grain_quantity.string)

            grain_steeped = soup.select('#{}_grain_steep .glyphicon'.format(i+1))
            if grain.steeped:
                self.assertTrue(len(grain_steeped) == 1)
            else:
                self.assertTrue(len(grain_steeped) == 0)

        boil_liquor = soup.select('#boil_liquor')[0]
        self.assertEqual('27 L', boil_liquor.string)

        boil_time = soup.select('#boil_time')[0]
        self.assertEqual(str(recipe.boil_time)+' mins', boil_time.string)

        # Hops
        for i, hop in enumerate(list(recipe.get_recipehop_ordered())):
            hop_elem = soup.select('#{}_hop a'.format(i+1))[0]
            self.assertEqual('{} {}%'.format(hop.hop.name, hop.alpha_acid_rating), hop_elem.string)

            hop_quantity = soup.select('#{}_hop_quantity'.format(i+1))[0]
            self.assertEqual('{:.0f} g'.format(hop.quantity, grain.unit), hop_quantity.string)

            hop_ibu = soup.select('#{}_hop_ibu'.format(i+1))[0]
            self.assertEqual(format(hop.ibu, '.1f'), hop_ibu.string)

            wta = soup.select('#{}_hop_wta'.format(i+1))[0]
            self.assertEqual(hop.display_when_to_add(), wta.string)

        # Other additions
        for i, other in enumerate(list(recipe.get_recipeother_ordered())):
            other_elem = soup.select('#{}_other a'.format(i+1))[0]
            self.assertEqual(str(other.other), other_elem.string)

            other_quantity = soup.select('#{}_other_quantity'.format(i+1))[0]
            self.assertEqual('{} {}'.format(other.quantity, other.unit), other_quantity.string)

            other_wta = soup.select('#{}_other_wta'.format(i+1))[0]
            self.assertEqual(other.when_to_add, other_wta.string)

        yeast = soup.select('#yeast a')[0]
        self.assertEqual(str(recipe.yeast), yeast.string)

        fermentation_temp = soup.select('#fermentation_temp')[0]
        self.assertEqual(str(recipe.fermentation_temp)+'°C', fermentation_temp.string)

        conditioning = soup.select('#conditioning_time')[0]
        self.assertEqual('{0} {1}@{2}°C'.format(recipe.conditioning_time, recipe.get_conditioning_time_quantifier_display(),
                                             recipe.conditioning_temp), conditioning.string)

        # Because we're logged in and the owner, the update, delete and customise buttons should be visible too
        back = soup.select('#back')
        update = soup.select('#update')
        delete = soup.select('#delete')
        customise = soup.select('#customise')

        self.assertTrue(len(back) == 4) # 4 tabs, 4 back buttons
        self.assertTrue(len(update) == 1)
        self.assertTrue(len(delete) == 1)
        self.assertTrue(len(customise) == 1)

        # Make sure the tabs are displayed
        overview = soup.select('#overview')
        gravity_readings = soup.select('#gravity_readings')
        temperature_readings = soup.select('#temp_readings')
        notes = soup.select('#notes')

        self.assertTrue(len(overview) == 1)
        self.assertTrue(len(gravity_readings) == 1)
        self.assertTrue(len(temperature_readings) == 1)
        self.assertTrue(len(notes) == 1)

        # Add a temperature reading to make the priming panel display
        tr = TemperatureReading(batch=brew, time=datetime.now(), value=18)
        tr.save()

        response = self.client.get(brew.get_absolute_url())
        soup = BeautifulSoup(response.content.decode('utf-8'), "html.parser")

        priming = soup.select('#priming')
        self.assertTrue(len(priming) == 1)


    def test_baserecipe_brew_detail_template(self):
        recipe = BaseRecipe.objects.get(pk=22)
        brew = Brew(recipe=recipe, owner=recipe.owner, brew_date=datetime.now())
        brew.save()

        self.client.login(username=self.user_name, password=self.user_password)

        # Go to the recipe
        response = self.client.get(brew.get_absolute_url())
        soup = BeautifulSoup(response.content.decode('utf-8'), "html.parser")

        # All the panels are displayed
        brew_details = soup.select('#brew_details')
        general = soup.select('#general')
        mash = soup.select('#mash')
        fermentables = soup.select('#fermentables')
        boil = soup.select('#boil')
        fermentation = soup.select('#fermentation')
        other_additions = soup.select('#other_additions')

        self.assertTrue(len(brew_details) == 1)
        self.assertTrue(len(brew_details) == 1)
        self.assertTrue(len(general) == 1)
        self.assertTrue(len(fermentables) == 1)
        self.assertTrue(len(mash) == 0)
        self.assertTrue(len(boil) == 0)
        self.assertTrue(len(fermentation) == 1)
        self.assertTrue(len(other_additions) == 1)

        # Make sure the values are being displayed
        name = soup.select('#name')[0]
        self.assertEqual(recipe.name, name.string)

        style = soup.select('#style')[0]
        self.assertEqual(recipe.style.name, style.string)

        description = soup.select('#description')[0]
        self.assertEqual(recipe.description, str(description.string).strip())

        original_gravity = soup.select('#original_gravity')[0]
        self.assertEqual(str(recipe.original_gravity), original_gravity.string)

        expected_final_gravity = soup.select('#expected_final_gravity')[0]
        self.assertEqual(str(recipe.expected_final_gravity), expected_final_gravity.string)

        estimated_abv = soup.select('#alcohol_by_volume')[0]
        self.assertEqual(format(recipe.alcohol_by_volume(), '.2f')+'%', estimated_abv.string)

        bitterness_rating = soup.select('#bitterness_rating')
        self.assertTrue(len(bitterness_rating) == 0)

        steep_liquor = soup.select('#steep_liquor')
        self.assertTrue(len(steep_liquor) == 0)

        steep_time = soup.select('#steep_time')
        self.assertTrue(len(steep_time) == 0)

        steep_temperature = soup.select('#steep_temperature')
        self.assertTrue(len(steep_temperature) == 0)

        mash_liquor = soup.select('#mash_liquor')
        self.assertTrue(len(mash_liquor) == 0)

        mash_time = soup.select('#mash_time')
        self.assertTrue(len(mash_time) == 0)

        mash_temp = soup.select('#mash_temperature')
        self.assertTrue(len(mash_temp) == 0)

        # Grain
        for i, grain in enumerate(list(recipe.get_recipegrain_ordered())):
            grain_elem = soup.select('#{}_grain a'.format(i+1))[0]
            self.assertEqual(grain.fermentable.name, grain_elem.string)

            grain_quantity = soup.select('#{}_grain_quantity'.format(i+1))[0]
            self.assertEqual('{} {}'.format(grain.quantity, grain.unit), grain_quantity.string)

            grain_steeped = soup.select('#{}_grain_steep'.format(i+1))
            self.assertTrue(len(grain_steeped) == 0)

        boil_liquor = soup.select('#boil_liquor')
        self.assertTrue(len(boil_liquor) == 0)

        boil_time = soup.select('#boil_time')
        self.assertTrue(len(boil_time) == 0)


        # Other additions
        for i, other in enumerate(list(recipe.get_recipeother_ordered())):
            other_elem = soup.select('#{}_other a'.format(i+1))[0]
            self.assertEqual(str(other.other), other_elem.string)

            other_quantity = soup.select('#{}_other_quantity'.format(i+1))[0]
            self.assertEqual('{} {}'.format(other.quantity, other.unit), other_quantity.string)

            other_wta = soup.select('#{}_other_wta'.format(i+1))[0]
            self.assertEqual(other.when_to_add, other_wta.string)

        yeast = soup.select('#yeast a')[0]
        self.assertEqual(str(recipe.yeast), yeast.string)

        fermentation_temp = soup.select('#fermentation_temp')[0]
        self.assertEqual(str(recipe.fermentation_temp)+'°C', fermentation_temp.string)

        conditioning = soup.select('#conditioning_time')[0]
        self.assertEqual('{0} {1}@{2}°C'.format(recipe.conditioning_time, recipe.get_conditioning_time_quantifier_display(),
                                             recipe.conditioning_temp), conditioning.string)

        # Because we're logged in and the owner, the update, delete and customise buttons should be visible too
        back = soup.select('#back')
        update = soup.select('#update')
        delete = soup.select('#delete')
        customise = soup.select('#customise')

        self.assertTrue(len(back) == 4) # 4 tabs, 4 back buttons
        self.assertTrue(len(update) == 1)
        self.assertTrue(len(delete) == 1)
        self.assertTrue(len(customise) == 1)

        # Make sure the tabs are displayed
        overview = soup.select('#overview')
        gravity_readings = soup.select('#gravity_readings')
        temperature_readings = soup.select('#temp_readings')
        notes = soup.select('#notes')

        self.assertTrue(len(overview) == 1)
        self.assertTrue(len(gravity_readings) == 1)
        self.assertTrue(len(temperature_readings) == 1)
        self.assertTrue(len(notes) == 1)

        # Add a temperature reading to make the priming panel display
        tr = TemperatureReading(batch=brew, time=datetime.now(), value=18)
        tr.save()

        response = self.client.get(brew.get_absolute_url())
        soup = BeautifulSoup(response.content.decode('utf-8'), "html.parser")

        priming = soup.select('#priming')
        self.assertTrue(len(priming) == 1)

    def test_issue_15_spellcheck(self):
        self.client.login(username=self.user_name, password=self.user_password)

        # Recipe: name & description
        response = self.client.get(reverse('recipe-create-basic'))
        recipe_soup = BeautifulSoup(response.content.decode('utf-8'), "html.parser")

        recipe_name = recipe_soup.select('#id_name')[0]
        recipe_description = recipe_soup.select('#id_description')[0]

        self.assertEqual('true', recipe_name['spellcheck'])
        self.assertEqual('true', recipe_description['spellcheck'])

        # Fermentable: name & description
        response = self.client.get(reverse('fermentable-create'))
        ferm_soup = BeautifulSoup(response.content.decode('utf-8'), "html.parser")

        ferm_name = ferm_soup.select('#id_name')[0]
        ferm_description = ferm_soup.select('#id_description')[0]

        self.assertEqual('true', ferm_name['spellcheck'])
        self.assertEqual('true', ferm_description['spellcheck'])

        # Hop: name & description
        response = self.client.get(reverse('hop-create'))
        hop_soup = BeautifulSoup(response.content.decode('utf-8'), "html.parser")

        hop_name = hop_soup.select('#id_name')[0]
        hop_description = hop_soup.select('#id_description')[0]

        self.assertEqual('true', hop_name['spellcheck'])
        self.assertEqual('true', hop_description['spellcheck'])

        # Other: name & description
        response = self.client.get(reverse('other-create'))
        other_soup = BeautifulSoup(response.content.decode('utf-8'), "html.parser")

        other_name = other_soup.select('#id_name')[0]
        other_description = other_soup.select('#id_description')[0]

        self.assertEqual('true', other_name['spellcheck'])
        self.assertEqual('true', other_description['spellcheck'])

        # Yeast: name & description
        response = self.client.get(reverse('yeast-create'))
        yeast_soup = BeautifulSoup(response.content.decode('utf-8'), "html.parser")

        yeast_name = yeast_soup.select('#id_name')[0]
        yeast_description = yeast_soup.select('#id_description')[0]

        self.assertEqual('true', yeast_name['spellcheck'])
        self.assertEqual('true', yeast_description['spellcheck'])